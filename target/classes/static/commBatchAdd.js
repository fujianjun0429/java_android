/**
 * Created by 13 on 2017/2/22.
 */

		function doSearchOnline(value){
			var url = 'onlineQuickInfoList?appName='+value;
			$('#onlineAppsGrid').datagrid('reload',url);
		}
	
		function toChecked(value,row,index){
			console.log("toChecked（）");
			var datas = $('#onlineAppsGrid').datagrid('getChecked');
			if(datas.length>0 ){
				for(var i=0;i<datas.length;i++){
			       var row = datas[i];
					console.log("toChecked（）%o", row);
			
			 		$('#resultTabel').datagrid('appendRow',row);
			
			        //获取行的编号，删除
			        var index=$('#onlineAppsGrid').datagrid('getRowIndex',row);
			        $('#onlineAppsGrid').datagrid('deleteRow',index);
			   }
			}
	    }
		
		function toUnChecked(appid){
			console.log("toUnChecked（）");
			var datas = $('#resultTabel').datagrid('getChecked');
			if(datas.length>0){
				for(var i=0;i<datas.length;i++){
			       var row = datas[i];
			
			 		$('#onlineAppsGrid').datagrid('appendRow',row);
			
			        //获取行的编号，删除
			        var index=$('#resultTabel').datagrid('getRowIndex',row);
			        $('#resultTabel').datagrid('deleteRow',index);
			    }
				
				//$('#onlineAppsGrid').datagrid('checkAll');
				$('#onlineAppsGrid').datagrid('beginEdit', 1);
				$('#onlineAppsGrid').datagrid('selectRow', index).datagrid('beginEdit', index);
				
			}
		}
		
		$.extend($.fn.datagrid.methods, {
			editCell: function(jq,param){
				return jq.each(function(){
					var fields = $(this).datagrid('getColumnFields',true).concat($(this).datagrid('getColumnFields'));
					for(var i=0; i<fields.length; i++){
						var col = $(this).datagrid('getColumnOption', fields[i]);
						col.editor1 = col.editor;
						if (fields[i] != param.field){
							col.editor = null;
						}
					}
					$(this).datagrid('beginEdit', param.index);
					for(var i=0; i<fields.length; i++){
						var col = $(this).datagrid('getColumnOption', fields[i]);
						col.editor = col.editor1;
					}
				});
			}
		});
		
		var editIndex = undefined;
		function endEditing(){
			if (editIndex == undefined){return true}
			if ($('#resultTabel').datagrid('validateRow', editIndex)){
				$('#resultTabel').datagrid('endEdit', editIndex);
				editIndex = undefined;
				return true;
			} else {
				return false;
			}
		}
		
		function onClickCell(index, field){
			if (endEditing()){
				$('#resultTabel').datagrid('selectRow', index)
						.datagrid('editCell', {index:index,field:field});
				editIndex = index;
			}
		}
	
