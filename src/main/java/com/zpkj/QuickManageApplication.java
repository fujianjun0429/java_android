package com.zpkj;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;


@SpringBootApplication
@MapperScan("com.zpkj.quick.manage.mapper")
@EnableScheduling
public class QuickManageApplication {
	private static final Logger LOGGER = LoggerFactory.getLogger(QuickManageApplication.class);
	private static final String TAG = QuickManageApplication.class.getSimpleName()+":{} ";

	public static void main(String[] args) {
		LOGGER.error(TAG, "++++++++++++++++++++++++++++++++++++++++++++start");
		ApplicationContext ctx = SpringApplication.run(QuickManageApplication.class, args);
		InetAddress localHost = null;
		try {
			localHost = Inet4Address.getLocalHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		String ip = localHost.getHostAddress();  // 返回格式为：xxx.xxx.xxx
		LOGGER.info("IP："+ip+"\t端口："+ctx.getEnvironment().getProperty("local.server.port")+"++++++++++++++++++++++++++++++++++:{}\n\n");
	}

}
