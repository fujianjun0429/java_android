package com.zpkj.quick.manage.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(StringUtil.class);
	
	public static String shortString(String sourceStr) {
		String resStr=sourceStr;
		if(!NullUtil.isNull(sourceStr)&& sourceStr.length()>=255){
			resStr = sourceStr.substring(0, 254);
		}
		return resStr;
	}

	
	
	public static boolean isEnglish(String charaString) {
		return charaString.matches("^[a-zA-Z]*");
	}

	public static boolean isChinese(String str) {
		String regEx = "[\\u4e00-\\u9fa5]+";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		if (m.find())
			return true;
		else
			return false;
	}

    /**
     * 将字符串转化为Boolean值
     */
    public static boolean string2Boolean(String str) {
        if ("y".equalsIgnoreCase(str)) {
            return true;
        }
        return false;
    }

    /**
     * 将字符串转化为Integer值
     */
    public static int string2Integer(String str) {
        int nRes = -1;
        if (!NullUtil.isNull(str)) {
            try {
                nRes = Integer.parseInt(str);
            }
            catch (Exception e) {
            	LOGGER.debug("string2Integer() : " + e.getMessage());
            }
        }
        return nRes;
    }
    /**
     * 将字符串转化为Integer值
     */
    public static int float2Integer(float f) {
    	int nRes = -1;
    	nRes =Math.round(f);
    	return nRes;
    }
    /**
     * 将字符串转化为Integer值
     */
    public static long str2Long(String str) {
        long nRes = -1;
        if (!NullUtil.isNull(str)) {
            try {
                nRes = Long.parseLong(str);
            }
            catch (Exception e) {
                LOGGER.debug("str2Long() : " + e.getMessage());
            }
        }
        return nRes;
    }
    /**
     * 将字符串转化为Float值
     */
    public static float string2Float(String str) {
        float result = 0.0f;
        if (!NullUtil.isNull(str)) {
            try {
                result = Float.parseFloat(str);
            }
            catch (Exception e) {
                LOGGER.debug( "string2Float() : " + e.getMessage());
            }
        }
        return result;
    }
    
    /**
     * 将字符串转化为double值
     */
    public static double string2Double(String str) {
        double result = 0.0;
        if (!NullUtil.isNull(str)) {
            try {
                result = Double.valueOf(str);
            }
            catch (Exception e) {
                LOGGER.debug( "string2Double() : " + e.getMessage());
            }
        }
        return result;
    }
    
    
    public static Date getDate(String dateString) {
        LOGGER.debug( "dateString = " + dateString);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = sf.parse(dateString);
        }
        catch (ParseException e) {
            LOGGER.debug( "",e);
        }
        return date;
    }
    
    /**
     * 获取格式化后的时间中的几点
     */
    public static int getFormatTime_Hour() {
        Calendar ca = Calendar.getInstance();
//        int year = ca.get(Calendar.YEAR);//获取年份
//        int month=ca.get(Calendar.MONTH);//获取月份
//        int day=ca.get(Calendar.DATE);//获取日
        int hour=ca.get(Calendar.HOUR_OF_DAY);//小时
//        int minute=ca.get(Calendar.MINUTE);//分
//        int second=ca.get(Calendar.SECOND);//秒
        LOGGER.debug("hour="+hour );
        return hour;
    }
    
    /**
     * 获取格式化后星期几
     */
    public static String getFormat_week() {
        Calendar ca = Calendar.getInstance();
        String day = "";
        int dayWeek = ca.get(Calendar.DAY_OF_WEEK);
        if(1 == dayWeek){  
            day ="星期天";  
        }else if(2 == dayWeek){  
            day ="星期一";  
        }else if(3 == dayWeek){  
            day ="星期二";  
        }else if(4 == dayWeek){  
            day ="星期三";  
        }else if(5 == dayWeek){  
            day ="星期四";  
        }else if(6 == dayWeek){  
            day ="星期五";  
        }else if(7 == dayWeek){  
            day ="星期六";  
        }  
        return day;
    }
    
    /**
     * 获取格式化后的日期时间字符串
     *  // 12小时制
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        // 24小时制
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
     */
    public static String getFormat_Date_HH_MM_SS(long time) {
        String result = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            result = dateFormat.format(time);
        }
        catch (Exception e) {
        }
        return result;
    }
    /**
     * 获取格式化后的日期时间字符串
     *  // 12小时制
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        // 24小时制
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
     */
    public static String getFormatDate_HH_MM_SS(long time) {
        String result = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            result = dateFormat.format(time);
        }
        catch (Exception e) {
        }
        return result;
    }
    
    /**
     * 获取格式化后的日期时间字符串
     */
    public static String getFormatDate_HH_MM(long time) {
        String result = "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            result = dateFormat.format(time);
        }
        catch (Exception e) {
        }
        return result;
    }
    
    public static String getFormatDate_HHMMSS(long time) {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        return dateFormat.format(time);
    }
    
    /** 
     * 将字符串数据转化为毫秒数 
     */  
    public static long str2MilTime(String timeStr) {
        Calendar c = Calendar.getInstance();  
        try {
            c.setTime(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(timeStr));  
            //c.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(timeStr));
        }
        catch (ParseException e) {
            LOGGER.debug("",e);
        }  
        LOGGER.debug("时间转化后的毫秒数为：" + c.getTimeInMillis());  
        return c.getTimeInMillis();
    }
   

    /**
     * 格式化大小(科学计数法)， 1000000byte = 1,000,000byte
     * @param size
     */
     public  static  String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = "KiB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MiB";
                size /= 1024;
            }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));
        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null) {
            resultBuffer.append(suffix);
        }
        return resultBuffer.toString();
    }
     
     /**
      * 格式化大小(科学计数法)， 100000字 = 10万字
      * @param size
      */
      public  static  String formatBookSize(long size) {
         String suffix = null;

         if (size >= 10000) {
             suffix = "万字";
             size /= 10000;
             if (size >= 10000) {
                 suffix = "亿字";
                 size /= 10000;
             }
         }

         StringBuilder resultBuffer = new StringBuilder(Long.toString(size));
         if (suffix != null) {
             resultBuffer.append(suffix);
         }
         return resultBuffer.toString();
     }
     
    /**
     * 格式化大小 1250000byte = 1.25M
     * 
     * @param size 应用的大小 单位为字节
     * @return 以KB/MB 为单位的大小
     */
    public static String formatAppSize(long size) {
        float sizes = Float.valueOf(size) / 1024;
        if (sizes > 1024) {
            sizes /= 1024;
            return String.format("%.1f", sizes) + "M";
        }
        else {
            return String.format("%.0f", sizes) + "KB";
        }
    }
    
    
    public static String delBankLine() {
        try {
            FileReader fi = new FileReader("test_8.txt");
            BufferedReader br = new BufferedReader(fi);
            FileWriter fw = new FileWriter("test_9.txt");
            PrintWriter pw = new PrintWriter(fw);
            String read;
            do {
                read = br.readLine();
                if (read != null) {
                    if (!read.equals("")) {
                        pw.println(read);
                    }
                }
            } while (read != null);
           
            pw.flush();
            pw.close();
            br.close();
        }
        catch (Exception e) {
        }
        return null;
    }
    
    public static String stream2String(InputStream inStream, String encoding) {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, len);
            }
            inStream.close();
        }
        catch (Exception e) {
        }
        String result = "";
        if (!NullUtil.isNull(encoding)) {
            result = new String(outStream.toByteArray());
        }
        else {
            try {
                result = new String(outStream.toByteArray(), encoding);
            }
            catch (UnsupportedEncodingException e) {
            }
        }
        return result;
    }
}
