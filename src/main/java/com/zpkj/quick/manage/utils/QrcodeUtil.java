package com.zpkj.quick.manage.utils;

import com.google.zxing.common.BitMatrix;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * 二维码的生成需要借助MatrixToImageWriter类，该类是由Google提供的，可以将该类直接拷贝到源码中使用
 */
@SuppressWarnings("restriction")
public class QrcodeUtil {
	private static final int BLACK = 0xFF000000;
	private static final int WHITE = 0xFFFFFFFF;

	private QrcodeUtil() {
	}

	public static BufferedImage toBufferedImage(BitMatrix matrix) {
		int width = matrix.getWidth();
		int height = matrix.getHeight();
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				image.setRGB(x, y, matrix.get(x, y) ? BLACK : WHITE);
			}
		}
		return image;
	}

	/**
	 * 生成二维码
	 * 
	 * @param matrix
	 * @param format
	 * @param file
	 * @throws IOException
	 */
	public static void writeToFile(BitMatrix matrix, String format, File file)
			throws IOException {
		BufferedImage image = toBufferedImage(matrix);
		if (!ImageIO.write(image, format, file)) {
			throw new IOException("Could not write an image of format "
					+ format + " to " + file);
		}
	}

	public static void writeToStream(BitMatrix matrix, String format,
			OutputStream stream) throws IOException {
		BufferedImage image = toBufferedImage(matrix);
		if (!ImageIO.write(image, format, stream)) {
			throw new IOException("Could not write an image of format "
					+ format);
		}
	}

	/**
	 * 合成图片：把图片叠加到二维码上面
	 * 
	 * @param qrcodeUrl 二维码url
	 * @param addPic 添加图片url
	 * @param outImageUrl 合并后的图片url
	 */
	public static void AmalgamateImage(String qrcodeUrl, String addPic,
			String outImageUrl) {
		try {
			for (int i = 0; i < 9; i++) {
				for (int j = 1; j < 4; j++) {
					InputStream imagein = new FileInputStream(qrcodeUrl);
					InputStream imagein2 = new FileInputStream(addPic);

					BufferedImage image = ImageIO.read(imagein);
					BufferedImage image2 = ImageIO.read(imagein2);
					Graphics g = image.getGraphics();
					g.drawImage(image2, image.getWidth() / 2,
							image.getHeight() / 2, image2.getWidth() / 2,
							image2.getHeight() / 2, null);
//					OutputStream outImage = new FileOutputStream(outImageUrl);
//					JPEGImageEncoder enc = JPEGCodec
//							.createJPEGEncoder(outImage);
//					enc.encode(image);
					
					String formatName = outImageUrl.substring(outImageUrl.lastIndexOf(".") + 1);
			        //FileOutputStream out = new FileOutputStream(dstName);
			        //JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
			        //encoder.encode(dstImage);
			        ImageIO.write(image, /*"GIF"*/ formatName /* format desired */ , new File(outImageUrl) /* target */ );
			         
					imagein.close();
					imagein2.close();
//					outImage.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
