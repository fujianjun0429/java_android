package com.zpkj.quick.manage.utils;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExUtil {
    
    /**
     * 判别一个字符串能否转化为数字
     */
    public static Boolean isNumber(String str) {
        if (str == null) {
            return Boolean.FALSE;
        }
        String rule = "[0-9]+";
        return str.matches(rule);
    }
    
    public static boolean isPhoneNumber(String num, boolean bGuDingPhone) {
        return num.matches("^\\d+\\D?$"); // ^表示行开始，$表示行结束
    }
    
    public static boolean isPhoneNumber(String num) {
        if ( NullUtil.isNull(num)) {
            return false;
        }
        String rule = "1[0-9]{10}";
        return num.trim().matches(rule);
    }
    
    /**
     * 非法字符过滤
     * @param str
     */
    public static String filterSpecialStr(String str) {
        // 正则表达式，是"[]"格式
        String regEx = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？\n\r]";
        Pattern p = Pattern.compile(regEx);
        String result = "";
        if(!NullUtil.isNull(str)){
            Matcher m = p.matcher(str);
            // 替换非法字符
            result= m.replaceAll("").trim();
        }
        return result;
    }
    
    /**
     * 过滤
     */
    public static String filterGif(String str) {
        if(NullUtil.isNull(str)) return null;
        // 正则表达式
        String regEx = "\\[\\s*mobcent_phiz[^]]+\\s*\\]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        
        // 替换
        String result = m.replaceAll("").trim();
        return result;
    }
    /**
     * 过滤 管理员
     */
    public static String filterManager(String str) {
        if(NullUtil.isNull(str)) return null;
        // 正则表达式
        String regEx = "[\\s*管理笔趣阁\\s*]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        
        // 替换
        String result = m.replaceAll("*").trim();
        return result;
    }
    
    /**
     * 过滤部分字符
     */
    public static String filterPostText(String str) {
        // 正则表达式
        String regEx = "[`~#$^&*|{}\\[\\]<>/?~#￥&*（）——+|{}【】\n\r]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        // 替换非法字符
        String result = m.replaceAll("").trim();
        return result;
    }
    /**
     * 过滤空格换行字符
     */
    public static String filterSpaceText(String str) {
        if(NullUtil.isNull(str)) return "";
        // 正则表达式
        String regEx = "[\t\n\r]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        // 替换非法字符
        String result = m.replaceAll("").trim();
        return result;
    }
    
    public static ArrayList<String> getGroups(String str) {
        ArrayList<String> results = new ArrayList<String>();
        String regEx = "\\[(\\s*[^]]+\\s*)\\]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        if (m.matches()) {
            for (int i = 1, size = m.groupCount(); i < size; i++) {
                //MLog.d("filterGif () group " + i + " =" + m.group(i));
            }
        }
        return results;
    }
    
    
    /**
     * \s* 空格/换行/tab 匹配
     * 分组的左括号为标志，第一个出现的分组的组号为1，第二个为2，以此类推。组0代表整个表达式
     */
    public static ArrayList<String> getParames(String str) {
        ArrayList<String> resArr = new ArrayList<String>();
        String regEx = "[\\s\\S]*location\\.replace.{2}(http[^\\?]*)\\?([^\"]*)[\\s\\S]*"; //[\\s\\S]* 相当于所有字符
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        //String test = m.replaceAll("");
        //String all = m.quoteReplacement(regEx);
        //boolean bTest= str.trim().matches(regEx);
        resArr.add("group(2)内容为以&连接的参数串");
        if (m.matches()) {  //matches()函数需要完全匹配，才返回 true
            for (int i = 1, size = m.groupCount(); i <= size; i++) {
                //MLog.d("getParames () group " + i + " =" + m.group(i));
                resArr.add(m.group(i));
            }
        }
        return resArr;
    }
    
    /**
     * \s* 空格/换行/tab 匹配
     * 分组的左括号为标志，第一个出现的分组的组号为1，第二个为2，以此类推。组0代表整个表达式
     */
    public static ArrayList<String> getZMUrl(String str) {
        ArrayList<String> resArr = new ArrayList<String>();
        String regEx = "[\\s\\S]*=([^\\.]*\\.jpg)[\\s\\S]*"; //[\\s\\S]* 相当于所有字符
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        //String test = m.replaceAll("");
        //String all = m.quoteReplacement(regEx);
        //boolean bTest= str.trim().matches(regEx);
        resArr.add("全部");
        if (m.matches()) {  //matches()函数需要完全匹配，才返回 true
            for (int i = 1, size = m.groupCount(); i <= size; i++) {
                //MLog.d("getParames () group " + i + " =" + m.group(i));
                resArr.add(m.group(i));
            }
        }
        return resArr;
    }
    
    public static ArrayList<String> getAction(String str) {
        ArrayList<String> resArr = new ArrayList<String>();
        String regEx = "[\\s\\S]*portalForm[\\s\\S]*action.{2}([^;]*)[\\s\\S]*"; //[\\s\\S]* 相当于所有字符
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        if (m.matches()) {  //matches()函数需要完全匹配，才返回 true
            for (int i = 1, size = m.groupCount(); i <= size; i++) {
                //MLog.d("getAction () group " + i + " =" + m.group(i));
                resArr.add(m.group(i));
            }
        }
        return resArr;
    }
    public static String getS(String str) {
        String result = "";
        String regEx = "[\\s\\S]*s=([\\s\\S]*)"; //[\\s\\S]* 相当于所有字符
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        if (m.matches()) {   
            for (int i = 1, size = m.groupCount(); i <= size; i++) {
                System.out.println("getAction () group " + i + " =" + m.group(i));
            }
            result = m.group(1);
        }
        return result;
    }
    
    /**
     * 我想采集一个网址的链接，然后提取链接文字。为了完成这一步骤，我分两步走：
        1、提前<a>和</a>之间的内容。如 从  "p id=km>&nbsp;<a href=http://hi.baidu.com>空间</a>” 提取 <a href=http://hi.baidu.com>空间</a>
        2、提取出"空间"两个字。
     */
//    public static String getTestStr()
//   {
//               String s="<p id=km>&nbsp;<a href=http://hi.baidu.com>空间</a>&nbsp;|&nbsp;</a>  ";
//               String regex="<a.*?/a>";  //非贪婪匹配      
//               //String regex = "<a(.*)</a>"; // 默认为贪婪匹配
//               Pattern pt=Pattern.compile(regex);
//               Matcher mt=pt.matcher(s);
//               if(mt.find())
//               {
//                   System.out.println(mt.group());
//                    String s2=">.*?</a>";
//                     Pattern pt2=Pattern.compile(s2);
//                     Matcher mt2=pt2.matcher(mt.group());
//                      if(mt2.find())
//                      {
//                           System.out.println(mt2.group());//得到结果
//                           return mt2.group();
//                      }
//                }
//        }
//   }
}
