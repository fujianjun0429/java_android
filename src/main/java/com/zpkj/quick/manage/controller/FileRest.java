package com.zpkj.quick.manage.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.service.FileService;


@RestController
public class FileRest {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileRest.class);

	@Autowired
	FileService fileService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/uploadFile")
    public GeneralResult uploadFileOrg(
         @RequestParam("upfile") MultipartFile upfile,
		 @RequestParam(value = "ftype", required = false, defaultValue = "jpg") String ftype,
		 @RequestParam(value = "id", required = false) Integer id
		 ) {
		
        GeneralResult result = new GeneralResult();
		if (upfile.isEmpty()) {
			result.setResultStatus(false);
			result.setErrorMessage("param upfile is not valid");
			return result;
		}
		try
		{
			return fileService.uploadAppImg(upfile, ftype, id);
		} catch (Exception e) {
			result.setErrorMessage("file upload failed");
			result.setResultStatus(false);
			LOGGER.error("file upload failed", e);
		}
		return result;
    }
	
	@RequestMapping(method = RequestMethod.POST, value = "/uploadApk")
    public GeneralResult uploadApk(
         @RequestParam("upfile") MultipartFile upfile,
		 @RequestParam(value = "ftype", required = false, defaultValue = "apk") String ftype,
		 @RequestParam(value = "appPkgName", required = false) String appPkgName
		 ) {
		
        GeneralResult result = new GeneralResult();
		if (upfile.isEmpty()) {
			result.setResultStatus(false);
			result.setErrorMessage("param upfile is isEmpty ");
			return result;
		}

		try {
			return fileService.uploadApk(upfile, ftype, appPkgName);
			
		} catch (Exception e) {
			result.setErrorMessage("file upload failed");
			result.setResultStatus(false);
			LOGGER.error("file upload failed", e);
		}
		return result;
    }
	
	
	@RequestMapping(value = "/uploadTest", method = RequestMethod.GET)
	public GeneralResult test(@RequestParam(value = "type", required = false, defaultValue = "jpg") String type) {

		GeneralResult result = new GeneralResult();
		LOGGER.error("file upload test", "test");
		return result;
	}
	
}
