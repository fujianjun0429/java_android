package com.zpkj.quick.manage.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zpkj.quick.manage.pojo.AdPositionSdk;
import com.zpkj.quick.manage.pojo.AdPositionType;
import com.zpkj.quick.manage.pojo.AdSdk;
import com.zpkj.quick.manage.pojo.QappResult;
import com.zpkj.quick.manage.service.AdService;
import com.zpkj.quick.manage.util.IUtil;
import com.zpkj.quick.manage.util.NullUtil;
import com.zpkj.quick.manage.util.QConstants;

@RestController
@RequestMapping(value = "/ad/")
public class AdController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdController.class);
	private static final String TAG = AdController.class.getSimpleName()+":{}";
	
	@Autowired
	AdService adService;
	
	//获取某个位置显示的广告
	@RequestMapping(value = "/getPositionAd" )
	public List<AdPositionSdk> getPositionAd(
			@RequestParam(value = "imei", required = false) String imei,
			@RequestParam(value = "brand", required = false) String brand,
			@RequestParam(value = "model", required = false) String model,
			@RequestParam(value = "positionId", required = false) String positionId,
			@RequestParam(value = "quickInfoKey", required = false) String quickInfoKey,
			@RequestParam(value = "request_id", required = false) String request_id,
			@RequestParam(value = "timestamp", required = false) String timestamp
			) {
		 List<AdPositionSdk> result = null;
		try {
			String cpPositionId = "";
			// 对外可见只有cpPositionId
			if(!NullUtil.isNull(positionId)){ 
				cpPositionId = positionId;
			}
			result = adService.getPositionAd(imei, brand, model , quickInfoKey,cpPositionId);
			
		} catch (Exception e) {
			LOGGER.error(TAG, "Server Exception! getPositionAd  内部错误", e);
		}
		
		return result;
	}
	
	//创建更新广告位置，和添加广告位sdk
	@RequestMapping(value = "/newOrUpdatePosition")
	public QappResult newOrUpdatePosition(
			@RequestParam(value = "quickInfoKey", required = true) String quickInfoKey,
			@RequestParam(value = "positionName", required = true) String positionName,//true 为了更新数据库书写简便
			@RequestParam(value = "positionTypeId", required = true) Integer positionTypeId,
			@RequestParam(value = "forceShowAd", required = false) Integer forceShowAd,
			@RequestParam(value = "sdkId", required = true) Integer sdkId,
			@RequestParam(value = "thirdAdId", required = true) String thirdAdId,
			@RequestParam(value = "thisSdkShow", required = true) Integer thisSdkShow,
			@RequestParam(value = "request_id", required = false) String request_id,
			@RequestParam(value = "timestamp", required = false) String timestamp
			) {
		QappResult result = new QappResult();
		try {
			result = adService.newOrUpdatePosition(positionName, IUtil.check(positionTypeId),
					IUtil.check(forceShowAd), quickInfoKey, IUtil.check(sdkId), thirdAdId,thisSdkShow);
			
		} catch (Exception e) {
			result.setCode(QConstants.ERROR_CODE);
			LOGGER.error(TAG, "Server Exception!", e);
			result.setSuccess(false);
			result.setMsg("服务器内部错误!");
		}
		
		result.setRequest_id(request_id);
		return result;
	}
	
	//更改该位置sdk是否可用
	@RequestMapping(value = "/enablemPositionSdk")
	public QappResult enablemPositionSdk(
			@RequestParam(value = "cpPositionId", required = true) String cpPositionId,
			@RequestParam(value = "sdkId", required = true) Integer sdkId,
			@RequestParam(value = "thisSdkShow", required = true) Integer thisSdkShow,
			@RequestParam(value = "request_id", required = false) String request_id,
			@RequestParam(value = "timestamp", required = false) String timestamp
			) {
		QappResult result = new QappResult();
		try {
			result = adService.enablemPositionSdk(cpPositionId, sdkId, thisSdkShow);
				
		} catch (Exception e) {
			result.setCode(QConstants.ERROR_CODE);
			LOGGER.error(TAG, "Server Exception!", e);
			result.setSuccess(false);
			result.setMsg("服务器内部错误!");
		}
		
		result.setRequest_id(request_id);
		return result;
	}
	
	//查询广告类型
	@RequestMapping(value = "/selectAdPositionTypes")
	public List<AdPositionType> selectAdPositionTypes(
			@RequestParam(value = "request_id", required = false) String request_id,
			@RequestParam(value = "timestamp", required = false) String timestamp
			) {
		List<AdPositionType> result =null;
		try {
			result = adService.selectAdPositionTypes();
		} catch (Exception e) {
			LOGGER.error(TAG, "Server Exception! selectSdks 服务器内部错误!", e);
		}
		
		return result;
	}
	
	//查询sdk列表
	@RequestMapping(value = "/selectSdks")
	public List<AdSdk> selectSdks(
			@RequestParam(value = "request_id", required = false) String request_id,
			@RequestParam(value = "timestamp", required = false) String timestamp
			) {
		List<AdSdk> result = null;
		try {
			result = adService.selectSdks();
			
		} catch (Exception e) {
			LOGGER.error(TAG, "Server Exception! selectSdks 内部错误", e);
		}
		
		return result;
	}
	
	//注册
	@RequestMapping(value = "/register" )
	public QappResult register(
			@RequestParam(value = "imei", required = false) String imei,
			@RequestParam(value = "brand", required = false) String brand,
			@RequestParam(value = "model", required = false) String model,
			@RequestParam(value = "quickKey", required = true) String quickKey,
			@RequestParam(value = "accountName", required = true) String accountName,//true 为了更新数据库书写简便
			@RequestParam(value = "accountPassword", required = true) String accountPassword,
			@RequestParam(value = "request_id", required = false) String request_id,
			@RequestParam(value = "timestamp", required = false) String timestamp
			) {
		QappResult result = new QappResult();
		try {
			result = adService.register(quickKey, accountName, accountPassword);
			
		} catch (Exception e) {
			result.setCode(QConstants.ERROR_CODE);
			LOGGER.error(TAG, "Server Exception!", e);
			result.setSuccess(false);
			result.setMsg("服务器内部错误!");
		}
		
		result.setRequest_id(request_id);
		return result;
	}
	
	//登录
	@RequestMapping(value = "/login" )
	public QappResult login(
			@RequestParam(value = "imei", required = false) String imei,
			@RequestParam(value = "brand", required = false) String brand,
			@RequestParam(value = "model", required = false) String model,
			@RequestParam(value = "accountName", required = true) String accountName, 
			@RequestParam(value = "accountPassword", required = true) String accountPassword,
			@RequestParam(value = "request_id", required = false) String request_id,
			@RequestParam(value = "timestamp", required = false) String timestamp
			) {
		QappResult result = new QappResult();
		try {
			result = adService.login( accountName, accountPassword);
		} catch (Exception e) {
			result.setCode(QConstants.ERROR_CODE);
			LOGGER.error(TAG, "Server Exception!", e);
			result.setSuccess(false);
			result.setMsg("服务器内部错误!");
		}
		
		result.setRequest_id(request_id);
		return result;
	}
	
	//认领App
	@RequestMapping(value = "/claimApp" )
	public QappResult claimApp(
			@RequestParam(value = "imei", required = false) String imei,
			@RequestParam(value = "brand", required = false) String brand,
			@RequestParam(value = "model", required = false) String model,
			@RequestParam(value = "accountName", required = false) String accountName, 
			@RequestParam(value = "accountPassword", required = false) String accountPassword,
			@RequestParam(value = "accountId", required = true) Integer accountId,
			@RequestParam(value = "quickKey", required = true) String quickKey,
			@RequestParam(value = "request_id", required = false) String request_id,
			@RequestParam(value = "timestamp", required = false) String timestamp
			) {
		QappResult result = new QappResult();
		try {
			result = adService.claimApp( quickKey, IUtil.check(accountId));
		} catch (Exception e) {
			result.setCode(QConstants.ERROR_CODE);
			LOGGER.error(TAG, "Server Exception!", e);
			result.setSuccess(false);
			result.setMsg("服务器内部错误!");
		}
		
		result.setRequest_id(request_id);
		return result;
	}
	
}
