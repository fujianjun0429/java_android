package com.zpkj.quick.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zpkj.quick.manage.pojo.PhoneSet;
import com.zpkj.quick.manage.pojo.QappResult;
import com.zpkj.quick.manage.pojo.SearchContentType;
import com.zpkj.quick.manage.service.PhoneSetService;
import com.zpkj.quick.manage.util.ControllerResultUtil;
import com.zpkj.quick.manage.util.IUtil;
import com.zpkj.quick.manage.util.QConstants;

@Controller
public class PhoneSetController {
	
	@Autowired
	PhoneSetService phoneSetService;
	
	@RequestMapping("/addPhoneSet")
	@ResponseBody
	public QappResult addPhoneSet(Integer id,String name,String phoneNick, String title){
		int count = phoneSetService.addPhoneSet( name,phoneNick,title);
		 QappResult result = new QappResult();
		 result.setSuccess(true);
		 result.setMsg(QConstants.SUCCESS);
		 result.setData(count);
		 
		 return result;
	}
	
	@RequestMapping("deletePhoneSet")
	@ResponseBody
	public String deletePhoneSet(Integer id){
		int result = phoneSetService.deletePhoneSet(id);
		if(result == 1){
			return ControllerResultUtil.SUCCEED;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("/addOrupdatePhoneSet")
	@ResponseBody
	public QappResult addOrupdatePhoneSet(Integer id,String name,String phoneNick,  Integer searchType){
		QappResult result = new QappResult();
		try {
			result = phoneSetService.addOrupdatePhoneSet(id, name, phoneNick, IUtil.check(searchType));
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMsg("服务器内部错误" + e.getMessage());
			return result;
		}
		return result;
	}

	@RequestMapping("phoneSetById")
	@ResponseBody
	public PhoneSet phoneSetById(Integer id){
		return phoneSetService.selectPhoneSetById(id);
	}
	
	@RequestMapping("phoneSetList")
	@ResponseBody
	public List<PhoneSet> phoneSetList(String phoneNick){
		return phoneSetService.selectPhoneList(phoneNick);
	}
	
	@RequestMapping("selectSearchContentTypeList")
	@ResponseBody
	public List<SearchContentType> selectSearchContentTypeList() {
		return phoneSetService.selectSearchContentTypeList();
	}
	
}
