package com.zpkj.quick.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zpkj.quick.manage.pojo.KViewType;
import com.zpkj.quick.manage.service.KViewTypeService;
import com.zpkj.quick.manage.util.ControllerResultUtil;

@Controller
public class KViewTypeController {

	@Autowired
	KViewTypeService kViewTypeService;
	
	@RequestMapping("insertKViewType")
	@ResponseBody
	public String insertKViewType(String title,String icon,Integer num,String action,String viewtype,Integer sort,Integer status){
		int result = kViewTypeService.insertKViewType(title,icon,num, action, viewtype,sort,status);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("deleteKViewType")
	@ResponseBody
	public String deleteKViewType(Integer id){
		int result = kViewTypeService.deleteKViewType(id);
		if(result == 1){
			return ControllerResultUtil.SUCCEED;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("updateKViewType")
	@ResponseBody
	public String updateKViewType(Integer id,String title,String icon,Integer num,String action,String viewtype,Integer sort,Integer status){
		int result = kViewTypeService.updateKViewType(id, title,icon,num, action, viewtype,sort,status);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("selectKViewTypeList")
	@ResponseBody
	public List<KViewType> selectKViewTypeList(){
		return kViewTypeService.selectKViewTypeList();
	}
	
	
}
