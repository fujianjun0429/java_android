package com.zpkj.quick.manage.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zpkj.quick.manage.service.KAdminService;

@Controller
public class ManageController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ManageController.class);
	
	@Autowired
	KAdminService kAdminService;
	
	@RequestMapping("/index")
//	@ResponseBody
	public String index() {
		return "login";
	}
	@RequestMapping("/login")
	public String login() {
		return "login";
	}

	@RequestMapping("/doLogin")
	public String doLogin(String userName, String userPassword) {
		if (kAdminService.canEnter(userName, userPassword)) {
			return "index";
		}
		return "adminError";
	}
	
	@RequestMapping("/groupManager")
	public String groupManager() {
		return "groupManager";
	}
	
	//批量推荐
	@RequestMapping("/shopGroupPositionMng")
	public String shopGroupPositionMng() {
		System.out.println("shopGroupPositionMngSingle");
		return "shopGroupPositionMng";
	}
	
	//单个位置推荐
	@RequestMapping("/singleGroupPositionMng")
	public String shopGroupPositionMngSingle() {
		System.out.println("shopGroupPositionMngSingle");
		return "shopGroupPositionMngSingle";
	}
	
	@RequestMapping("/sdkSearchSuggest")
	public String sdkSearchSuggest() {
		return "sdkSearchSuggest";
	}
	
	@RequestMapping("/sdkDefaultTopic")
	public String sdkDefaultTopic() {
		return "sdkDefaultTopic";
	}
	
	@RequestMapping("/sdkLastOpen")
	public String sdkLastOpen() {
		return "sdkLastOpen";
	}

	@RequestMapping("/updateApk")
	public String updateApk() {
		return "updateApk";
	}
	
	@RequestMapping("/uploadRpk")
	public String uploadRpk() {
		return "uploadRpk";
	}
	
	@RequestMapping("/adMng")
	public String adMng() {
		return "adMng";
	}
	
    @RequestMapping("upDownList")
    public String quickInfoPage(){
        return "upDownList";
    }
    
    @RequestMapping("quickListPage")
    public String quickListPage(){
    	return "quickListPage";
    }
    
    @RequestMapping("quickEdit")
    public String quickEdit(){
    	return "quickEdit";
    }
    
    @RequestMapping("baseCatalog")
    public String baseCatalog(){
    	return "baseCatalog";
    }
    
    @RequestMapping("phoneSet")
    public String phoneSet(){
    	return "phoneSet";
    }
    
    @RequestMapping("quickTagInfo")
    public String quickTagInfo(){
    	return "quickTagInfo";
    }
    
    @RequestMapping("quickSpecial")
    public String quickSpecial(){
    	return "quickSpecial";
    }
    
    @RequestMapping("quickRecommend")
    public String quickRecommend(){
    	return "quickRecommend";
    }
    
    @RequestMapping("quickWords")
    public String quickWords(){
    	return "quickWords";
    }
    
    @RequestMapping("editQuick")
    public String editQuick(){
    	return "editQuick";
    }
    
    @RequestMapping("attrListPage")
    public String attrListPage(){
    	return "attrListPage";
    }
    
    @RequestMapping("addQuick")
    public String spuInfoPage(){
    	return "addQuick";
    }
    
	@RequestMapping("/recommends")
    public String getUserName(@ModelAttribute("viewType") String viewType){
        System.out.println("--viewType-- : " + viewType);
        
        return "recommends";
    }
   
}
