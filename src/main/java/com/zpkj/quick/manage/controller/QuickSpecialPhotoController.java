package com.zpkj.quick.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zpkj.quick.manage.pojo.QuickSpecialPhoto;
import com.zpkj.quick.manage.service.QuickSpecialPhotoService;
import com.zpkj.quick.manage.util.ControllerResultUtil;

@Controller
public class QuickSpecialPhotoController {

	@Autowired
	QuickSpecialPhotoService quickSpecialPhotoService;
	
	@RequestMapping("insertSpecialPhoto")
	@ResponseBody  //====@ResponseBody 当方法上面没有写ResponseBody,底层会将方法的返回值封装为ModelAndView对象。 当有ResponseBody，那么直接将字符串返回到客户端；如果是一个对象，会将对象转化为json串，然后返回到客户端
	public String insertSpecialPhoto(String info,String local,String jumpaddr,Integer specialId){
		int result = quickSpecialPhotoService.insertSpecialPhoto(info, local, jumpaddr, specialId);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("deleteSpecialPhoto")
	@ResponseBody
	public String deleteSpecialPhoto(Integer id){
		int result = quickSpecialPhotoService.deleteSpecialPhoto(id);
		if(result == 1){
			return ControllerResultUtil.SUCCEED;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("updateSpecialPhoto")
	@ResponseBody
	public String updateSpecialPhoto(Integer id,String info,String local,String jumpaddr,Integer specialId){
		int result = quickSpecialPhotoService.updateSpecialPhoto(id, info, local, jumpaddr, specialId);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("selectSpecialPhotoById")
	@ResponseBody
	public QuickSpecialPhoto selectSpecialPhotoById(Integer id){
		return quickSpecialPhotoService.selectSpecialPhotoById(id);
	}
	
	@RequestMapping("selectPhotoBySpecialId")
	@ResponseBody
	public List<QuickSpecialPhoto> selectPhotoBySpecialId(Integer specialId){
		return quickSpecialPhotoService.selectPhotoBySpecialId(specialId);
	}
	
	
	@RequestMapping("selectPhotoList")
	@ResponseBody
	public List<QuickSpecialPhoto> selectPhotoList(Integer specialId){
		return quickSpecialPhotoService.selectPhotoList(specialId);
	}
	
}
