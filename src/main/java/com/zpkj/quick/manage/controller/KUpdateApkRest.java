package com.zpkj.quick.manage.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.zpkj.quick.manage.pojo.CheckUpdateBean;
import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.pojo.GroupBatchApp;
import com.zpkj.quick.manage.pojo.KUpdateBean;
import com.zpkj.quick.manage.service.KUpdateApkService;
import com.zpkj.quick.manage.util.JSONUtils;

@RestController
public class KUpdateApkRest {
	private static final Logger LOGGER = LoggerFactory.getLogger(KUpdateApkRest.class);

	@Autowired
	KUpdateApkService kUpdateApkService;
	
	@RequestMapping("/selectUpdateApks")
	public List<KUpdateBean> selectUpdateApks(){
		return kUpdateApkService.selectUpdateApks();
	}
	
	@RequestMapping("/getUpdateApks")
	public GeneralResult getUpdateApks(@RequestBody List<CheckUpdateBean> apps){
		GeneralResult result = new GeneralResult();
		try {
			Gson gson = new Gson();
			
			LOGGER.error("getUpdateApks {}","apps="+gson.toJson(apps));
			result = kUpdateApkService.getUpdateApks(apps);
		} catch (Exception e) {
			result.setErrorMessage("getUpdateApks failed");
			result.setResultStatus(false);
			LOGGER.error("getUpdateApks {} ", e);
		}

		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/saveUpdateBean")
	public GeneralResult saveUpdateBean(
			@RequestParam("upfile") MultipartFile upfile,
			@RequestParam(value = "gameId", required = false )  int gameId,
			@RequestParam(value = "versionCode", required = false )  int versionCode,
			@RequestParam(value = "versionName", required = false )  String versionName,
			@RequestParam(value = "link", required = false )  String link,
			@RequestParam(value = "md5", required = false )  String md5,
			@RequestParam(value = "status", required = false )  int status,
			@RequestParam(value = "ssui", required = false )  int ssui,
			@RequestParam(value = "enginepkgname", required = false )  String enginepkgname,
			@RequestParam(value = "updateDesc", required = false )  String updateDesc
			) {//
		GeneralResult result = new GeneralResult();
		
		try
		{
			KUpdateBean updateBean = new KUpdateBean();
			updateBean.setGameId(gameId);
			updateBean.setVersionCode(versionCode);
			updateBean.setVersionName(versionName);
			updateBean.setLink(link); //保存文件成功时，会替换
			updateBean.setMd5(md5);
			updateBean.setStatus(status);
			updateBean.setEnginepkgname(enginepkgname);
			updateBean.setUpdateDesc(updateDesc);
			updateBean.setSsui(ssui);

			return kUpdateApkService.saveUpdateApks(upfile, updateBean);
		} catch (Exception e) {
			result.setErrorMessage("saveUpdateBean failed");
			result.setResultStatus(false);
			LOGGER.error("saveUpdateBean {} ", e);
		}
		
		return result;
		
	}
	
}
