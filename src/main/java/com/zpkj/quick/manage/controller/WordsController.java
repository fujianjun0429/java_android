package com.zpkj.quick.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zpkj.quick.manage.pojo.QuickWords;
import com.zpkj.quick.manage.service.WordsService;
import com.zpkj.quick.manage.util.ControllerResultUtil;

@Controller
public class WordsController {
	
	@Autowired
	WordsService quickWordsService;
	
	@RequestMapping("insertWords")
	@ResponseBody
	public String insertWords(String name,Integer status){
		int result = quickWordsService.insertWords(name, status);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("deleteWords")
	@ResponseBody
	public String deleteWords(Integer id){
		int result = quickWordsService.deleteWords(id);
		if(result == 1){
			return ControllerResultUtil.SUCCEED;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("updateWords")
	@ResponseBody
	public String updateWords(Integer id,String name,Integer status){
		int result = quickWordsService.updateWords(id, name, status);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("selectWordsById")
	@ResponseBody
	public QuickWords selectWordsById(Integer id){
		return quickWordsService.selectWordsById(id);
	}
	
	@RequestMapping("selectWordsList")
	@ResponseBody
	public List<QuickWords> selectWordsList(){
		return quickWordsService.selectWordsList();
	}
	
}
