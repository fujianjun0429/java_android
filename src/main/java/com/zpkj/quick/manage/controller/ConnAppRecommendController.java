package com.zpkj.quick.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zpkj.quick.manage.pojo.ConnAppRecommend;
import com.zpkj.quick.manage.service.ConnAppRecommendService;
import com.zpkj.quick.manage.util.ControllerResultUtil;

@Controller
public class ConnAppRecommendController {

	@Autowired
	ConnAppRecommendService connAppRecommendService;
	
	@RequestMapping("insertConnAppRecommend")
	@ResponseBody
	public String insertConnAppRecommend(Integer quickId,Integer recommendId){
		System.out.println("quickId="+quickId +"\t recommendId="+recommendId);
		int result = connAppRecommendService.insertConnAppRecommend(quickId, recommendId);
		if(result == 1){
			return ControllerResultUtil.SUCCEED;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("selectQuickByRecommend")
	@ResponseBody
	public List<ConnAppRecommend> selectQuickByRecommend(Integer recommendId){
		return connAppRecommendService.selectQuickByRecommend(recommendId);
	}
	
	@RequestMapping("deleteConnAppRecommend")
	@ResponseBody
	public String deleteConnAppRecommend(Integer quickId,Integer recommendId){
		int result = connAppRecommendService.deleteConnAppRecommend(quickId, recommendId);
		if(result == 1){
			return ControllerResultUtil.SUCCEED;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	
}
