package com.zpkj.quick.manage.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.pojo.Rpk;
import com.zpkj.quick.manage.service.RpkService;
import com.zpkj.quick.manage.util.IUtil;

@RestController
public class RpkRest {
	private static final Logger LOGGER = LoggerFactory.getLogger(RpkRest.class);

	@Autowired
	RpkService rpkService;
	
	@RequestMapping("/selectRpks")
	public List<Rpk> selectRpks(
			@RequestParam(value = "rpkName", required = false) String rpkName,
			@RequestParam(value = "rpkPkgName", required = false) String rpkPkgName) {
		return rpkService.selectRpks(rpkName, rpkPkgName);
	}
	
	@RequestMapping("/selectAllRpks")
	public List<Rpk> selectAllRpks() {
		return rpkService.selectAllRpks();
	}
	
	@RequestMapping("/deleteRpk")
	public GeneralResult deleteRpk(@RequestParam(value = "packageName", required = true) String packageName) {
		GeneralResult result = new GeneralResult();
		try {
			return rpkService.deleteRpk(packageName);
		} catch (Exception e) {
			result.setErrorMessage("deleteRpk failed");
			result.setResultStatus(false);
			LOGGER.error("deleteRpk {} ", e);
		}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/saveRpk")
	public GeneralResult saverpk(
			@RequestParam("rpkFile") MultipartFile rpkFile,
			@RequestParam("iconFile") MultipartFile iconFile,
			@RequestParam(value = "rpkId", required = false )  Integer rpkId,
			@RequestParam(value = "packageName", required = true )  String packageName,
			@RequestParam(value = "iconUrl", required = false )  String iconUrl,
			@RequestParam(value = "rpkName", required = false )  String rpkName,
			@RequestParam(value = "rpkUrl", required = false )  String rpkUrl,
			@RequestParam(value = "fileMd5", required = false )  String fileMd5,
			@RequestParam(value = "size", required = false )  Integer size,
			@RequestParam(value = "rpkVersionCode", required = false )  Integer rpkVersionCode,
			@RequestParam(value = "openCount", required = false )  Integer openCount,
			@RequestParam(value = "enable", required = false )  Integer enable
			) {
		GeneralResult result = new GeneralResult();
		LOGGER.error("saverpk {} ", packageName+"  openCount="+openCount);
		
		try
		{
			Rpk rpk = new Rpk();
			rpk.setRpkId(IUtil.check(rpkId));
			rpk.setPackageName(packageName);
			rpk.setIconUrl(iconUrl);
			rpk.setRpkName(rpkName); 
			rpk.setRpkUrl(rpkUrl) ;
			rpk.setFileMd5(fileMd5); 
			rpk.setSize(IUtil.check(size)); 
			rpk.setRpkVersionCode(IUtil.check(rpkVersionCode)); 
			rpk.setOpenCount(IUtil.check(openCount)); 
			rpk.setEnable(IUtil.check(enable)); 

			return rpkService.saveRpk(rpkFile, iconFile, rpk);
		} catch (Exception e) {
			result.setErrorMessage("saveRpk failed");
			result.setResultStatus(false);
			LOGGER.error("saverpk {} ", e);
		}
		
		return result;
		
	}
	
}
