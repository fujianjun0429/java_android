package com.zpkj.quick.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zpkj.quick.manage.pojo.TagCatalog;
import com.zpkj.quick.manage.service.TagCatalogService;
import com.zpkj.quick.manage.util.ControllerResultUtil;

@Controller
public class TagCatalogController {
	
	@Autowired
	TagCatalogService tagCatalogService;
	
	@RequestMapping("insertTagCatalog")
	@ResponseBody
	public String insertTagCatalog(String name){
		int result = tagCatalogService.insertTagCatalog(name);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("deleteTagCatalog")
	@ResponseBody
	public String deleteTagCatalog(Integer id){
		int result = tagCatalogService.deleteTagCatalog(id);
		if(result == 1){
			return ControllerResultUtil.SUCCEED;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("updateTagCatalog")
	@ResponseBody
	public String updateTagCatalog(Integer id,String name){
		int result = tagCatalogService.updateTagCatalog(id, name);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("selectTagCatalogById")
	@ResponseBody
	public TagCatalog selectTagCatalogById(Integer id){
		return tagCatalogService.selectTagCatalogById(id);
	}
	
	@RequestMapping("selectTagCatalogList")
	@ResponseBody
	public List<TagCatalog> selectTagCatalogList(){
		return tagCatalogService.selectTagCatalogList();
	}
	
}
