package com.zpkj.quick.manage.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.pojo.GroupBatchApp;
import com.zpkj.quick.manage.pojo.GroupIdBean;
import com.zpkj.quick.manage.pojo.GroupPositionApp;
import com.zpkj.quick.manage.pojo.GroupPositionBean;
import com.zpkj.quick.manage.service.GroupService;
import com.zpkj.quick.manage.util.IUtil;

@RestController
public class GroupRest {
	private static final Logger LOGGER = LoggerFactory.getLogger(GroupRest.class);
	private static final String TAG = GroupRest.class.getSimpleName()+"：{}";

	@Autowired
	GroupService groupService;
	
	@RequestMapping("/selectGroups")
	public List<GroupIdBean> selectGroups( 
			  @RequestParam(value = "productType", required = false ) String productType, 
			  @RequestParam(value = "groupId", required = false ) Integer groupId,//====注意不要int,否则传null时，转化会奔溃
			  @RequestParam(value = "onlyVorH", required = false ) Integer onlyVorH
			){
		
		return groupService.selectGroups(productType, IUtil.check(groupId), IUtil.check(onlyVorH));
	}
	
	@RequestMapping("/selectGroup")
	public GroupIdBean selectGroup( @RequestParam("viewType") String viewType){
		
		return groupService.selectGroup(viewType);
	}
	
	@RequestMapping( value = "/insertOrUpdateGroup")
	public GeneralResult insertOrUpdateGroup(
			   @RequestParam("groupId") Integer groupId, 
			   @RequestParam("title") String title, 
			   @RequestParam("info") String info, 
			   @RequestParam("sort") int sort, 
			   @RequestParam("descGroup") String descGroup,
			   @RequestParam("viewType") String viewType,
			   @RequestParam(value = "groupAction", required = false) String groupAction,
			   @RequestParam("productType") String productType,
			   @RequestParam("status") int status,
			   @RequestParam("startTime") String startTime,
			   @RequestParam("endTime") String endTime
		
			) {//  
		GeneralResult result = new GeneralResult();
		
		try{
			return groupService.insertOrUpdateGroup(groupId, title, info, sort, descGroup, viewType, groupAction, productType, status, startTime, endTime);
			
		} catch (Exception e) {
			result.setErrorMessage("insertOrUpdateGroup failed");
			result.setResultStatus(false);
			LOGGER.error("insertOrUpdateGroup {} ", e);
		}
		
		return result;
		
	}
	
	@RequestMapping("/deleteGroup")
	public GeneralResult deleteGroup(  @RequestParam("groupId") Integer groupId){
		GeneralResult result = new GeneralResult();
		try{
			return groupService.deleteGroup(groupId);
		} catch (Exception e) {
			result.setErrorMessage("saveUpdateBean failed");
			result.setResultStatus(false);
			LOGGER.error("deleteGroup {} ", e);
		}
		
		return result;
		
	}
	///////////////////////////position///////////////////////////////////////
	@RequestMapping("/selectGroupPositions")
	public List<GroupPositionBean> selectGroupPositions( 
			@RequestParam(value = "groupId", required =false) Integer groupId,
			@RequestParam(value="viewType",required = false) String viewType
			){
		LOGGER.debug(TAG, "viewType="+viewType+ " \t groupId="+groupId);
		return groupService.selectGroupPositions(viewType, IUtil.check(groupId));
	}
	
	@RequestMapping( value = "/insertOrUpdateGroupPosition")
	public GeneralResult insertOrUpdateGroupPosition(
			   @RequestParam(value = "id", required = false ) Integer id, 
			   @RequestParam("groupId") int groupId, 
			   @RequestParam("positionNo") int positionNo, 
			   @RequestParam("positionDesc") String positionDesc, 
			   @RequestParam("status") int status
			) {
		GeneralResult result = new GeneralResult();
		
		try{
			return groupService.insertOrUpdateGroupPosition(id, groupId, positionNo, positionDesc, status);
			
		} catch (Exception e) {
			result.setErrorMessage(" failed");
			result.setResultStatus(false);
			LOGGER.error("insertOrUpdateGroup {} ", e);
		}
		
		return result;
		
	}
	
	@RequestMapping("/deleteGroupPosition")
	public GeneralResult deleteGroupPosition(  @RequestParam("id") Integer id){
		GeneralResult result = new GeneralResult();
		try{
			return groupService.deleteGroupPosition(id);
		} catch (Exception e) {
			result.setErrorMessage(" failed");
			result.setResultStatus(false);
			LOGGER.error("deleteGroupPosition {} ", e);
		}
		
		return result;
		
	}
	///////////////////////positionApp///////////////////////////////////////////
	@RequestMapping("/insertApp2Position")
	GeneralResult insertOrUpdateApp2Position(
			@RequestParam(value = "id",required = false ) Integer id, 
			   @RequestParam(value = "positionId",required = false ) Integer positionId, 
			   @RequestParam(value = "appId",required = false ) Integer appId, 
			   @RequestParam(value = "topicId",required = false ) Integer topicId, 
			   @RequestParam(value = "hotWordsId",required = false ) Integer hotWordsId, 
			   @RequestParam(value = "h5Id",required = false )Integer h5Id, 
			   @RequestParam(value = "actionType", required = false ) String actionType, 
			   @RequestParam(value = "searchType", required = false )  Integer searchType, 
			   @RequestParam(value = "goUrl",required = false ) String goUrl, 
			   @RequestParam(value = "startTime",required = false ) String startTime, 
			   @RequestParam(value = "endTime",required = false ) String endTime, 
			   @RequestParam(value = "peoples",required = false ) Integer peoples,
			   @RequestParam(value = "imageUrl",required = false ) String imageUrl ,
			   @RequestParam(value = "bak1",required = false ) String bak1 , //用于存放机组ID
			   @RequestParam(value = "selectType",required = false ) String selectType , 
			   @RequestParam(value = "selectId",required = false ) Integer selectId , 
				@RequestParam(value = "imageFile",required = false) MultipartFile imageFile
			   ){
		GeneralResult result = new GeneralResult();
		try{
			LOGGER.error("insertOrUpdateApp2Position {} ", positionId+"/t"+appId+topicId+hotWordsId+h5Id+"selectType:"+selectType+selectId);
			return groupService.insertOrUpdateApp2Position(selectType,  IUtil.check(selectId),
					IUtil.check(id), IUtil.check(positionId), IUtil.check(appId), IUtil.check(topicId), IUtil.check(hotWordsId), 
					IUtil.check(h5Id), actionType, IUtil.check(searchType), goUrl, startTime, endTime, IUtil.check(peoples), bak1,imageUrl,imageFile);
		} catch (Exception e) {
			result.setErrorMessage(" failed");
			result.setResultStatus(false);
			LOGGER.error("insertApp2Position {} ", e);
		}
		
		return result;
		
	}
	
	@RequestMapping("/updateAllBak1")
	GeneralResult updateAllBak1(
			@RequestParam(value = "bak1",required = false ) String bak1  //用于存放机组ID
			){
		GeneralResult result = new GeneralResult();
		try{
			return groupService.updateAllBak1( bak1);
		} catch (Exception e) {
			result.setErrorMessage("failed");
			result.setResultStatus(false);
			LOGGER.error("updateAllBak1: {} ", e);
		}
		
		return result;
	}

	@RequestMapping("/selectAppOfPositions")
	List<GroupPositionApp> selectAppOfPositions(
			@RequestParam(value = "positionId", required = false) Integer positionId) {
		return groupService.selectAppOfPositions(IUtil.check(positionId));
	}

	@RequestMapping("/deleteAppOfPosition")
	GeneralResult deleteAppOfPosition(@RequestParam("id") Integer id) {
		GeneralResult result = new GeneralResult();
		try {
			return groupService.deleteAppOfPosition(id);
		} catch (Exception e) {
			result.setErrorMessage(" failed");
			result.setResultStatus(false);
			LOGGER.error("deleteAppOfPosition {} ", e);
		}

		return result;

	}
	
	@RequestMapping(value = "/batch/appOfPosition", method = RequestMethod.POST)
	public GeneralResult batchInsertApp(
			@RequestParam(value = "groupId", required = false) Integer groupId,
			@RequestBody List<GroupBatchApp> datas) //====接口接收对象参数 RequestBody， 前端post一个json字符串，不用key
	{
		GeneralResult result = new GeneralResult();
		try {
//			for (GroupBatchApp item : datas) {
//				LOGGER.error(TAG, item.getAppId() +" item positionNo:"+item.getPositionNo()+"\t"+item.getKey());
//			}
			LOGGER.debug(TAG, "groupId="+groupId);
			
			groupService.batchInsertApp(groupId,  datas);
			
			result.setResultStatus(true);
		} catch (Exception e) {
			result.setResultStatus(false);
			LOGGER.error("Server Exception!", e);
			result.setErrorMessage("服务器内部错误!"+e.getMessage());
		}
		
		return result;
	}
	
	@RequestMapping("/selectAppOfGroup")
	List<GroupBatchApp> selectAppOfGroup(
			@RequestParam(value = "viewType", required = false) String viewType,
			@RequestParam(value = "groupId", required = false) Integer groupId
			) {
		LOGGER.error(TAG, " selectAppOfGroup groupId=:"+groupId);
		return groupService.selectAppOfGroup(viewType, IUtil.check(groupId));
	}
	
	
}
