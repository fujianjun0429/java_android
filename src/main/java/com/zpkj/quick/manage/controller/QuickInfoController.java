package com.zpkj.quick.manage.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.pojo.QuickInfo;
import com.zpkj.quick.manage.service.QuickInfoService;
import com.zpkj.quick.manage.util.ControllerResultUtil;

@Controller
public class QuickInfoController {
	private static final Logger LOGGER = LoggerFactory.getLogger(QuickInfoController.class);
	
	  @Autowired
	  QuickInfoService quickInfoService;
	
	  
	  @RequestMapping("quickInfoList")
	  @ResponseBody
	  public List<QuickInfo> getQuickList(String appName, Integer page, Integer status_id){
		  System.out.println("page="+page);
	      List<QuickInfo> quickInfos = quickInfoService.getQuickList(appName, page,status_id);
	      return quickInfos;
	      
	  }
	  
	  @RequestMapping("onlineQuickInfoList")
	  @ResponseBody
	  public List<QuickInfo> onlineQuickInfoList(String appName, Integer page){
		  System.out.println("page="+page);
		  List<QuickInfo> quickInfos = quickInfoService.onlineQuickInfoList(appName, page);
		  return quickInfos;
		  
	  }
	  
	  @RequestMapping("allUpOrDown")
	  @ResponseBody
	  public int allUpOrDown( Integer onOrOff){
		  System.out.println("onOrOff="+onOrOff);
		  int r = quickInfoService.allUpOrDown( onOrOff);
		  return r;
		  
	  }
	  
	  @RequestMapping("quickInfo")
	  @ResponseBody
	  public QuickInfo getQuickInfo(Integer id){
		  QuickInfo quickInfo = quickInfoService.getQuickInfoById(id);
		  return quickInfo;
	  }
	  
	  @RequestMapping("quickInfoListByWord")
	  @ResponseBody
	  public List<QuickInfo> getQuickListByWord(String name){
		  List<QuickInfo> quickInfos = quickInfoService.getQuickListByWord(name);
		  return quickInfos;
	  }
	  
	 
	  @RequestMapping("quickInfoListByKey")
	  @ResponseBody
	  public List<QuickInfo> getQuickListByKey(String key){
		  List<QuickInfo> quickInfos = quickInfoService.getQuickListByKey(key);
		  return quickInfos;
	  }
	  
	  
	  @RequestMapping("setQuickStatusOnline")
	  @ResponseBody
	  public String setQuickStatusOnline(Integer id){
		  int result = quickInfoService.setQuickStatusOnline(id);
		  if(result == 1){
			  return ControllerResultUtil.SUCCEED;
		  }else{
			  return ControllerResultUtil.ERROR_RESULT;
		 }
		  
	  }
	  
	  @RequestMapping("setQuickStatusOffline")
	  @ResponseBody
	  public String setQuickStatusOffline(Integer id){
		  int result = quickInfoService.setQuickStatusOffline(id);
		  if(result == 1){
			  return ControllerResultUtil.SUCCEED;
		  }else{
			  return ControllerResultUtil.ERROR_RESULT;
		  }
	  }
	  
	  
	  @RequestMapping("/quickEditOrAdd")
	  @ResponseBody
	  public GeneralResult quickEditOrAdd(
			    @RequestParam("iconFile") MultipartFile iconFile,
				@RequestParam(value = "id", required = false )  Integer id,
				@RequestParam(value = "name", required = false )  String name,
				@RequestParam(value = "key", required = false )  String key,
				@RequestParam(value = "info", required = false )  String info,
				@RequestParam(value = "engineUri", required = false )  String engineUri,
				@RequestParam(value = "enginePackage", required = false )  String enginePackage,
				@RequestParam(value = "engineAction", required = false )  String engineAction,
				@RequestParam(value = "type", required = false )  String type,
				@RequestParam(value = "icon", required = false )  String icon,
				@RequestParam(value = "minVersion", required = false )  String minVersion,
				@RequestParam(value = "statusId", required = false )  Integer statusId
				) {
			GeneralResult result = new GeneralResult();
			try {
				LOGGER.error("quickEditOrAdd {}","type="+type);
				result = quickInfoService.quickEditOrAdd(iconFile, id, name, key, info,
						engineUri, enginePackage, engineAction,
						type, icon, minVersion, statusId);
			} catch (Exception e) {
				result.setErrorMessage("quickEditOrAdd failed");
				result.setResultStatus(false);
				LOGGER.error("quickEditOrAdd {} ", e);
			}
	
			return result;
	  }
	
}
