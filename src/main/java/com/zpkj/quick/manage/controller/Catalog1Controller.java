package com.zpkj.quick.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zpkj.quick.manage.pojo.Catalog1;
import com.zpkj.quick.manage.service.Catalog1Service;
import com.zpkj.quick.manage.util.ControllerResultUtil;

@Controller
public class Catalog1Controller {
	
	@Autowired
	Catalog1Service catalog1Service;
	
	@RequestMapping("catalog1List")
	@ResponseBody
	public List<Catalog1> Catalog1List(){
		List<Catalog1> list = catalog1Service.getCatalog1List();
		return list;
	}
	
	@RequestMapping("addCatalog1")
	@ResponseBody
	public String addCatalog1(String catalog1Name){
		int result = catalog1Service.addCatalog1(catalog1Name);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("deleteCatalog1")
	@ResponseBody
	public String deleteCatalog1(Integer catalog1Id){
		int result = catalog1Service.deleteCatalog1(catalog1Id);
		if(result == 1){
			return ControllerResultUtil.SUCCEED;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("updateCatalog1")
	@ResponseBody
	public String updateCatalog1(Integer id,String name){
		int result = catalog1Service.updateCatalog1(id, name);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("catalog1ById")
	@ResponseBody
	public Catalog1 catalog1ById(Integer catalog1Id){
		return catalog1Service.getCatalog1(catalog1Id);
	}
	
}
