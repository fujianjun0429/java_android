package com.zpkj.quick.manage.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zpkj.quick.manage.service.H5Service;
import com.zpkj.quick.manage.util.IUtil;
import com.zpkj.quick.manage.util.NullUtil;
import com.zpkj.quick.manage.util.StringUtil;

@Controller
public class ZhifuControll {
	private static final Logger LOGGER = LoggerFactory.getLogger(ZhifuControll.class);
	private static final String TAG = ZhifuControll.class.getSimpleName()+"{}";
	
	@Autowired
	H5Service h5Service;
	
	@RequestMapping("/zhifu")
//	@ResponseBody
	public String zhifu(HttpServletResponse response, 
			HttpServletRequest request,
			@RequestParam("nRoleId") String nRoleId, 
			@RequestParam("nServerId") String nServerId,
			@RequestParam("szGroup") String szGroup,
			@RequestParam("nGroupIndex") String nGroupIndex
			) {
		LOGGER.error(TAG, "nRoleId=" + nRoleId
				+"\tnServerId="+nServerId
				+"\tszGroup="+szGroup
				+"\tnGroupIndex="+nGroupIndex
				);
//		local nRoleId = pPlayer.dwID;									 
//		local nServerId = Sdk:GetServerId();	
//		local GoodsType2 = "card";											
//		local CardType1 = "Week";
//		"http://182.61.28.245:30702/zhifu?nServerId=22&nRoleId=444422&szGroup=aaaa&nGroupIndex=aaaaaaaa";
	
		String price =h5Service.getPrice( nRoleId, szGroup, nGroupIndex);
		String remark = nRoleId + "_" + szGroup + "_" + nGroupIndex + "_" + nServerId;
		LOGGER.error(TAG, "price=" + price);
		
		//先注释，正式上线时打开
		if (!NullUtil.isNull(price)) {
			String zhifuUrl = h5Service.getHeepayUrlWX(price, nRoleId, szGroup, remark);
			LOGGER.error(TAG, "zhifuUrl=" + zhifuUrl);

			try {
				if (!NullUtil.isNull(zhifuUrl)) {
					response.sendRedirect(zhifuUrl);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return "zhifu";
	}
	
	
	@RequestMapping(value = "/zhifu/buynotify_heepay")
	public String buynotify_heepay(
			@RequestParam(value = "result", required = false) Integer result,
			@RequestParam(value = "pay_message", required = false) String pay_message,
			@RequestParam(value = "agent_id", required = false) String agent_id,
			@RequestParam(value = "jnet_bill_no", required = false) String jnet_bill_no,
			@RequestParam(value = "agent_bill_id", required = false) String agent_bill_id,
			@RequestParam(value = "pay_amt", required = false) String pay_amt,
			@RequestParam(value = "pay_type", required = false) String pay_type,
			@RequestParam(value = "sign", required = false) String sign,
			@RequestParam(value = "remark", required = false) String remark
			) {
		String reStr ="error";
		try {
			LOGGER.error("buynotify_heepay{}", "agent_bill_id=" + agent_bill_id
					+ "\t pay_message =" + pay_message
					+ "\t agent_id =" + agent_id
					+ "\t jnet_bill_no =" + jnet_bill_no
					+ "\t pay_amt =" + pay_amt
					+ "\t pay_type =" + pay_type
					+ "\t sign =" + sign
					+ "\t remark =" + remark
					);
			String merId = agent_id;
			String appId = agent_id;
			String merOrderId = agent_bill_id;
			int orderStatus = -1;//只有支付成功时会回调，目前改成100表明成功
			if(1== IUtil.check(result)){
				orderStatus = 100;
			}
			String amount =  StringUtil.string2Float(pay_amt)*100 + "";
			String signValue = sign;
			String orderTime = StringUtil.getFormatDate_HH_MM_SS(System.currentTimeMillis());;
			String productName =remark;
			String productDesc = agent_bill_id;
			String extInfo = remark;
			
		   reStr = h5Service.buynotify(merId, amount, merOrderId, appId, orderStatus, signValue, orderTime,
					productName, productDesc, extInfo);
		   reStr = "ok";
			
		} catch (Exception e) {
			LOGGER.error("buynotify_heepay Server Exception{}", e);
			reStr = "error";
		}

		return reStr;
	}
	
	/**
	 * 购买成功调用
	 */
	@RequestMapping(value = "/zhifu/buySuccessGet", method = RequestMethod.GET)
	@ResponseBody
	public String buySuccessGet() {
		return "\r\n  购买成功，请到邮件附件收取！";
	}
	
   
}
