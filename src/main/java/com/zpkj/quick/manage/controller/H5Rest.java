package com.zpkj.quick.manage.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.pojo.H5Bean;
import com.zpkj.quick.manage.service.H5Service;
import com.zpkj.quick.manage.util.IUtil;
import com.zpkj.quick.manage.util.QConstants;

@RestController
public class H5Rest {
	private static final Logger LOGGER = LoggerFactory.getLogger(H5Rest.class);
	private static final String TAG = H5Rest.class.getSimpleName()+"{}";

	@Autowired
	H5Service h5Service;
	
	
	@RequestMapping("/insertH5")
	GeneralResult insertH5(
			@RequestParam("id") Integer id, 
			@RequestParam("name") String name,
			@RequestParam("url") String url,
			@RequestParam("imageUrl") String imageUrl,
			@RequestParam("status") Integer status) {

		GeneralResult result = new GeneralResult();
		try {
			result = h5Service.insertH5(name, url, imageUrl, status);
		} catch (Exception e) {
			LOGGER.error(TAG, e);
			result.setResultStatus(false);
			result.setErrorMessage(QConstants.ERROR + e.getMessage());
			return result;
		}
		return result;
	}
	
	@RequestMapping("/deleteH5")
	GeneralResult deleteH5(@RequestParam("id") Integer id) {
		GeneralResult result = new GeneralResult();
		try {
			int iId = IUtil.check(id);
			result = h5Service.deleteH5(iId);
		} catch (Exception e) {
			LOGGER.error(TAG, e);
			result.setResultStatus(false);
			result.setErrorMessage(QConstants.ERROR + e.getMessage());
			return result;
		}
		return result;
	}

	@RequestMapping("/selectH5s")
	List<H5Bean> selectH5s() {
		return h5Service.selectH5s();
	}
	
}
