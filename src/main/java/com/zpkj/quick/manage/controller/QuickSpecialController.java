package com.zpkj.quick.manage.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zpkj.quick.manage.pojo.QappResult;
import com.zpkj.quick.manage.pojo.QuickSpecial;
import com.zpkj.quick.manage.pojo.SpecialApp;
import com.zpkj.quick.manage.service.QuickSpecialService;
import com.zpkj.quick.manage.util.ControllerResultUtil;
import com.zpkj.quick.manage.util.IUtil;
import com.zpkj.quick.manage.util.QConstants;

@Controller
public class QuickSpecialController {
	private static final Logger LOGGER = LoggerFactory.getLogger(QuickSpecialController.class);
	private static final String TAG =QuickSpecialController.class.getSimpleName();
	
	@Autowired
	QuickSpecialService quickSpecialService;
	
	@RequestMapping("addOrEditSpecial")
	@ResponseBody
	public QappResult addOrEditSpecial(Integer id, 
			@RequestParam("name") String name, 
			@RequestParam("info") String info, 
			@RequestParam("isdefault") Integer isdefault, 
			@RequestParam("startTime") String startTime,
			@RequestParam("endTime") String endTime, 
			@RequestParam("specialIcon") String specialIcon,
			@RequestParam("iconFile") MultipartFile iconFile
			) {
		QappResult result = new QappResult();
		try {
			int iId = IUtil.check(id);
			if (iId <= 0) {
				result = quickSpecialService.insertSpecial(name, info, isdefault, specialIcon, startTime, endTime,
						iconFile);
			} else {
				result = quickSpecialService.updateSpecial(iId, name, info, isdefault, specialIcon, startTime, endTime,
						iconFile);
			}
		} catch (Exception e) {
			LOGGER.error(TAG, e);
			result.setSuccess(false);
			result.setMsg(QConstants.ERROR + e.getMessage());
			return result ;
		}
		
		
		return result ;
		
	}
	
//	@RequestMapping("insertSpecial")
//	@ResponseBody
//	public String insertSpecial(String name,String info,Integer isdefault){
//		int result = quickSpecialService.insertSpecial(name, info,isdefault);
//		if(result == 1){
//			return ControllerResultUtil.SUCCEED_RESULT;
//		}else{
//			return ControllerResultUtil.ERROR_RESULT;
//		}
//	}
	
	@RequestMapping("deleteSpecial")
	@ResponseBody
	public String deleteSpecial(Integer id){
		int result = quickSpecialService.deleteSpecial(id);
		if(result == 1){
			return ControllerResultUtil.SUCCEED;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
//	@RequestMapping("updateSpecail")
//	@ResponseBody
//	public String updateSpecail(Integer id,String name,String info,Integer isdefault){
//		int result = quickSpecialService.updateSpecial(id, name, info,isdefault);
//		if(result == 1){
//			return ControllerResultUtil.SUCCEED_RESULT;
//		}else{
//			return ControllerResultUtil.ERROR_RESULT;
//		}
//	}
	
	@RequestMapping("selectSpecialById")
	@ResponseBody
	public QuickSpecial selectSpecialById(Integer id){
		return quickSpecialService.selectSpecialById(id);
	}
	
	@RequestMapping("selectSpecialList")
	@ResponseBody
	public List<QuickSpecial> selectSpecialList(){
		return quickSpecialService.selectSpecialList();
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////
	@RequestMapping("/selectSpecialApps")
	@ResponseBody
	public List<SpecialApp> selectSpecialApps(	
			@RequestParam(value = "specialId", required = false )  Integer specialId
			){
		return quickSpecialService.selectSpecialApps(specialId);
	}
	
	
	@RequestMapping("/addAppToSpecial")
	@ResponseBody
	public QappResult addAppToSpecial(	
			@RequestParam(value = "specialId", required = false )  Integer specialId,
			@RequestParam(value = "appId", required = false )  Integer appId
			){
		QappResult result = new QappResult();
		try {
			
			result = quickSpecialService.addAppToSpecial(specialId, appId);
		} catch (Exception e) {
			LOGGER.error(TAG, e);
			result.setSuccess(false);
			result.setMsg(QConstants.ERROR + e.getMessage());
			return result ;
		}
		
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);
		return result ;
	}
	
	@RequestMapping("/deleteAppToSpecial")
	@ResponseBody
	public QappResult deleteAppToSpecial(	
			@RequestParam(value = "specialId", required = false )  Integer specialId,
			@RequestParam(value = "appId", required = false )  Integer appId
			){
		QappResult result = new QappResult();
		try {
			
			result = quickSpecialService.deleteAppToSpecial(specialId, appId);
		} catch (Exception e) {
			LOGGER.error(TAG, e);
			result.setSuccess(false);
			result.setMsg(QConstants.ERROR + e.getMessage());
			return result ;
		}
		
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);
		return result ;
	}
	
}
