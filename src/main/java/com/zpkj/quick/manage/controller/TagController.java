package com.zpkj.quick.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zpkj.quick.manage.pojo.QuickTag;
import com.zpkj.quick.manage.service.TagService;
import com.zpkj.quick.manage.util.ControllerResultUtil;

@Controller
public class TagController {

	@Autowired
	TagService quickTagService;
	
	@RequestMapping("insertQuickTag")
	@ResponseBody
	public String insertQuickTag(String name,Integer catalogId){
		int result = quickTagService.insertQuickTag(name, catalogId);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("deleteQuickTag")
	@ResponseBody
	public String deleteQuickTag(Integer id){
		int result = quickTagService.deleteQuickTag(id);
		if(result == 1){
			return ControllerResultUtil.SUCCEED;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("updateQuickTag")
	@ResponseBody
	public String updateQuickTag(Integer id,String name,Integer catalogId){
		int result = quickTagService.updateQuickTag(id, name, catalogId);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("selectTagById")
	@ResponseBody
	public QuickTag selectTagById(Integer id){
		return quickTagService.selectTagById(id);
	}
	
	@RequestMapping("selectTagByCatalogId")
	@ResponseBody
	public List<QuickTag> selectTagByCatalogId(Integer catalogId){
		return quickTagService.selectTagByCatalogId(catalogId);
	}
	
	@RequestMapping("selectTagList")
	@ResponseBody
	public List<QuickTag> selectTagList(){
		return quickTagService.selectTagList();
	}
	
}
