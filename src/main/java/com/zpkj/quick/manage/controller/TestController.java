package com.zpkj.quick.manage.controller;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zpkj.quick.manage.pojo.QappResult;
import com.zpkj.quick.manage.util.StringUtil;

@Controller
@RequestMapping("/testParent/")  //类级别@RequestMapping，代表以下所有url请求需中间加 ‘/testParent’
public class TestController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);
	
	 @ModelAttribute  //====所有 的请求前，会先运行这个@ModelAttribute标注的函数 。并可接受参数 
     public void populateModel(@RequestParam(value = "abc", required = false) String abc, Model model) {  
        model.addAttribute("attributeName", abc);  
        model.addAttribute("my_attributeName", "aaaaaaaamy_attributeName");  
        System.out.println("\n\n abc="+abc);
     } 
	
//	默认 RequestMapping("url")即为value的值；  也可以显式说明 RequestMapping(value="url")
//	 consumes:指定处理请求的提交内容类型（Content-Type），例如application/json,text/html；
//	 produces:指定返回的内容类型，仅当request请求头中的（Accept）类型中包含该指定类型才返回；
//	 @PathVariable 请求url中的参数绑定给String petId
	@RequestMapping(value ="/test/{petId}", method = RequestMethod.GET /*, consumes="application/json" */)
	public String test(HttpServletRequest request, @PathVariable String petId) {
		//====获取request请求头
//		String header = request.getHeader("X-Forwarded-For");
//
//		//====获取request 请求的 post body
//		ServletInputStream inputStream = null;
//		try {
//			inputStream = request.getInputStream();
//			StringUtil.printBody(inputStream);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//        //====获取request 参数Map
//		Map properties = request.getParameterMap();
//	    StringUtil.printMap(properties);
//
		return "test";//在没有配置的情况下，return "hello”; 或者return "hello.html"都可以，它们都会到templates/hello.html去。
	}
	
	
    @RequestMapping("/test1")
    @ResponseBody
    public String test1(Model model){
    	System.out.println("\n\n my_attributeName="+model.asMap().get("my_attributeName"));
    	return "test1";
    }
    
    @RequestMapping("/testResponeHeader")
    @ResponseBody
	public ResponseEntity<QappResult> test2(@RequestParam(value="id",required=false) Integer id, HttpServletResponse response) {
    	//添加返回header
//    	response.setHeader("header_test", "header_test");
//    	response.setHeader("header_test2", "header_test2");
    	return ResponseEntity
                .ok()
                .cacheControl(CacheControl.maxAge(10*60, TimeUnit.SECONDS))//10分钟缓存,http级缓存，时间内不会更新数据给客户端
                .eTag("testEtag") // 版本
                .body(new QappResult());
    	
    }

}
