package com.zpkj.quick.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zpkj.quick.manage.pojo.PhoneType;
import com.zpkj.quick.manage.service.PhoneTypeService;
import com.zpkj.quick.manage.util.ControllerResultUtil;

@Controller
public class PhoneTypeController {
	
	@Autowired
	PhoneTypeService phoneTypeService;
	
	@RequestMapping("addPhoneType")
	@ResponseBody
	public String addPhoneType(String name,Integer groupId){
		int result = phoneTypeService.addPhoneType(name, groupId);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("deletePhoneType")
	@ResponseBody
	public String deletePhoneType(Integer id){
		int result = phoneTypeService.deletePhoneType(id);
		if(result == 1){
			return ControllerResultUtil.SUCCEED;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("updatePhoneType")
	@ResponseBody
	public String updatePhoneType(Integer id,String name,Integer groupId){
		int result = phoneTypeService.updatePhoneType(id,name,groupId);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("selectPhoneTypeById")
	@ResponseBody
	public PhoneType selectPhoneTypeById(Integer id){
		return phoneTypeService.selectPhoneTypeById(id);
	}
	
	@RequestMapping("selectPhoneTypeByGroupId")
	@ResponseBody
	public List<PhoneType> selectPhoneTypeByGroupId(Integer groupId){
		List<PhoneType> list = phoneTypeService.selectPhoneTypeByGroup(groupId);
		return list;
	}
	
	@RequestMapping("selectPhoneTypeList")
	@ResponseBody
	public List<PhoneType> selectPhoneTypeList(String name){
		return phoneTypeService.selectPhoneTypeList(name);
	}
	
}
