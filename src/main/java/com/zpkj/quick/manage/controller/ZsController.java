package com.zpkj.quick.manage.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zpkj.quick.manage.pojo.QappResult;
import com.zpkj.quick.manage.service.TestService;
import com.zpkj.quick.manage.service.ZsService;
import com.zpkj.quick.manage.util.QConstants;

@RestController
@RequestMapping(value = "/tts/")
public class ZsController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ZsController.class);
	private static final String TAG = ZsController.class.getSimpleName()+":{}";
	
	@Autowired
	ZsService zsService;
	
	@Autowired
	TestService   testService;
	
	@RequestMapping(value = "/testHttp")
	public QappResult testHttp() {
		QappResult result = new QappResult();
		try {
//			result = zsService.testHttp();
			
//			testService.testArr_n_k();
//			testService.testArr_maopao();
			
//			testService.testBinarySearch();
//			testService.printTwoArr();
//			testService.testSubSumArr();
			testService.testCharPrint();
			
		} catch (Exception e) {
			LOGGER.error(TAG, "Server Exception! testHttp  内部错误", e);
		}

		return result;
	}
	

	@RequestMapping(value = "/helper" )
	public QappResult hepler(
			@RequestParam(value = "imei", required = false) String imei,
			@RequestParam(value = "brand", required = false) String brand,
			@RequestParam(value = "model", required = false) String model,
			@RequestParam(value = "request_id", required = false) String request_id,
			@RequestParam(value = "timestamp", required = false) String timestamp
			) {
		QappResult result = new QappResult();
		try {
			result = zsService.helper();
		} catch (Exception e) {
			result.setCode(QConstants.ERROR_CODE);
			LOGGER.error(TAG, "Server Exception!", e);
			result.setMsg("服务器内部错误!");
			result.setSuccess(false);
		}
		
		result.setRequest_id(request_id);
		return result;
	}
	
	
}
