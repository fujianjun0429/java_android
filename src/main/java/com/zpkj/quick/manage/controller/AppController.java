package com.zpkj.quick.manage.controller;

import com.zpkj.quick.manage.pojo.AjaxResult;
import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.service.IAppService;
import com.zpkj.quick.manage.util.http.HttpDoRequest;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/tvbox/app")
public class AppController
{
    private static final Logger logger = LoggerFactory.getLogger(AppController.class);
//    private String prefix = "tvbox/app";
    private int playCount = 0;

    @Autowired
    private IAppService meAppService;

    @Autowired
    HttpDoRequest httpDoRequest;

    @GetMapping("/getConfig")
    @ResponseBody
    public GeneralResult getConfig(String packageName,
                                   String versionCode,
                                   String imei,
                                   String androidId,
                                   String mac,
                                   String imsi,
                                   String channel) {
        return meAppService.getConfig(packageName, versionCode, imei, androidId,
                mac, imsi, channel);
    }

    @GetMapping("/getAppsAndSites")
    @ResponseBody
    public String getAppsAndSites(String packageName, String versionCode) {
        logger.info("getAppsAndSites  packageName=" + packageName + " versionCode=" + versionCode);
        return meAppService.getAppsAndSites(packageName, versionCode);
    }

    @GetMapping("/site_json")
    @ResponseBody
    public String getSiteJson() {
        return meAppService.getSiteJson();
    }

    @GetMapping("/getAppsAndSitesSe")
    @ResponseBody
    public String getAppsAndSitesSe() {
        return meAppService.getAppsAndSitesSe();
    }

    @GetMapping("/getInitInfo")
    @ResponseBody
    public GeneralResult getAd(String packageName, String versionCode, String channel, String androidId) {
        logger.info("getInitInfo ad  packageName=" + packageName + " versionCode=" + versionCode + " androidId=" + androidId);
        GeneralResult result = new GeneralResult();
        try {
            result = meAppService.getAd(packageName, versionCode, channel);
        } catch (Exception e) {
            result.setResultStatus(false);
            result.setErrorMessage("服务器错误!");
        }
        return result;
    }

    @ApiOperation("获取升级信息")
    @GetMapping("/getUpdateInfo")
    @ResponseBody
    public GeneralResult getUpdateInfo(String packageName, String versionCode, String channel, String androidId) {
        logger.info( "getUpdateInfo  packageName=" + packageName + " versionCode=" + versionCode + " androidId=" + androidId);
        GeneralResult result = new GeneralResult();
        try {
            result = meAppService.getUpdateInfo(packageName, versionCode, channel);
        } catch (Exception e) {
            result.setResultStatus(false);
            result.setErrorMessage("服务器错误!");
        }
        return result;
    }

    @ApiOperation("获取热词")
    @GetMapping("/hotWords")
    @ResponseBody
    public String hotWords(String versionCode, String androidId, String searchWord) {
        logger.info("hotWords  versionCode =" + versionCode
                + " androidId=" + androidId
                + " searchWord=" + searchWord
        );
        return meAppService.hotWords(versionCode, androidId);
    }

    @ApiOperation("上报播放结果")
    @PostMapping("/upPlayResult")
    @ResponseBody
    public GeneralResult upPlayResult(@RequestBody Map<String, Object> paramsMap) {
        String errMap = (String) paramsMap.get("errorMap");
        String successMap = (String) paramsMap.get("successMap");
        String androidId = (String) paramsMap.get("androidId");
        String versionCode = (String) paramsMap.get("versionCode");
        String versionName = (String) paramsMap.get("versionName");
        String pre = "upPlayResult  versionCode=" + versionCode
                + " versionName=" + versionName
                + " androidId=" + androidId
                + " playCount=" + (++playCount);
        logger.info(pre + " \terrMap=" + errMap);
        logger.info(pre + " \tsuccessMap=" + successMap);
        GeneralResult result = new GeneralResult();
        result.setResultStatus(true);
        return result;
    }

    @ApiOperation("测试get")
    @GetMapping("/get")
    @ResponseBody
    public AjaxResult testGet() {
        logger.info("\t testGet start");
        HashMap<String, String> map = new HashMap<>();
        map.put("offset", "" + 1);
        map.put("count", "" + 2);
//        HttpDoRequest.getInstance().doGetRequest("http://localhost:8081/system/me/list",map);
//        HttpDoRequest.getInstance().doPostRequest("http://localhost:8081/system/me/list",map);
        String result =  httpDoRequest.doPostMapInJson("http://localhost:36036/system/me/post",map);
        logger.info("post result ="+result);
        return AjaxResult.success(map);
    }

    @PostMapping("/post")
    @ResponseBody
    public AjaxResult testPost() {
        HashMap<String, String> map = new HashMap<>();
        map.put("offset", "" + 1);
        map.put("count", "" + 2);
        return AjaxResult.success(map);
    }

    @RequestMapping("/noGetPost")
    @ResponseBody
    public AjaxResult noGetPost() {
        HashMap<String, String> map = new HashMap<>();
        map.put("noGetPost", "" + 1);
        map.put("noGetPost2", "" + 2);
        return AjaxResult.success(map);
    }

}
