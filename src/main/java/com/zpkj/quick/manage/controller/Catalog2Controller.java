package com.zpkj.quick.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zpkj.quick.manage.pojo.Catalog2;
import com.zpkj.quick.manage.service.Catalog2Service;
import com.zpkj.quick.manage.util.ControllerResultUtil;

@Controller
public class Catalog2Controller {
	
	@Autowired
	Catalog2Service catalog2Service;
	
	@RequestMapping("catalog2ByCatalog1Id")
	@ResponseBody
	public List<Catalog2> catalog2ByCatalog1Id(Integer catalog1Id){
		List<Catalog2> list = catalog2Service.getCatalog2ByCatalog1Id(catalog1Id);
		return list;
	}
	
	@RequestMapping("addCatalog2")
	@ResponseBody
	public String addCatalog2(String name,Integer catalog1){
		int result = catalog2Service.addCatalog2(name, catalog1);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("catalog2List")
	@ResponseBody
	public List<Catalog2> catalog2List(){
		List<Catalog2> list = catalog2Service.getCatalog2List();
		return list;
	}
	
	@RequestMapping("deleteCatalog2")
	@ResponseBody
	public String deleteCatalog2(Integer catalog2Id){
		int result = catalog2Service.deleteCatalog2(catalog2Id);
		if(result == 1){
			return ControllerResultUtil.SUCCEED;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("updateCatalog2")
	@ResponseBody
	public String updateCatalog2(Integer id,String name,Integer catalog1){
		int result = catalog2Service.updateCatalog2(id, name, catalog1);
		if(result == 1){
			return ControllerResultUtil.SUCCEED_RESULT;
		}else{
			return ControllerResultUtil.ERROR_RESULT;
		}
	}
	
	@RequestMapping("catalog2ById")
	@ResponseBody
	public Catalog2 catalog2ById(Integer catalog2Id){
		return catalog2Service.getCatalog2ById(catalog2Id);
	}
	
}
