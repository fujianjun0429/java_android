package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.ConnAppRecommendDao;
import com.zpkj.quick.manage.pojo.ConnAppRecommend;
import com.zpkj.quick.manage.util.IUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConnAppRecommendService  {

	@Autowired
	ConnAppRecommendDao connAppRecommendMapper;
	
	
	public int insertConnAppRecommend(int quickId, int recommendId) {
		try {
			Integer i = connAppRecommendMapper.selectConnAppRecommend(quickId, recommendId);
			if(IUtil.check(i)<=0){
				connAppRecommendMapper.insertConnAppRecommend(quickId, recommendId);
			}else{
				System.out.println("has i="+IUtil.check(i));
			}
			return 1; 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	public List<ConnAppRecommend> selectQuickByRecommend(int recommendId) {
		return connAppRecommendMapper.selectQuickByRecommend(recommendId);
	}

	
	public int deleteConnAppRecommend(int quickId, int recommendId) {
		try {
			connAppRecommendMapper.deleteConnAppRecommend(quickId, recommendId);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

}
