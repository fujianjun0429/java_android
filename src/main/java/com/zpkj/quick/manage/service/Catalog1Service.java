package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.Catalog1Dao;
import com.zpkj.quick.manage.pojo.Catalog1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Catalog1Service {

	@Autowired
	Catalog1Dao catalog1Mapper;
	
	public List<Catalog1> getCatalog1List() {
		List<Catalog1> list = catalog1Mapper.selectCatalog1List();
		return list;
	}

	public int addCatalog1(String catalog1Name) {
		try {
			catalog1Mapper.insertCatalog1(catalog1Name);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int deleteCatalog1(int catalog1Id) {
		try {
			catalog1Mapper.deleteCatalog1(catalog1Id);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int updateCatalog1(int id, String name) {
		try {
			catalog1Mapper.updateCatalog1(id, name);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public Catalog1 getCatalog1(int catalog1Id) {
		return catalog1Mapper.selectCatalog1ById(catalog1Id);
	}

}
