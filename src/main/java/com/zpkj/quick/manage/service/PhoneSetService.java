package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.PhoneSetDao;
import com.zpkj.quick.manage.mapper.PhoneTypeDao;
import com.zpkj.quick.manage.pojo.PhoneSet;
import com.zpkj.quick.manage.pojo.PhoneType;
import com.zpkj.quick.manage.pojo.QappResult;
import com.zpkj.quick.manage.pojo.SearchContentType;
import com.zpkj.quick.manage.util.IUtil;
import com.zpkj.quick.manage.util.QConstants;
import com.zpkj.quick.manage.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PhoneSetService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PhoneSetService.class);
	private static final String TAG =PhoneSetService.class.getSimpleName();


	@Autowired
	PhoneSetDao phoneSetMapper;
	
	@Autowired
	PhoneTypeDao phoneTypeMapper;
	
	public int addPhoneSet(String name, String phoneNick, String title) {
		try {
			int searchType = StringUtil.string2Integer(title.substring(0,1));
			LOGGER.debug(TAG, "searchType="+searchType);
			phoneSetMapper.insertPhoneSet(name,phoneNick,searchType);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	public int deletePhoneSet(int id) {
		try {
			phoneSetMapper.deletePhoneSet(id);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	public QappResult addOrupdatePhoneSet(Integer id, String name, String phoneNick, int searchType) {
		QappResult result = new QappResult();
		try {
			int iId = IUtil.check(id);
			Integer rInteger=null;
			if(iId>0){
				rInteger=phoneSetMapper.updatePhoneSet(id, name,phoneNick,searchType);
			}else{
				rInteger=phoneSetMapper.insertPhoneSet(name,phoneNick,searchType);
			}
			result.setSuccess(true);
			result.setMsg(QConstants.SUCCESS);
			result.setData(rInteger);
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMsg("服务器内部错误"+e.getMessage());
		}
		return result;
	}

	
	public PhoneSet selectPhoneSetById(int id) {
		return phoneSetMapper.selectPhoneSetById(id);
	}

	
	public List<PhoneSet> selectPhoneList(String phoneNick) {
		
		List<PhoneSet> phoneSetList =  phoneSetMapper.selectPhoneList(phoneNick);
		for(PhoneSet phone:phoneSetList ){
			//查询包含的机型
			 List<PhoneType> phoneTypes = phoneTypeMapper.selectPhoneTypeByGroupId(phone.getId());
			 String phoneTypeStr = "";
			 for(PhoneType phoneType : phoneTypes){
				 //连起 来 显示
				 phoneTypeStr += phoneType.getName()+",";
			 }
			 
			 phone.setPhoneTypeStr(phoneTypeStr);
		}
		
		return phoneSetList;
	}
	
	public List<SearchContentType> selectSearchContentTypeList() {
		
		List<SearchContentType> phoneSetList =  phoneSetMapper.selectSearchContentTypeList();
		return phoneSetList;
	}

}
