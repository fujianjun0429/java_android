package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.TagDao;
import com.zpkj.quick.manage.pojo.QuickTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService {

	@Autowired
	TagDao quickTagMapper;

	
	public int insertQuickTag(String name, int catalogId) {
		try {
			quickTagMapper.insertQuickTag(name, catalogId);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public int deleteQuickTag(int id) {
		try {
			quickTagMapper.deleteQuickTag(id);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public int updateQuickTag(int id, String name, int catalogId) {
		try {
			quickTagMapper.updateQuickTag(id, name, catalogId);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public QuickTag selectTagById(int id) {
		return quickTagMapper.selectTagById(id);
	}
	
	public List<QuickTag> selectTagByCatalogId(int catalogId) {
		return quickTagMapper.selectTagByCatalogId(catalogId);
	}

	
	public List<QuickTag> selectTagList() {
		return quickTagMapper.selectTagList();
	}
	
	
	
}
