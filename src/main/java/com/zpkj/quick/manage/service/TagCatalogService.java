package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.TagCatalogDao;
import com.zpkj.quick.manage.pojo.TagCatalog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagCatalogService{

	@Autowired
	TagCatalogDao tagCatalogMapper;
	
	
	public int insertTagCatalog(String name) {
		try {
			tagCatalogMapper.insertTagCatalog(name);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public int deleteTagCatalog(int id) {
		try {
			tagCatalogMapper.deleteTagCatalog(id);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public int updateTagCatalog(int id, String name) {
		try {
			tagCatalogMapper.updateTagCatalog(id, name);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public TagCatalog selectTagCatalogById(int id) {
		return tagCatalogMapper.selectTagCatalogById(id);
	}

	
	public List<TagCatalog> selectTagCatalogList() {
		List<TagCatalog> list = tagCatalogMapper.selectTagCatalogList();
		return list;
	}

}
