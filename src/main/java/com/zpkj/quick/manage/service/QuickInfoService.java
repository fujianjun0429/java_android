package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.QuickInfoDao;
import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.pojo.QuickInfo;
import com.zpkj.quick.manage.util.*;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class QuickInfoService {
	private static final Logger LOGGER = LoggerFactory.getLogger(QuickInfoService.class);
	private static final String TAG = QuickInfoService.class.getSimpleName()+"{}";
	
	@Autowired
	QuickInfoDao quickInfoMapper;
	
	
	@Autowired
	FileService fileService;

	
	public  int allUpOrDown(Integer onOrOff){
		int status = IUtil.check(onOrOff);
		return IUtil.check(quickInfoMapper.allOnOfOff(status));
	}
	
	public List<QuickInfo> getQuickList(String appName, Integer page,Integer status_id) {
		int iPage = IUtil.check(page);
		if (iPage <= 0)
			iPage =1;
		int offset = (iPage-1)*200;
		int iStatus_id = IUtil.check(status_id);
		List<QuickInfo> resultList = null;
		if(iStatus_id>0){
			resultList = quickInfoMapper.selectQuickInfoListOnlyOnline(appName );
		}else{
			resultList= quickInfoMapper.selectQuickInfoList(appName,offset );
		}
		
		try {
			for (QuickInfo item : resultList) {
				// unicode 转中文
				item.setDeveloper(UnicodeUtil.unicodeToString(item.getDeveloper()));
			}
		} catch (Exception e) {
		}
		return resultList;
		
	}
	
	
	public List<QuickInfo> onlineQuickInfoList(String appName, Integer page) {
		int iPage = IUtil.check(page);
		if (iPage <= 0)
			iPage =1;
		int offset = (iPage-1)*1000;
		return quickInfoMapper.onlineQuickInfoList(appName,offset);
	}

	
	public QuickInfo getQuickInfoById(int id) {
		return quickInfoMapper.selectQuickInfoById(id);
	}

	
	public List<QuickInfo> getQuickListByWord(String name) {
		return quickInfoMapper.selectQuickInfoListByWord(name);
	}

	
	public List<QuickInfo> getQuickListByKey(String key) {
		return quickInfoMapper.selectQuickInfoListByKey(key);
	}

	
	public int setQuickStatusOnline(int id) {
		try {
			quickInfoMapper.updateQuickStatusOnline(id);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	public int setQuickStatusOffline(int id) {
		try {
			quickInfoMapper.updateQuickStatusOffline(id);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return 0;
	}

	
	public GeneralResult quickEditOrAdd(
			     MultipartFile iconFile,
				   Integer id,
				   String name,
				   String key,
				   String info,
				   String engineUri,
				   String enginePackage,
				   String engineAction,
				   String type,
				   String icon,
				   String minVersion,
				   Integer statusId
	      ) {
		
			GeneralResult result = new GeneralResult();
			
			if (null !=iconFile && iconFile.getSize() != 0) {//有icon图片文件上传
				String iconUrl = fileService.doUploadFile(iconFile, "icon");
				if (NullUtil.isNull(iconUrl)) {
					result.setErrorCode(QConstants.ERROR);
					result.setErrorMessage("upload icon file failed");
					return result;
				}
				//上传图片保存成功，更新url
				icon = iconUrl;
			}
			
			Integer r=null;
			if(null == id){
		    	//添加一条qucik
				try {
					// 同时存拼音，用于搜索
					PinyinTool pinyinTool = new PinyinTool();
					String pinYin = pinyinTool.toPinYin(name);
					String pinYinBrief = pinyinTool.toGetPinYinFirstChar(name);
					
					r= quickInfoMapper.addQuickEdit(name,pinYin,pinYinBrief, key,info, engineUri, enginePackage,
			    			engineAction, type, icon, minVersion, statusId);

				} catch (BadHanyuPinyinOutputFormatCombination e) {
					LOGGER.error(TAG, "pinyinTool()",e);
				}
		    
		    }else{
		    	//更新一条qucik
		    	r=quickInfoMapper.updateQuickEdit(id, name, key, info,engineUri, enginePackage,
		    			engineAction, type, icon, minVersion, statusId);
		    }
			result.setResultData(r);
			result.setResultStatus(true);
			result.setErrorMessage(QConstants.SUCCESS);
			return result;
	}
	

}
