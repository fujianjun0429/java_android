package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.Catalog2Dao;
import com.zpkj.quick.manage.pojo.Catalog2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Catalog2Service  {

	@Autowired
	Catalog2Dao catalog2Mapper;
	
	public List<Catalog2> getCatalog2ByCatalog1Id(int catalog1Id) {
		List<Catalog2> list = catalog2Mapper.selectCatalog2ByCatalog1Id(catalog1Id);
		return list;
	}

	
	public int addCatalog2(String name, int catalog1) {
		try {
			catalog2Mapper.insertCatalog2(name, catalog1);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	public List<Catalog2> getCatalog2List() {
		return catalog2Mapper.selectCatalog2List();
	}

	
	public int deleteCatalog2(int catalog2Id) {
		try {
			catalog2Mapper.deleteCatalog2(catalog2Id);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public Catalog2 getCatalog2ById(int catalog2Id) {
		return catalog2Mapper.selectCatalog2ById(catalog2Id);
	}

	public int updateCatalog2(int id, String name, int catalog1) {
		try {
			catalog2Mapper.updateCatalog2(id, name, catalog1);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

}
