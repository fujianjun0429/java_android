package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.QuickSpecialPhotoDao;
import com.zpkj.quick.manage.pojo.QuickSpecialPhoto;
import com.zpkj.quick.manage.util.IUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuickSpecialPhotoService {

	@Autowired
	QuickSpecialPhotoDao quickSpecialPhotoMapper;

	
	public int insertSpecialPhoto(String info, String local, String jumpaddr, int specialId) {
		try {
			quickSpecialPhotoMapper.insertSpecialPhoto(info, local, jumpaddr, specialId);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public int deleteSpecialPhoto(int id) {
		try {
			quickSpecialPhotoMapper.deleteSpecialPhoto(id);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public int updateSpecialPhoto(int id, String info, String local, String jumpaddr, int specialId) {
		try {
			quickSpecialPhotoMapper.updateSpecialPhoto(id, info, local, jumpaddr, specialId);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public QuickSpecialPhoto selectSpecialPhotoById(int id) {
		return quickSpecialPhotoMapper.selectSpecialPhotoById(id);
	}

	
	public List<QuickSpecialPhoto> selectPhotoBySpecialId(int specialId) {
		return quickSpecialPhotoMapper.selectPhotoBySpecialId(specialId);
	}

	
	public List<QuickSpecialPhoto> selectPhotoList(Integer specialId) {
		int iSpecialId = IUtil.check(specialId);
		return quickSpecialPhotoMapper.selectPhotoList(iSpecialId);
	}
	
	
	
	
}
