package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.KAdminDao;
import com.zpkj.quick.manage.pojo.KAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KAdminService {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileService.class);
	private static final String  TAG = KAdminService.class.getSimpleName()+"：{}";

	@Autowired
	KAdminDao adminDao;
	
	public boolean canEnter(String userName, String userPassword) {
		KAdmin kAdmin= adminDao.selectUser(userName);
		
		if(null == kAdmin){
			return false;
		}else if(userPassword.equalsIgnoreCase(kAdmin.getUserPassword())){
			LOGGER.debug(TAG, userName+"||"+ kAdmin.getUserPassword() +kAdmin.getId()+kAdmin.getUserName()+kAdmin.getPermission());
			return true;
		}
		return false;
	}
	
}
