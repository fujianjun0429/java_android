package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.PhoneTypeDao;
import com.zpkj.quick.manage.pojo.PhoneType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhoneTypeService  {

	@Autowired
	PhoneTypeDao phoneTypeMapper;
	
	
	public int addPhoneType(String name, int groupId) {
		try {
			phoneTypeMapper.insertPhoneType(name, groupId);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	public int deletePhoneType(int id) {
		try {
			phoneTypeMapper.deletePhoneType(id);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	public int updatePhoneType(int id,String name,int groupId) {
		try {
			phoneTypeMapper.updatePhoneType(id,name,groupId);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	public PhoneType selectPhoneTypeById(int id) {
		return phoneTypeMapper.selectPhoneTypeById(id);
	}

	
	public List<PhoneType> selectPhoneTypeByGroup(int groupId) {
		return phoneTypeMapper.selectPhoneTypeByGroupId(groupId);
	}

	
	public List<PhoneType> selectPhoneTypeList(String name) {
		return phoneTypeMapper.selectPhoneTypeList(name);
	}

}
