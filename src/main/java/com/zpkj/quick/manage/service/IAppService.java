package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.pojo.GeneralResult;

/**
 * gongnengService接口
 * 
 * @author ruoyi
 * @date 2022-11-03
 */
public interface IAppService {
    public String getAppsAndSites(String packageName, String versionCode) ;
    public String getSiteJson();

    public String getAppsAndSitesSe();

    public GeneralResult getAd(
            String packageName,
            String versionCode,
            String channel
    );
    public GeneralResult getUpdateInfo(
            String packageName,
            String versionCode,
            String channel
    );
    public String hotWords(
            String mac,
            String androidId
    );

    public GeneralResult getConfig(String packageName,
                                   String versionCode,
                                   String imei,
                                   String androidId,
                                   String mac,
                                   String imsi,
                                   String channel
    );

}
