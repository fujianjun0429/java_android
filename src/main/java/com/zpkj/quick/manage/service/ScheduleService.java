package com.zpkj.quick.manage.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ScheduleService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleService.class);
	private static final String TAG =ScheduleService.class.getSimpleName();

//	@Autowired
//	private AppletInfoService appletService;
//	
	// 每天2点， 更新拉取百度小程序数据
	@Scheduled(cron = "0 0 2 * * ?")
	public void updateAppData() {
		try {
			//appletService.updateData();
		} catch (Exception e) {
			LOGGER.error("appletService error:{}", e);
		}
	}
	

//	//@Resource和@Autowired都是做bean的注入时使用， 出现 错误时，可有一下几个问题需要去检查：
//1. 检查各类是否加了注解，包括@service,@repository 等等；（注意@Autowired放在service实现上，而不是接口类上面。）
//2. 包是否正确扫描到，这个很重要！！！
	
//	//定时任务。（注意启动类Application加注解：@EnableScheduling）
//	1)fixedRate=5000 表示每隔5000ms，	Spring scheduling会调用一次该方法，	不论该方法的执行时间是多少
//	2)fixedDelay = 5000 表示当方法执行完毕5000ms后，Spring scheduling会再次调用该方法 
//	3)cron="*/5 * * * * * *"提供了一种通用的定时任务表达式，这里表示每隔5秒执行一次
//	@Scheduled(fixedDelay = 5000)
//    public void fixedDelayJob() throws InterruptedException {
//        TimeUnit.SECONDS.sleep(2);
//    }
	
	// 每15分钟，
//	@Scheduled(cron = "0 */15 * * * ?")
//	public void getDevices() {
//		try {
//			//deviceService.getDeviceList(null);
//		} catch (Exception e) {
//			LOGGER.error("getDeviceList error: {}", e);
//		}
//	}
	
//	// 每天2点执行
//	@Scheduled(cron = "0 0 2 * * ?")
//	public void scheduleUpdatePinyin() {
//		mQappService.scheduleUpdatePinyin();
//	}
//	
//	private int index = 0;
//	// 每30秒
//	@Scheduled(cron = "30 * * * * ?")
//	public void setIsAllForceUpdate() {
//		try {
//			configService.setIsAllForceUpdate(++index%2);
//		} catch (Exception e) {
//			LOGGER.error("setIsAllForceUpdate error: {}", e);
//		}
//	}
//	   
	
	
}
