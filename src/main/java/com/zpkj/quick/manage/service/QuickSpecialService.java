package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.QuickSpecialDao;
import com.zpkj.quick.manage.pojo.QappResult;
import com.zpkj.quick.manage.pojo.QuickSpecial;
import com.zpkj.quick.manage.pojo.SpecialApp;
import com.zpkj.quick.manage.util.IUtil;
import com.zpkj.quick.manage.util.QConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class QuickSpecialService {

	@Autowired
	QuickSpecialDao quickSpecialMapper;
	@Autowired
	FileService fileService;
	
	
	public QappResult insertSpecial(String name, String info, int isdefault, 
			String specialIcon, String startTime,
			String endTime, MultipartFile iconFile) {
		QappResult result = new QappResult();
//		String sSpecialIcon = specialIcon;
//		if (null !=iconFile && iconFile.getSize() != 0) {
//			sSpecialIcon = fileService.doUploadFile(iconFile, "jpg");
//		}
//		Timestamp lStartTime = new Timestamp(DateUtils.parseDate(startTime, DateUtils.DEFAULT_DATE_FORMAT_MONTH_FRONT).getMillis());
//		Timestamp lEndTime =  new Timestamp(DateUtils.parseDate(endTime, DateUtils.DEFAULT_DATE_FORMAT_MONTH_FRONT).getMillis());
//		int r = quickSpecialMapper.insertSpecial(name, info, isdefault, lStartTime, lEndTime, sSpecialIcon);
//		result.setData(r);
//
//		result.setSuccess(true);
//		result.setMsg(QConstants.SUCCESS);
		return result;
	}
	
	public int deleteSpecial(int id) {
		try {
			quickSpecialMapper.deleteSpecial(id);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	public QappResult updateSpecial(int id, String name, String info, int isdefault, 
			String specialIcon, String startTime,
			String endTime, MultipartFile iconFile) {
		QappResult result = new QappResult();
		String sSpecialIcon = specialIcon;
		if (null !=iconFile && iconFile.getSize() != 0) {
			sSpecialIcon = fileService.doUploadFile(iconFile, "jpg");
		}
//		Timestamp lStartTime = new Timestamp(DateUtils.parseDate(startTime, DateUtils.DEFAULT_DATE_FORMAT_MONTH_FRONT).getMillis());
//		Timestamp lEndTime =  new Timestamp(DateUtils.parseDate(endTime, DateUtils.DEFAULT_DATE_FORMAT_MONTH_FRONT).getMillis());
//		int r = quickSpecialMapper.updateSpecial(id, name, info, isdefault, lStartTime, lEndTime, sSpecialIcon);
//		result.setData(r);
//
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);
		return result;
	}

	
	public QuickSpecial selectSpecialById(int id) {
		return quickSpecialMapper.selectSpecialById(id);
	}

	
	public List<QuickSpecial> selectSpecialList() {
		return quickSpecialMapper.selectSpecialList();
	}
	
	public List<SpecialApp> selectSpecialApps(Integer specialId) {
		int iSpecialId = IUtil.check(specialId);
		return quickSpecialMapper.selectSpecialApps(iSpecialId);
	}
	
	public QappResult addAppToSpecial(Integer specialId, Integer appId) {
		QappResult result = new QappResult();
		SpecialApp specialApp= quickSpecialMapper.selectSpecialApp(specialId, appId);
		if(null == specialApp){
			int r= quickSpecialMapper.addAppToSpecial(specialId, appId);
			result.setData(r);
		}
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);
		return result;
	}
	
	public QappResult deleteAppToSpecial(Integer specialId, Integer appId) {
		QappResult result = new QappResult();
		int r= quickSpecialMapper.deleteAppToSpecial(specialId, appId);
		result.setData(r);
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);
		return result;
		
	}

}
