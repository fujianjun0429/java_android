package com.zpkj.quick.manage.service;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.zpkj.quick.manage.mapper.QuickInfoDao;
import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.util.IUtil;
import com.zpkj.quick.manage.util.NullUtil;
import com.zpkj.quick.manage.util.RegExUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Service
public class FileService {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileService.class);
		
    @Value("${upload-Filepath}")
    private String uploadFilepath ; // 注入application.properties的配置属性
    
    @Value("${upload-File-Domain}")
	private String uploadFileDomain ;
    
    @Value("${aliyun.oss.point}")
    private String endpoint ;
    @Value("${aliyun.oss.key.id}")
    private String accessKeyId ;
    @Value("${aliyun.oss.key.secret}")
    private String accessKeySecret ;
    
    @Value("${aliyun.oss.key.bucketname}")
    private String yourBucketName ;
    @Value("${aliyun.oss.url.host}")
    private String ossHost ;
    
//    	aliyun.oss.point=http://oss-cn-hangzhou.aliyuncs.com
//    	aliyun.oss.key.id=LTAIBoLjpjuH1t1O
//    	aliyun.oss.key.secret=C2xIoEtbgiDEUtw9xOnAXhIrUEcCi2
//    	aliyun.oss.key.bucketname=test-arch-oss-01
//    	aliyun.oss.url.host=test-arch-oss-01.oss-cn-hangzhou.aliyuncs.com
    
    
	@Autowired
	QuickInfoDao quickInfoMapper;
	
	public GeneralResult uploadAppImg( MultipartFile upfile, String ftype, Integer appId){
		GeneralResult result = new GeneralResult();
		result.setResultStatus(true);
		
		String picUrl= doUploadFile2Oss(upfile, ftype);
		
        result.setResultData(picUrl);
        
        int iAppId = IUtil.check(appId);
        quickInfoMapper.updateQuickInfoImage(iAppId, picUrl);
		return result;
		
	}
	
	public GeneralResult uploadApk( MultipartFile upfile, String ftype, String  pkgName){
		GeneralResult result = new GeneralResult();
		result.setResultStatus(true);
		
		//String apkUrl= doUploadFile(upfile, ftype);
		String apkUrl= doUploadFile2Oss(upfile, ftype);
		
		result.setResultData(apkUrl);
		
		return result;
		
	}
	
	public String doUploadFile( MultipartFile upfile, String ftype){
		return doUploadFile2Oss(upfile, ftype);
	}
	
	/**上传保存任意文件，并返回文件url
	 * 1、首先存到服务器
	 * 2、再把服务器文件存到阿里云oss
	 */
	public String doUploadFile2Oss( MultipartFile upfile, String ftype){
		String targetPath = uploadFilepath + File.separator + ftype + File.separator;
		
		File tempDir = new File(targetPath);
		if (!tempDir.exists()) {
			tempDir.mkdirs();
		}
		LOGGER.info("doUploadFile targetPath = " + targetPath);
		
		String preName = RegExUtil.filterSpecialStr(upfile.getOriginalFilename())+System.currentTimeMillis();
		if(preName.length()>32){
			preName.substring(31);
		}
		String  fileName = preName;
		String[] postfix = upfile.getOriginalFilename().split("[.]");
		if(!NullUtil.isNull(postfix)){
			fileName = preName + "." + postfix[postfix.length-1];
		}
		
		File targetFile = new File(targetPath + fileName);
		if (targetFile.exists()) {
			targetFile.delete();
		}
		try {
			// 使用 MultipartFile 提供的方法
			upfile.transferTo(targetFile);
		} catch (IOException e) {
			LOGGER.error("upload file is failed {}", e);
			return "";
		}
		
//		// Endpoint以杭州为例，其它Region请按实际情况填写。
//		String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
//		// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
//		String accessKeyId = "<yourAccessKeyId>";
//		String accessKeySecret = "<yourAccessKeySecret>";

		// 创建OSSClient实例。
		OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

		String abFileName= ftype +"/"+fileName;
		if(null != ossClient){
			// 上传文件。<yourLocalFile>由本地文件路径加文件名包括后缀组成，例如/users/local/myfile.txt。
			ossClient.putObject(yourBucketName, abFileName, targetFile);
	
			// 关闭OSSClient。
			ossClient.shutdown();
		}
		
		String fileUrl = ossHost + File.separator + abFileName;
		return fileUrl;
		
	}
	
}
