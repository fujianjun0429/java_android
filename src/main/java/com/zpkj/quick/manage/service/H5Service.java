package com.zpkj.quick.manage.service;

import com.google.gson.Gson;
import com.zpkj.quick.manage.mapper.H5Dao;
import com.zpkj.quick.manage.pojo.GameGift;
import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.pojo.H5Bean;
import com.zpkj.quick.manage.pojo.Metation;
import com.zpkj.quick.manage.util.*;
import com.zpkj.quick.manage.util.http.HttpDoRequest;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;


@Service
public class H5Service {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileService.class);
	private static final String  TAG = H5Service.class.getSimpleName()+"{}";
	
	public static final String IP = "http://182.61.28.245:30702"; // 支付服务器
	private static final String NOTIFY_URL_WX_HEEPAY = IP + "/zhifu/buynotify_heepay";
	private static final String BUY_CALLBACK_URL_get = IP + "/zhifu/buySuccessGet";
	// 游戏服务器,发送购买物品链接
	//private static final String SEND_MAIL_URL ="http://103.123.212.180:8088/efunsendreward"; //游戏服务器发邮件
	private static final String SEND_MAIL_URL ="http://182.61.46.182:8088/efunsendreward"; //游戏服务器发邮件
//	private static final String SEND_MAIL_URL ="http://103.123.212.180:8088/efunsendreward"; //游戏服务器发邮件
//	private static final String SEND_MAIL_URL ="http://182.61.46.182:8088/efunsendreward";
//	private static final String SEND_MAIL_URL ="http://103.123.212.180:8088/efunsendreward"; //游戏服务器发邮件

	@Autowired
	HttpDoRequest httpDoRequest;
	
	@Autowired
	H5Dao h5Dao;
	
	public GeneralResult insertH5(String name, String url, String imageUrl, Integer status) {
	GeneralResult result = new GeneralResult();
		try{
			int r= h5Dao.insertH5(name, url, imageUrl, status);
			result.setResultData(r);
			result.setResultStatus(true);
			result.setErrorMessage(QConstants.SUCCESS);
		} catch (Exception e) {
			result.setErrorMessage("failed");
			result.setResultStatus(false);
			LOGGER.error(TAG, e);
		}
		
		return result;
	}
	public GeneralResult deleteH5(int id) {
		GeneralResult result = new GeneralResult();
		try{
			int r= h5Dao.deleteH5(id);
			result.setResultData(r);
			result.setResultStatus(true);
			result.setErrorMessage(QConstants.SUCCESS);
		} catch (Exception e) {
			result.setErrorMessage("failed");
			result.setResultStatus(false);
			LOGGER.error(TAG, e);
		}
		
		return result;
	}

	public List<H5Bean> selectH5s() {
		return h5Dao.selectH5s();
	}
	
	
	
	
	public String getHeepayUrlWX(String price, String nRoleId, String szGroup, String zdyStr) {
		String HEEPAY_URL="https://Pay.Heepay.com/DirectPay/applypay.aspx";
		//测试商户号2083328      秘钥852B4A10455E49F280EEFCB1   这是测试账号和秘钥
		//正式商户号：2121944     秘钥：280DE1DD8BD641E4B49A76D7
		String MERCH_ID="2121944";
		String MERCH_KEY="280DE1DD8BD641E4B49A76D7";
		
			HashMap<String, String> map = new HashMap<>();
		    map.put("version", 1+"");
		    map.put("scene", "h5");
		    map.put("pay_type", "30");
		    map.put("agent_id", MERCH_ID);
		    
	        map.put("pay_amt", price);
	        map.put("goods_name"," ");
	        
	        float  fenzhang=StringUtil.string2Float(price)-(StringUtil.string2Float(price)*1.5f/100f);
	        String goodsNote="1689:"+fenzhang;
	        LOGGER.debug(TAG, "goodsNote="+goodsNote);
//		        map.put("goods_note",goodsNote);
	        map.put("goods_note","");
	        map.put("remark", zdyStr);
	        map.put("payment_mode", "cashier");
		        Metation metation = new Metation();
		        metation.s="Android";
		        metation.n="shubaoyuedu";
		        metation.id=nRoleId+System.currentTimeMillis();
		        Gson gson = new Gson();
		        String jsonStr=gson.toJson(metation);
		    map.put("meta_option", EncryptUtil.encryptBASE64(jsonStr.getBytes() ));
		    
	        map.put("bank_card_type", "-1");
	        //订单号
	        String mStrOrderId = OrderUtil.getOrderNO( nRoleId, szGroup);
	        map.put("agent_bill_id", mStrOrderId);

	        map.put("notify_url", NOTIFY_URL_WX_HEEPAY);
	        map.put("return_url", BUY_CALLBACK_URL_get);
	        map.put("user_ip", "182.61.28.245");
//	        map.put("agent_bill_time", DateUtils.getCurrentDateMark2());
	        
	        String preStr ="version="+map.get("version")
	        	+"&agent_id="+map.get("agent_id")
	        	+"&agent_bill_id="+map.get("agent_bill_id")
	        	+"&agent_bill_time="+map.get("agent_bill_time")
	        	+"&pay_type="+map.get("pay_type")
	        	+"&pay_amt="+map.get("pay_amt")
	        	+"&notify_url="+map.get("notify_url")
	        	+"&return_url="+map.get("return_url")
	        	+"&user_ip="+map.get("user_ip")
	        	+"&bank_card_type="+map.get("bank_card_type")
	        	+"&remark="+map.get("remark");
	        String sign = AndroidMD5.getMd5Str(preStr + "&key=" + MERCH_KEY);
	        LOGGER.debug("preStr+key {} ", preStr + "&key=" + MERCH_KEY);
	        map.put("sign", sign);

        String urlResult = "";
        HttpEntity backEntry = httpDoRequest.doGetRequestBackEntry(HEEPAY_URL,map);
		try {
			LOGGER.error("backEntry str {} ", "backEntry" + backEntry);
			if (null != backEntry) {
				byte[] byteResult = EntityUtils.toByteArray(backEntry);
				String strResult = new String(byteResult);
				// LOGGER.error("strResult str {} ", strResult);

				if (strResult.contains("0000")) {
					LOGGER.error("strResult str {} ", "下单成功");
//					urlResult = RegExUtil.getHeepayUrl(strResult);
				} else {
					LOGGER.error("strResult str {} ", "下单失败");
				}
			}
		} catch (Exception e) {
			LOGGER.error(TAG, e);
		}

		return urlResult;
	}
	
	
	//( nRoleId, szGroup, nGroupIndex);
	public String getPrice( String nRoleId, String szGroup, String nGroupIndex) {
		String price = null;
		 GameGift gameGift = h5Dao.selectGameGift(szGroup, nGroupIndex);
		 if(null != gameGift){
			 price = gameGift.getPrice();
		 }
		return price;
	}
	
	
	/**
	 * 购买 回调
	 */
	public String  buynotify(String merchantNo,
			String orderAmount, String orderNo, 
			String wtfOrderNo,
			int orderStatus, String sign,
			String payTime,String productName,
			String productDesc,
			String remark) {
		LOGGER.error("buynotify{}","\t orderNo ="+orderNo+"_"+orderAmount+"_"+orderStatus+"_"+sign+"_"+payTime);
		Integer hasSave= h5Dao.seleceOrder(orderNo);
		hasSave = IUtil.check(hasSave);
		if(hasSave>0){
			//此订单已处理
			return "success";
		}
		if(NullUtil.isNull(remark)){
			return "failed";
		}
		//String remark = nRoleId + "_" + szGroup + "_" + nGroupIndex + "_" + nServerId;
		String[] remarkArr = remark.split("_");
		String nRoleId = remarkArr[0];
		String szGroup = remarkArr[1];
		String nGroupIndex = remarkArr[2];
		String nServerId = remarkArr[3];
		//发送邮件
//		serialNo= (1048592 +20638549) * 2 - 500;
//		http://103.193.192.177:8088/efunsendreward?
//		userId=1&roleId=1048592&serverCode=10001&serialNo=43373782&packageId=Gold,250&title=情缘剑侠的邮件&content=aaa
		HashMap<String, String> map = new HashMap<>();
	    GameGift gameGift = h5Dao.selectGameGift(szGroup, nGroupIndex);
		   
	    map.put("userId", gameGift.getUserId());
	    map.put("roleId", ""+nRoleId);
	    map.put("serverCode", nServerId);
	    map.put("title", "购买物品邮件");
	    map.put("content", "购买物品，请查收");
		//int serialNo = (StringUtil.string2Integer(nRoleId) + 20638549) * 2 - 500;//龙雀
//		int serialNo = (StringUtil.string2Integer(nRoleId) + 343870593) * 2 - 500;//杨门
		int serialNo = (StringUtil.string2Integer(nRoleId) + 87654321) * 2 - 500;//杨门 （自己）
	    map.put("serialNo", serialNo+"");
	    
	    String shouChong = "item,1240,1;";
	    //查找没有首充，送首充礼包
	    Integer count= h5Dao.selectShouChong(""+nRoleId);
	    count =IUtil.check(count);
	    if(count>0) {
	    	//已首充过
	    	map.put("packageId", gameGift.getPackageId());
	    }else {
			map.put("packageId", shouChong + gameGift.getPackageId());
	    }
	    
	    String re= httpDoRequest.doGetRequest(SEND_MAIL_URL, map);
		LOGGER.error("buynotify{}","send mail re="+re);
		
		//保存订单
		payTime = StringUtil.getFormat_Date_HH_MM_SS(System.currentTimeMillis());
		h5Dao.saveOrder(nRoleId, merchantNo, orderAmount, orderNo, wtfOrderNo, orderStatus, sign, payTime, szGroup,
				productDesc, remark);

		return "success";
	}
	
}
