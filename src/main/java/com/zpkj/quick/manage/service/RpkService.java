package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.RpkDao;
import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.pojo.Rpk;
import com.zpkj.quick.manage.util.NullUtil;
import com.zpkj.quick.manage.util.QConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class RpkService {

	@Autowired
	RpkDao rpkDao;
	
	@Autowired
	FileService fileService;
	
	
	public GeneralResult saveRpk(
			MultipartFile rpkFile, 
			MultipartFile iconFile, 
			Rpk rpk) {
		GeneralResult result = new GeneralResult();
		
		if (null !=rpkFile && rpkFile.getSize() != 0) {//====上传的文件为空时 不是null ，而是getSize() ==0
			String rpkUrl = fileService.doUploadFile2Oss(rpkFile, "rpk");
			if (NullUtil.isNull(rpkUrl)) {
				result.setResultStatus(false);
				result.setErrorCode(QConstants.ERROR);
				result.setErrorMessage("upload rpk file failed");
				return result;
			}

			// 保存文件成功
			rpk.setRpkUrl(rpkUrl);
		}
		if (null !=iconFile && iconFile.getSize() != 0) {//上传的文件为空时 不是null ，而是getSize() ==0
			String iconUrl = fileService.doUploadFile2Oss(iconFile, "icon");
			if (NullUtil.isNull(iconUrl)) {
				result.setResultStatus(false);
				result.setErrorCode(QConstants.ERROR);
				result.setErrorMessage("upload icon file failed");
				return result;
			}
			
			// 保存文件成功
			rpk.setIconUrl(iconUrl);
		}
		
		
		Integer r = rpkDao.saveRpk(rpk);
		
		result.setResultData(r);
		result.setResultStatus(true);
		result.setErrorMessage(QConstants.SUCCESS);
		return result;
	}
	
	public List<Rpk> selectRpks(String rpkName, String rpkPkgName) {
		return rpkDao.selectRpks(rpkName, rpkPkgName);
	}
	public List<Rpk> selectAllRpks() {
		return rpkDao.selectAllRpks();
	}
	
	public GeneralResult deleteRpk(String rpkPkgName) {
		GeneralResult result = new GeneralResult();
		Integer r= rpkDao.delRpk(rpkPkgName);
		result.setResultData(r);
		result.setResultStatus(true);
		result.setErrorMessage(QConstants.SUCCESS);
		return result;
	}

}
