package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.KViewTypeDao;
import com.zpkj.quick.manage.pojo.KViewType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KViewTypeService {

	@Autowired
	KViewTypeDao kViewTypeMapper;
	
	
	public int insertKViewType(String title,String icon,int num, String action, String viewtype,int sort,int status) {
		try {
			kViewTypeMapper.insertKViewType(title,icon,num, action, viewtype,sort,status);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	public int deleteKViewType(int id) {
		try {
			kViewTypeMapper.deleteKViewType(id);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public int updateKViewType(int id, String title,String icon,int num, String action, String viewtype,int sort,int status) {
		try {
			kViewTypeMapper.updateKViewType(id, title,icon,num, action, viewtype,sort,status);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	
	public List<KViewType> selectKViewTypeList() {
		return kViewTypeMapper.selectKViewTypeList();
	}

}
