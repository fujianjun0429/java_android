package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.KUpdateApkDao;
import com.zpkj.quick.manage.pojo.CheckUpdateBean;
import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.pojo.KUpdateBean;
import com.zpkj.quick.manage.util.NullUtil;
import com.zpkj.quick.manage.util.QConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Service
public class KUpdateApkService {
	private static final Logger LOGGER = LoggerFactory.getLogger(KUpdateApkService.class);
	private static final String  TAG = KUpdateApkService.class.getSimpleName()+"：{}";

	@Autowired
	KUpdateApkDao kUpdateApkDao;
	
	@Autowired
	FileService fileService;
	
	
	public GeneralResult saveUpdateApks(MultipartFile upfile, KUpdateBean updateBean) {
		GeneralResult result = new GeneralResult();
		
		if (null !=upfile && upfile.getSize() != 0) {//====上传的文件为空时 不是null ，而是getSize() ==0
//			String apkUrl = fileService.doUploadFile(upfile, "apk");
			String apkUrl = fileService.doUploadFile2Oss(upfile, "apk");
			if (NullUtil.isNull(apkUrl)) {
				result.setErrorCode(QConstants.ERROR);
				result.setErrorMessage("upload file failed");
				return result;
			}

			// 保存文件成功
			updateBean.setLink(apkUrl);
		}
		
		Integer r = kUpdateApkDao.saveKupdate(updateBean);
		result.setResultData(r);
		result.setResultStatus(true);
		result.setErrorMessage(QConstants.SUCCESS);
		return result;
	}
	
	public List<KUpdateBean> selectUpdateApks() {
		return kUpdateApkDao.selectUpdateApks();
	}
	
	public GeneralResult getUpdateApks(List<CheckUpdateBean> apps) {
		GeneralResult result = new GeneralResult();
		try {
			if(NullUtil.isNull(apps)){
				result.setResultStatus(false);
				result.setErrorMessage("请求参数错误");
				return result;
			}
			
			List<KUpdateBean> resultApps= new ArrayList<KUpdateBean>();
			for(CheckUpdateBean app:apps){
				KUpdateBean update=	kUpdateApkDao.selectOneUpdateApk(app);
				if(null != update){
					resultApps.add(update);
				}
			}
			result.setResultData(resultApps);
			result.setResultStatus(true);
		} catch (Exception e) {
			result.setResultStatus(false);
			LOGGER.error("Server Exception!", e);
			result.setErrorMessage("服务器内部错误!"+e.getMessage());
		}
		
		return result;
	}

}
