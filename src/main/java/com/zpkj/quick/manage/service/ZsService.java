package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.ZsDao;
import com.zpkj.quick.manage.pojo.QappResult;
import com.zpkj.quick.manage.pojo.ZsAggregation;
import com.zpkj.quick.manage.pojo.ZsApp;
import com.zpkj.quick.manage.util.QConstants;
import com.zpkj.quick.manage.util.http.HttpDoRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ZsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ZsService.class);
	private static final String  TAG = ZsService.class.getSimpleName()+":{}"; 
	
	@Autowired
	ZsDao zsDao;
	
	@Autowired
	HttpDoRequest httpDoRequest;
	
	public QappResult helper() {
		LOGGER.debug(TAG, "helper");
		QappResult result = new QappResult(); 
		result.setTimestamp(System.currentTimeMillis());
		
		List<ZsAggregation> aggregations = zsDao.getAggregations();
		
		for(ZsAggregation aggregation: aggregations){
			List<ZsApp> apps= zsDao.getApps(aggregation.getId());
			for(ZsApp app:apps){
				List<String> questions = zsDao.getQuestions(app.getId());
				app.setQuestions(questions);
			}
			aggregation.setApps(apps);
		}

		result.setData(aggregations);
		result.setCode(QConstants.SUCCESS_CODE_200);
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);
		
		return result;
	}
	
	public QappResult testHttp() {
		LOGGER.debug(TAG, "testHttp");
		QappResult result = new QappResult(); 
		result.setTimestamp(System.currentTimeMillis());
		
		String res= httpDoRequest.doGetRequestInCache("http://182.61.28.245:30700/sdk/showShenFen", null, 2);
		
		result.setData(res);
		result.setCode(QConstants.SUCCESS_CODE_200);
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);
		
		return result;
	}
	

	
}
