package com.zpkj.quick.manage.service.help;

public interface GroupConstans {
	
//	int SDK_SUGGUEST_GROUPID=6;
//	int SDK_SEARCH_GROUPID=8;
//	int SDK_MORE_SUGGEST_GROUPID=9;
//	int SDK_ALL_SEARCH_GROUPID=10;
	
//	int POSITION_NO1=1;
//	int POSITION_NO2=2;
//	int POSITION_NO3=3;
//	int POSITION_NO4=4;
//	int POSITION_NO5=5;
	
	//（1:小程序 2快应用  3H5跳转  4专题  5搜索词）
	int SEARCH_TYPE_QAPP=2;
	int SEARCH_TYPE_BDSWAN=1;
	int SEARCH_TYPE_H5=3;
	int SEARCH_TYPE_TOPICAL=4;
	int SEARCH_TYPE_HOT_WORD=5;
	
	String ACTION_TYPE_QAPP="qapp";
	String ACTION_TYPE_BDSWAN="bdswan";
	String ACTION_TYPE_TOPIC="special";
	String ACTION_TYPE_H5="h5";
	String ACTION_TYPE_HOTWORDS="hotWords";
	
	
	String PRODUCT_TYPE_SHOP = "shop";
	String PRODUCT_TYPE_SDK = "sdk";
	

	String GROUP_TYPE_SDK_LAST_OPEN = "sdkLastOpen"; 
	String GROUP_TYPE_SDK_SEARCH_SUGGEST = "sdkSearchSuggest";
	String GROUP_TYPE_SDK_DEFAULT_TOPIC = "sdkDefaultTopic";
	String GROUP_TYPE_SDK_ALL_SEARCH = "sdkAllSearch";

}
