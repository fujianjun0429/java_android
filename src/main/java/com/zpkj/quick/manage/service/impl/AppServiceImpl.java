package com.zpkj.quick.manage.service.impl;

import com.zpkj.quick.manage.pojo.GeneralResult;
import com.zpkj.quick.manage.pojo.ReportConfig;
import com.zpkj.quick.manage.pojo.UpdateInfo;
import com.zpkj.quick.manage.service.IAppService;
import com.zpkj.quick.manage.util.NullUtil;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class AppServiceImpl implements IAppService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppServiceImpl.class);
    private int siteCount = 0;
    private int liugongziCount = 0;
    private int seCount = 0;
    private int updateCount = 0;
    private int adCount = 0;
    private Map<String, String> userMap = new HashMap<>();

    @Override
    public String getAppsAndSites(String packageName, String versionCode) {
        //int hour = StringUtil.getFormatTime_Hour();
        LOGGER.info("siteCount=" + (++siteCount));
        if (NullUtil.isNull(packageName)|| "null".equalsIgnoreCase(packageName)) {
            //低版本
            return readFile("excelTemplate/site.json");
        } else {
            //12及以上版本
            if (packageName.equalsIgnoreCase("com.studio.osc")
                    || packageName.equalsIgnoreCase("com.tvbox.yijia")) {
                return readFile("excelTemplate/site.json");
            }
        }
        return "error";
    }

    @Override
    public String getSiteJson() {
        LOGGER.info("liugongziCount=" + (++liugongziCount));
        return readFile("excelTemplate/site.json");
    }

    @Override
    public String getAppsAndSitesSe() {
        LOGGER.info("seCount=" + (++seCount));
        return readFile("excelTemplate/liugongzi.json");
    }

    /**
     * 用户上报,返回配置
     */
    @Override
    public GeneralResult getConfig(String packageName,
                                   String versionCode,
                                   String imei,
                                   String androidId,
                                   String mac,
                                   String imsi,
                                   String channel
    ) {
        GeneralResult result = new GeneralResult();
        ReportConfig config = new ReportConfig();
        config.setDesc("获取配置返回");
        config.setCaution("免责声明：本软件影片资源和接口全部来源于互联网，如有侵权，请联系删除，本人不承担任何连带责任，谢谢！QQ群：879811030");
        config.setbUseLocalData(false); //注意修改这个配置，默认true，用本地配置//已废弃
        result.setResultData(config);
        result.setResultStatus(true);

        userMap.put(androidId, androidId);
        LOGGER.info("getConfig imei=" + imei + "\tandroidId=" + androidId + "\tmac=" + mac + "\timsi=" + imsi
                + "\tpackageName=" + packageName + "\tversionCode=" + versionCode + "\tchannel=" + channel +
                "\t userMap.size =" + userMap.size()
        );
        return result;
    }

    /**
     * 广告
     */
    @Override
    public GeneralResult getAd(String packageName, String versionCode, String channel) {
        GeneralResult result = new GeneralResult();
        UpdateInfo ad = new UpdateInfo();
        ad.setUpdateurl("http://103.125.248.111:8080/app/tv.apk");
        ad.setDesc("万能影视，新版本升级，更好用，更易用！");
        ad.setImgUrl("http://103.125.248.111:8080/app/wangneng.jpg");
        ad.setbForceUpdate(false);
        ad.setIsUpdate("N");//是否显示广告， Y并且url不为空==显示广告
        ad.setVersionCode(11); //废弃
        result.setResultStatus(true);
        result.setResultData(ad);
        LOGGER.info("adCount =" + (++adCount));
        return result;
    }
    /**
     * 检测更新信息
     */
    @Override
    public GeneralResult getUpdateInfo(String packageName, String versionCode, String channel) {
        GeneralResult result = new GeneralResult();
        UpdateInfo update = new UpdateInfo();
        update.setUpdateurl("http://movie100.top:8081/app/movie100.apk");
        update.setDesc("修复已知bug，优化性能");
        update.setbForceUpdate(false);
        update.setIsUpdate("N");//是否升级 Y并且versionCode>本地和 URL不为空==显示广告
        update.setVersionCode(1);
        result.setResultStatus(true);
        result.setResultData(update);
        LOGGER.info("updateCount =" + (++updateCount));
        return result;
    }

    @Override
    public String hotWords(String mac, String androidId) {
        return readFile("excelTemplate/hotwords.json");
    }


    public String readFile(String filePath) {
        String content = "";
        try {
            //读取文件OK
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(filePath);//解析及播放最好的
            if (null != inputStream) {
                content = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
            } else {
                LOGGER.info("read " + filePath + " inputStream == null");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }





//    @Override
//    public MeConfig getAppsAndSites() {
////        List<MSite> sites = MSiteMapper.selectMSiteList(null);
////        List<MeLive> channels = meLiveMapper.selectAllLives();
////        List<MeParses> parses = meParsesMapper.selectMeParsesList(null);
////        List<MeIjk> options = meIjkMapper.selectMeIjkList(null);
////
////        for(MSite site:sites){
////            site.setKey(site.getSiteKey());
////        }
////
////        List<MeConfig.LiveRsp> lives = new ArrayList<>();
//        MeConfig meConfig = new MeConfig();
////        MeConfig.LiveRsp live = meConfig.new LiveRsp(); //内部类初始化
////        live.setGroup("redirect");
////        live.setChannels(channels);
////        lives.add(live);
////
////        List<MeConfig.IjkRsp> ijkRsps = new ArrayList<>();
////        MeConfig.IjkRsp ijk = meConfig.new IjkRsp(); //内部类初始化
////        ijk.setGroup("软解码");
////        ijk.setOptions(options);
////        ijkRsps.add(ijk);
////
////        meConfig.setSpider("https://github.com/heroaku/TVboxo/raw/main/Jar/xo1.jar");
////        meConfig.setLives(lives);
////        meConfig.setSites(sites);
////        meConfig.setIjk(ijkRsps);
////        meConfig.setParses(parses);
////        String[] flags = new String[]{"youku", "qq", "iqiyi", "qiyi", "letv", "sohu", "tudou", "pptv", "mgtv", "wasu", "bilibili", "renrenmi", "优酷", "芒果", "腾讯", "爱奇艺", "奇艺", "ltnb", "rx", "CL4K", "xfyun", "wuduzy"};
////        String[] ads = new String[]{"mimg.0c1q0l.cn", "www.googletagmanager.com", "www.google-analytics.com", "mc.usihnbcq.cn", "mg.g1mm3d.cn", "mscs.svaeuzh.cn", "cnzz.hhttm.top", "tp.vinuxhome.com", "cnzz.mmstat.com", "www.baihuillq.com", "s23.cnzz.com", "z3.cnzz.com", "c.cnzz.com", "stj.v1vo.top", "z12.cnzz.com", "img.mosflower.cn", "tips.gamevvip.com", "ehwe.yhdtns.com", "xdn.cqqc3.com", "www.jixunkyy.cn", "sp.chemacid.cn", "hm.baidu.com", "s9.cnzz.com", "z6.cnzz.com", "um.cavuc.com", "mav.mavuz.com", "wofwk.aoidf3.com", "z5.cnzz.com", "xc.hubeijieshikj.cn", "tj.tianwenhu.com", "xg.gars57.cn", "k.jinxiuzhilv.com", "cdn.bootcss.com", "ppl.xunzhuo123.com", "xomk.jiangjunmh.top", "img.xunzhuo123.com", "z1.cnzz.com", "s13.cnzz.com", "xg.huataisangao.cn", "z7.cnzz.com", "xg.huataisangao.cn", "z2.cnzz.com", "s96.cnzz.com", "q11.cnzz.com", "thy.dacedsfa.cn", "xg.whsbpw.cn", "s19.cnzz.com", "z8.cnzz.com", "s4.cnzz.com", "f5w.as12df.top", "ae01.alicdn.com", "www.92424.cn", "k.wudejia.com", "vivovip.mmszxc.top", "qiu.xixiqiu.com", "cdnjs.hnfenxun.com", "cms.qdwght.com"};
////        meConfig.setFlags(flags);
////        meConfig.setAds(ads);
//
//        return meConfig;
//    }

}
