package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.AdDao;
import com.zpkj.quick.manage.pojo.*;
import com.zpkj.quick.manage.util.IUtil;
import com.zpkj.quick.manage.util.QConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdService {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdService.class);
	private static final String  TAG = AdService.class.getSimpleName()+":{}"; 
	
	@Autowired
	AdDao mAdDao;

//	//获取引擎广告配置
//	public QappResult getAdConfig(String imei, String brand, String model, String pkgname) {
//		LOGGER.debug(TAG, "getAdConfig");
//		if(NullUtil.isNull(imei)) imei = "imei";
//		QappResult result = new QappResult(); 
//		
//		AdConfig adConfig = mAdDao.selectAdConfig(pkgname);
//		result.setData(adConfig);
//		
//		result.setCode (QConstants.SUCCESS_CODE_200);
//		result.setMsg(QConstants.SUCCESS);
//		result.setSuccess(true);
//		result.setTimestamp(System.currentTimeMillis());
//		
//		return result;
//	}
//	
//	//获取广告位广告类型
//	public QappResult getAdPositions(String imei, String brand, String model,  Integer positionId, String quickInfoKey) {
//		LOGGER.debug(TAG, "getAdPositions");
//		QappResult result = new QappResult(); 
//		
//		List<AdPosition> positions = mAdDao.selectAdPositions(IUtil.check(positionId), quickInfoKey) ;
//		if(!NullUtil.isNull(positions) && positions.size() == 1){
//			//传入位置id时，只有一条记录
//			result.setData(positions.get(0));
//		}else{
//			//查询此快应用所有广告位
//			result.setData(positions );
//		}
//		
//		result.setCode (QConstants.SUCCESS_CODE_200);
//		result.setSuccess(true);
//		result.setMsg(QConstants.SUCCESS);
//		result.setTimestamp(System.currentTimeMillis());
//		
//		return result;
//	}
	
	
	//获取广告位对应广告 
	public List<AdPositionSdk> getPositionAd(String imei, String brand, String model,   String quickInfoKey,String cpPositionId) {
		LOGGER.debug(TAG, "getPositionAd");
		QappResult result = new QappResult(); 
		
		List<AdPositionSdk> positionSdks = mAdDao.getPositionAd( quickInfoKey,cpPositionId);
		// 传入位置id时，只查询一个位置的广告 ，查询此快应用所有广告位
		result.setData(positionSdks);
		
		result.setCode (QConstants.SUCCESS_CODE_200);
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);
		result.setTimestamp(System.currentTimeMillis());
		
		return positionSdks;
	}
	
	//创建更新广告位置，和添加广告位sdk
	public QappResult newOrUpdatePosition( String positionName,int positionTypeId, int forceShowAd, String quickInfoKey,
			int sdkId, String thirdAdId ,Integer thisSdkShow) {
		LOGGER.debug(TAG, "newPosition");
		QappResult result = new QappResult(); 
		
		//查询广告位(同一快应用同种类型广告,只能有一条记录)
		AdPosition position = mAdDao.selectPosition(positionTypeId, quickInfoKey);
		if(null != position){
			//更新广告位
			mAdDao.updatePosition(position.getId(), positionName, forceShowAd);
		}else{
			//插入广告位到position表,同时获得positionId
			position = new AdPosition();
			position.setPositionName(positionName);
			position.setPositionTypeId(positionTypeId);
			position.setQuickInfoKey(quickInfoKey);
			position.setForceShowAd(forceShowAd);
		    int r = mAdDao.insert2Position(position);
		    
		    Integer  pId  = position.getId();
		    AdPositionType adPositionType =mAdDao.selectAdPositionType(positionTypeId);
			String cpPositionId = adPositionType.getTypePreSuffix() +pId;
			mAdDao.updatePositionCP(position.getId(),cpPositionId);
		}
		Integer positionId  = position.getId();
		
		//插入到中间表
		Integer insertCount= mAdDao.insertOrUpdatemPositionSdk(thirdAdId, positionId, sdkId,IUtil.check(thisSdkShow));
		if(thisSdkShow != null){
			mAdDao.enablemPositionSdk(IUtil.check(thisSdkShow), positionId, sdkId);
		}
		
		result.setData(insertCount);
		result.setCode (QConstants.SUCCESS_CODE_200);
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);
		result.setTimestamp(System.currentTimeMillis());
		
		return result;
	}
	
	//更改该位置sdk是否可用
	public QappResult enablemPositionSdk(String cpPositionId, Integer sdkId, Integer thisSdkShow) {
		LOGGER.debug(TAG, "enablemPositionSdk");
		QappResult result = new QappResult();
		result.setCode(QConstants.ERROR_CODE);
		result.setSuccess(false);
		result.setMsg(QConstants.ERROR);
		
		// 更新中间表，更改thisSdkShow
		if (thisSdkShow != null) {
			AdPosition adPosition = mAdDao.selectPositionByCppositionId(cpPositionId);
			if (null != adPosition) {
				int positionId = adPosition.getId();
				Integer r = mAdDao.enablemPositionSdk(IUtil.check(thisSdkShow), IUtil.check(positionId),IUtil.check(sdkId));
				result.setCode(QConstants.SUCCESS_CODE_200);
				result.setSuccess(true);
				result.setMsg(QConstants.SUCCESS);
			}
		}
		
		result.setTimestamp(System.currentTimeMillis());

		return result;
	}
	
	//获取广告类型列表
	public List<AdPositionType> selectAdPositionTypes( ) {
		LOGGER.debug(TAG, "selectAdPositionTypes");
		QappResult result = new QappResult(); 
		
		List<AdPositionType> adPositionTyps = mAdDao.selectAdPositionTypes();
		result.setData(adPositionTyps);
		
		result.setCode (QConstants.SUCCESS_CODE_200);
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);
		result.setTimestamp(System.currentTimeMillis());
		
		return adPositionTyps;
	}
	
	//获取sdk列表
	public List<AdSdk> selectSdks( ) {
		LOGGER.debug(TAG, "selectSdks");
		QappResult result = new QappResult(); 
		
		List<AdSdk> sdks = mAdDao.selectSdks();
		result.setData(sdks);
		
		result.setCode (QConstants.SUCCESS_CODE_200);
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);
		result.setTimestamp(System.currentTimeMillis());
		
		return sdks;
	}
	
	
	//注册
	public QappResult register( String  quickKey, String accountName, String accountPassword) {
		LOGGER.debug(TAG, "register");
		QappResult result = new QappResult(); 
		
		AdAccount account = mAdDao.allreayRegister(quickKey);
		if (null != account) {
			result.setCode(QConstants.ERROR_CODE);
			result.setSuccess(false);
			result.setMsg("此快应用已被认领，如果你认为被人冒领，请申诉");
			result.setTimestamp(System.currentTimeMillis());
			return result;
		}
		account = mAdDao.sameAccount(accountName);
		if (null != account) {
			result.setCode(QConstants.ERROR_CODE);
			result.setSuccess(false);
			result.setMsg("已有相同账号，请换个账号名称注册");
			result.setTimestamp(System.currentTimeMillis());
			return result;
		}
		
		int insertCount= mAdDao.register(quickKey, accountName, accountPassword);
		result.setData(insertCount);
		result.setCode (QConstants.SUCCESS_CODE_200);
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);
		result.setTimestamp(System.currentTimeMillis());
		
		return result;
	}
	
	//登录
	public QappResult login(  String accountName, String accountPassword) {
		LOGGER.debug(TAG, "login");
		QappResult result = new QappResult(); 
		result.setTimestamp(System.currentTimeMillis());
		
		AdAccount acount= mAdDao.login( accountName, accountPassword);
		if(null == acount){
			result.setCode (QConstants.ERROR_CODE);
			result.setSuccess(false);
			result.setMsg("账号或密码不正确！");
		}else{
			result.setData(acount);
			result.setCode (QConstants.SUCCESS_CODE_200);
			result.setSuccess(true);
			result.setMsg(QConstants.SUCCESS);
		}
		
		return result;
	}
	
	//认领app
	public QappResult claimApp(  String quickKey, int accountId) {
		LOGGER.debug(TAG, "login");
		QappResult result = new QappResult(); 
		result.setTimestamp(System.currentTimeMillis());

		QuickInfo qapp = mAdDao.selectAppByKey(quickKey);
		if (qapp == null) {
			result.setCode(QConstants.ERROR_CODE);
			result.setSuccess(false);
			result.setMsg(quickKey+"此快应用未找到，请确认包名正确");
			return result;
		}
		String appId = qapp.getId() + "";

		int r = mAdDao.claimApp(appId, quickKey, accountId);
		result.setData(r);
		result.setCode(QConstants.SUCCESS_CODE_200);
		result.setSuccess(true);
		result.setMsg(QConstants.SUCCESS);

		return result;
	}

	
}
