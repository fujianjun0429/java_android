package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.WordsDao;
import com.zpkj.quick.manage.pojo.QuickWords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WordsService {

	@Autowired
	WordsDao quickWordsDao;
	
	
	public int insertWords(String name, int status) {
		try {
			quickWordsDao.insertWords(name, status);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public int deleteWords(int id) {
		try {
			quickWordsDao.deleteWords(id);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public int updateWords(int id, String name, int status) {
		try {
			quickWordsDao.updateWords(id, name, status);
			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	public QuickWords selectWordsById(int id) {
		return quickWordsDao.selectWordsById(id);
	}

	
	public List<QuickWords> selectWordsList() {
		return quickWordsDao.selectWordsList();
	}

}
