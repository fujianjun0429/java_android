package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.*;
import com.zpkj.quick.manage.pojo.*;
import com.zpkj.quick.manage.service.help.GroupConstans;
import com.zpkj.quick.manage.util.IUtil;
import com.zpkj.quick.manage.util.NullUtil;
import com.zpkj.quick.manage.util.QConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class GroupService {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileService.class);
	private static final String  TAG = GroupService.class.getSimpleName()+"{}";
	
	public static final String  SELECT_TYPE_APP = "app";
	public static final String  SELECT_TYPE_SPECIAL = "special";
	public static final String  SELECT_TYPE_H5 = "h5";
	public static final String  SELECT_TYPE_HOTWORDS = "hotWords";
	
	@Value("${server-Domain}")
	private String serverDomain ; // 注入application.properties的配置属性

	@Autowired
	GroupDao groupDao;
	@Autowired
	QuickInfoDao quickInfoDao;
	
	@Autowired
	QuickSpecialDao quickSpecialDao;
	
	@Autowired
	WordsDao quickWordsDao;
	
	@Autowired
	H5Dao h5Dao;
	
	@Autowired
	FileService fileService;
	
	public GeneralResult insertOrUpdateGroup(Integer groupId, String title, String info, int sort, String descGroup,
			String viewType, String groupAction, String productType, int status, String startTime, String endTime) {
		GeneralResult result = new GeneralResult();
		
		try{
			int iGroupId = IUtil.check(groupId);
			int r = 0;
			//====构造Timestamp时间
//			Timestamp sStartTime = new Timestamp(DateUtils.parseDate(startTime, DateUtils.DEFAULT_DATE_FORMAT_MONTH_FRONT).getMillis());
//			Timestamp sEndTime = new Timestamp(DateUtils.parseDate(endTime, DateUtils.DEFAULT_DATE_FORMAT_MONTH_FRONT).getMillis());
//			groupAction = serverDomain+"/api/appset/apps/?id="+iGroupId;
//			if (iGroupId <= 0) {
//				r= groupDao.insertGroup(title, info, sort, descGroup, viewType, groupAction, productType, status, sStartTime, sEndTime);
//				if(r >=1){
//					Integer maxGroupId = groupDao.selectMaxGroupId();
//					groupAction = serverDomain+"/api/appset/apps/?id="+maxGroupId;
//					r = groupDao.updateGroup(maxGroupId, title, info, sort, descGroup, viewType, groupAction, productType, status, sStartTime, sEndTime);
//				}
//			}else{
//				r = groupDao.updateGroup(iGroupId, title, info, sort, descGroup, viewType, groupAction, productType, status, sStartTime, sEndTime);
//			}
			result.setResultData(r);
			result.setResultStatus(true);
			result.setErrorMessage(QConstants.SUCCESS);
		} catch (Exception e) {
			result.setErrorMessage("failed");
			result.setResultStatus(false);
			LOGGER.error(TAG, e);
		}
		
		return result;
	}
	
	public List<GroupIdBean> selectGroups(String productType, int groupId, int onlyVorH) {
		return groupDao.selectGroups(productType, groupId, onlyVorH);
	}
	
	public  GroupIdBean selectGroup(String viewType) {
		return groupDao.selectGroup(viewType);
	}
	
	public GeneralResult deleteGroup(Integer groupId) {
		GeneralResult result = new GeneralResult();
		
		try{
			int	r=groupDao.deleteGroup(groupId);
			result.setResultData(r);
			result.setResultStatus(true);
			result.setErrorMessage(QConstants.SUCCESS);
		} catch (Exception e) {
			result.setErrorMessage("deleteGroup failed");
			result.setResultStatus(false);
			LOGGER.error(TAG, e);
		}
		
		return result;
	}
	
	//////////////////////position/////////////////////////////////////////
	public GeneralResult insertOrUpdateGroupPosition(Integer id, int groupId, int positionNo, String positionDesc, int status) {
		GeneralResult result = new GeneralResult();
		
		try{
			int iid = IUtil.check(id);
			int r = 0;
			if(iid<=0){
				r = groupDao.insertGroupPosition(groupId, positionNo, positionDesc, status);
			}else{
				r = groupDao.updateGroupPosition(id, groupId, positionNo, positionDesc, status);
			}
			result.setResultData(r);
			result.setResultStatus(true);
			result.setErrorMessage(QConstants.SUCCESS);
		} catch (Exception e) {
			result.setErrorMessage("failed");
			result.setResultStatus(false);
			LOGGER.error(TAG, e);
		}
		
		return result;
	}
	
	
	public GeneralResult insertGroupPosition(int groupId, int positionNo, String positionDesc, int status) {
		GeneralResult result = new GeneralResult();

		try {
			 int r=groupDao.insertGroupPosition(groupId, positionNo, positionDesc, status);
			 result.setResultData(r);
			result.setResultStatus(true);
			result.setErrorMessage(QConstants.SUCCESS);
		} catch (Exception e) {
			result.setErrorMessage(" failed");
			result.setResultStatus(false);
			LOGGER.error(TAG, e);
		}

		return result;

	}

	GeneralResult updateGroupPosition(int id, int groupId, int positionNo, String positionDesc, int status) {
		GeneralResult result = new GeneralResult();
		try {
			int r = groupDao.updateGroupPosition(id, groupId, positionNo, positionDesc, status);
			result.setResultData(r);
			result.setResultStatus(true);
			result.setErrorMessage(QConstants.SUCCESS);
		} catch (Exception e) {
			result.setErrorMessage(" failed");
			result.setResultStatus(false);
			LOGGER.error(TAG, e);
		}

		return result;

	}

	public List<GroupPositionBean> selectGroupPositions(String viewType, int iGroupId) {
		int groupId = iGroupId;
		if(groupId == 0 && !NullUtil.isNull(viewType)){
			GroupIdBean groupIdBean = groupDao.selectGroup(viewType);
			if (null != groupIdBean) {
				groupId = groupIdBean.getGroupId();
			}
		}
		return groupDao.selectGroupPositions(groupId);
	}
		
	public GeneralResult deleteGroupPosition(Integer id) {
		GeneralResult result = new GeneralResult();
		try {
			int r = groupDao.deleteGroupPosition(id);
			result.setResultData(r);
			result.setResultStatus(true);
			result.setErrorMessage(QConstants.SUCCESS);
		} catch (Exception e) {
			result.setErrorMessage(" failed");
			result.setResultStatus(false);
			LOGGER.error(TAG, e);
		}

		return result;
	}
	
	/////////////////////positionApp/////////////////////////
	public	GeneralResult insertOrUpdateApp2Position(String selectType, int selectId, int id,int positionId, int appId, int topicId, int hotWordsId, int h5Id,
			String actionType, int searchType, String goUrl, String startTime, String endTime, int peoples,
			String bak1, //bak1用于存放机组ID
			String imageUrl, MultipartFile imageFile) {
		GeneralResult result = new GeneralResult();

		try {
			
			if (SELECT_TYPE_APP.equalsIgnoreCase(selectType)) {
				appId = selectId;
			} else if (SELECT_TYPE_SPECIAL.equalsIgnoreCase(selectType)) {
				topicId = selectId;
			} else if (SELECT_TYPE_H5.equalsIgnoreCase(selectType)) {
				h5Id = selectId;
			} else {
				hotWordsId = selectId;
			}
			
			peoples = com.zpkj.quick.manage.util.MathUtil.getRandom(500, 50000);
			if(appId>0){
				QuickInfo quickInfo = quickInfoDao.selectQuickInfoById(appId);
				actionType = quickInfo.getType();
				searchType = GroupConstans.SEARCH_TYPE_BDSWAN;
				if(GroupConstans.ACTION_TYPE_QAPP.equalsIgnoreCase(actionType )){
					searchType = GroupConstans.SEARCH_TYPE_QAPP;
				}
			}
			else if (topicId > 0) {
				actionType = GroupConstans.ACTION_TYPE_TOPIC;
				searchType = GroupConstans.SEARCH_TYPE_TOPICAL;
			}
			else if (h5Id > 0) {
				actionType = GroupConstans.ACTION_TYPE_H5;
				searchType = GroupConstans.SEARCH_TYPE_H5;
			}
			else if (hotWordsId > 0) {
				actionType = GroupConstans.ACTION_TYPE_HOTWORDS;
				searchType = GroupConstans.SEARCH_TYPE_HOT_WORD;
			}

			if (null !=imageFile && imageFile.getSize() != 0) {
				imageUrl = fileService.doUploadFile(imageFile, "jpg");
			}

//			Timestamp sStartTime = new Timestamp(DateUtils.parseDate(startTime, DateUtils.DEFAULT_DATE_FORMAT_MONTH_FRONT).getMillis());
//			Timestamp sEndTime = new Timestamp(DateUtils.parseDate(endTime, DateUtils.DEFAULT_DATE_FORMAT_MONTH_FRONT).getMillis());
//			if(NullUtil.isNull(bak1)){
//				bak1= 1+"";
//			}
//			if (id > 0) {
//				int r = groupDao.updateApp2Position(id, positionId, appId, topicId, hotWordsId, h5Id, actionType,
//						searchType, goUrl, sStartTime, sEndTime, peoples,bak1, imageUrl);
//				result.setResultData(r);
//			} else {
//				int r = groupDao.insertApp2Position(positionId, appId, topicId, hotWordsId, h5Id, actionType,
//						searchType, goUrl, sStartTime, sEndTime, peoples, bak1,imageUrl);
//				result.setResultData(r);
//			}
			result.setResultStatus(true);
			result.setErrorMessage(QConstants.SUCCESS);
		} catch (Exception e) {
			result.setErrorMessage(" failed");
			result.setResultStatus(false);
			LOGGER.error(TAG, e);
		}

		return result;

	}

	public GeneralResult updateAllBak1(String bak1 // bak1用于存放机组ID
			) {
		GeneralResult result = new GeneralResult();

		try {
			int r = groupDao.updateAllBak1(bak1);
			result.setResultData(r);
			result.setResultStatus(true);
			result.setErrorMessage(QConstants.SUCCESS);
		} catch (Exception e) {
			result.setErrorMessage(" failed");
			result.setResultStatus(false);
			LOGGER.error(TAG, e);
		}

		return result;

	}

	public List<GroupPositionApp> selectAppOfPositions(int positionId) {
		List<GroupPositionApp> apps= groupDao.selectAppOfPositions(positionId);
		for(GroupPositionApp item:apps){
			if (item.getSearchType() == GroupConstans.SEARCH_TYPE_QAPP
					|| item.getSearchType() == GroupConstans.SEARCH_TYPE_BDSWAN) {
				QuickInfo appInfo = quickInfoDao.selectQuickInfoById(item.getAppId());
				if(null != appInfo){
					item.setShowName(appInfo.getName());
					item.setSelectId(appInfo.getId());
					item.setSelectType(SELECT_TYPE_APP);
				}
			} 
			else if (item.getSearchType() == GroupConstans.SEARCH_TYPE_TOPICAL) {
				QuickSpecial special = quickSpecialDao.selectSpecialById(item.getTopicId());
				if(null != special){
					item.setShowName(special.getName());
					item.setSelectId(special.getId());
					item.setSelectType(SELECT_TYPE_SPECIAL);
				}
			}
			else if (item.getSearchType() == GroupConstans.SEARCH_TYPE_H5) {
				item.setSelectId(item.getH5Id());
				item.setSelectType(SELECT_TYPE_H5);
			}
			else if (item.getSearchType() == GroupConstans.SEARCH_TYPE_HOT_WORD) {
				QuickWords hotword = quickWordsDao.selectWordsById(item.getHotWordsId());
				if(null != hotword){
					item.setShowName(hotword.getName());
					item.setSelectId(hotword.getId());
					item.setSelectType(SELECT_TYPE_HOTWORDS);
				}
			}
		}
		return apps ;
	}
	

	public GeneralResult deleteAppOfPosition(Integer id) {
		GeneralResult result = new GeneralResult();
		try {
			int r = groupDao.deleteAppOfPosition(id);
			result.setResultData(r);
			result.setResultStatus(true);
			result.setErrorMessage(QConstants.SUCCESS);
		} catch (Exception e) {
			result.setErrorMessage(" failed");
			result.setResultStatus(false);
			LOGGER.error(TAG, e);
		}

		return result;
	}
	
	////////////////////////////////改版/////////////////////////////////////////////////////////
	public List<GroupBatchApp> selectAppOfGroup(String viewType, int groupId) {
		 List<GroupBatchApp> apps = groupDao.selectAppOfGroup(viewType, groupId);
		for(GroupBatchApp item:apps){
			if (item.getSearchType() == GroupConstans.SEARCH_TYPE_QAPP
					|| item.getSearchType() == GroupConstans.SEARCH_TYPE_BDSWAN) {
				QuickInfo appInfo = quickInfoDao.selectQuickInfoById(item.getAppId());
				if(null != appInfo){
					item.setShowName(appInfo.getName());
					item.setKey(appInfo.getKey());
				}
			} 
			else if (item.getSearchType() == GroupConstans.SEARCH_TYPE_TOPICAL) {
				QuickSpecial special = quickSpecialDao.selectSpecialById(item.getTopicId());
				if(null != special){
					item.setShowName(special.getName());
				}
			}
			else if (item.getSearchType() == GroupConstans.SEARCH_TYPE_H5) {
			}
			else if (item.getSearchType() == GroupConstans.SEARCH_TYPE_HOT_WORD) {
				QuickWords hotword = quickWordsDao.selectWordsById(item.getHotWordsId());
				if(null != hotword){
					item.setShowName(hotword.getName());
				}
			}
		}
		return apps ;
	}
	
	public  GeneralResult  batchInsertApp( int groupId, List<GroupBatchApp> apps) {
		GeneralResult result = new GeneralResult();
		//0,全部删除栏目的所有app
		//1、逐个查询位置存不存在， 
		//2、位置存在 ，设置positionId 
		//3、位置不存在，创建，设置positionId
		//4、批量添加app
		List<GroupPositionBean> positons = groupDao.selectGroupPositions(groupId);
		for(GroupPositionBean postion:positons){
			groupDao.deletePositionApps(postion.getId());
		}
		
		for(GroupBatchApp item:apps){
			GroupPositionBean groupPosition = groupDao.selectGroupPosition(groupId, item.getPositionNo());
			if(null != groupPosition ){
				//存在
				item.setPositionId(groupPosition.getId());
				
			}else{
				//不存在
				GroupPositionBean groupPositionNew = new GroupPositionBean();
				groupPositionNew.setGroupId(groupId);
				groupPositionNew.setPositionNo(item.getPositionNo());
				groupPositionNew.setPositionDesc(item.getPositionNo()+"位置");
				groupPositionNew.setStatus(1);
				
				//创建位置
				groupDao.insertGroupPositionBean(groupPositionNew);
				item.setPositionId(groupPositionNew.getId());
				item.setBak1(""+1); //新位置，机组默认为1
			}
			
			//插入附加信息
//			Timestamp sStartTime = DateUtils.getCurrentTimestamp();
//			Timestamp sEndTime = new Timestamp(DateUtils.parseDate(DateUtils.getYearNext(), DateUtils.DEFAULT_DATE_FORMAT_H).getMillis());
//			item.setStartTime(sStartTime);
//			item.setEndTime(sEndTime);
//			if (GroupConstans.ACTION_TYPE_QAPP.equalsIgnoreCase(item.getActionType())) {
//				item.setSearchType(GroupConstans.SEARCH_TYPE_QAPP);
//			} else if (GroupConstans.ACTION_TYPE_BDSWAN.equalsIgnoreCase(item.getActionType())) {
//				item.setSearchType(GroupConstans.SEARCH_TYPE_BDSWAN);
//			} else {
//				item.setSearchType(GroupConstans.SEARCH_TYPE_TOPICAL);
//			}
		}
		
		int r= groupDao.batchInsertApp(apps);

		result.setResultData(r);
		result.setResultStatus(true);
		return result;
	}

}
