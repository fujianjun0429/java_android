package com.zpkj.quick.manage.service;

import com.zpkj.quick.manage.mapper.ZsDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;


@Service
public class TestService {
	private static final Logger LOGGER = LoggerFactory.getLogger(TestService.class);
	private static final String  TAG = TestService.class.getSimpleName()+":{}"; 
	
	@Autowired
	ZsDao zsDao;
	
	//产生一维数组
	private int[]  getArr() {
		int n = 100;
		int[] arr = new int[n];
		Random random = new Random();
		for (int i = 0; i < n; i++) {
			arr[i] = random.nextInt(500);
		}
		return arr;
	}
	//产生二维数组
	private int[][] getTwoDemensionArr() {
		int[][] twoArr = new int[100][100];
		int width = 6;
		int value = 1;

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < width; j++) {
				twoArr[i][j] = value++;
			}
		}
		return twoArr;
	}
	
	//n个数组找出k个最小的
	public void testArr_n_k() {	
		LOGGER.error(TAG, "{} testArr_n_k（） start" );
		int k = 5;
		int arr[] = getArr();
		int size = arr.length;

		for(int i=0; i<k; i++) {
			for(int j=i+1; j<size;j++) {
				if(arr[i]>arr[j]) {
					int temp =arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
			System.out.print(arr[i]+" ");
		}
		System.out.println("");
	}
	
	// 冒泡法排序
	public int[]  testArr_maopao() {
		LOGGER.error(TAG, "{} testArr_maopao（） start");
        int arr[] = getArr();
        int size = arr.length;
		for (int i = 0; i < size; i++) {
			for (int j = i + 1; j < size; j++) {
				if (arr[i] > arr[j]) {
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
			System.out.print(arr[i] + " ");
		}
		System.out.println("");
		return arr;
	}
	
	
	public void testBinarySearch() {
		LOGGER.error(TAG, " testBinarySearch（） start");
		int[] arr=testArr_maopao();
		int key =25;
		int index = binarySearch(arr, 0, arr.length, key);
		System.out.println("key="+key +" \t index="+index);
	}
	
	//二分法查找，也称为折半法，是一种在有序数组中查找特定元素的搜索算法。
	private int binarySearch(int[] arr,int low,int high,int key){
		if(low>high){return -1;}
		int mid=(int)Math.floor((low+high)/2);
		if(key==arr[mid]){
			return mid;
		}else if(key<arr[mid]){
			high=mid-1;
			return binarySearch(arr,low,high,key);
		}else{
			low=mid+1;
			return binarySearch(arr,low,high,key);
		}
	}
	
	
	//顺时针打印一个矩阵
	public void printTwoArr() {
		LOGGER.error(TAG, " printTwoArr（） start");
		int[][] twoArr = getTwoDemensionArr(); 
		output(twoArr, 0, 5-1);
	}
	
	private void output(int[][] twoArr, int start, int end) {
		if (start > end || end <= 0)
			return;
		for (int i = start; i <= end; i++) {
			System.out.print (twoArr[start][i]+" ");
		}
		for (int i = start + 1; i <= end; i++) {
			System.out.print (twoArr[i][end]+" ");
		}

		for (int i = end - 1; i >= start; i--) {
			System.out.print (twoArr[end][i]+" ");
		}
		for (int i = end - 1; i > start; i--) {
			System.out.print (twoArr[i][start]+" ");
		}
		
		System.out.println();

		output(twoArr, start + 1, end - 1);
	}
	
	//给出一个排序好的数组和一个数，求数组中连续元素的和等于所给数的子数组
	public void testSubSumArr() {
		LOGGER.error(TAG, " testSubSumArr（） start");
		int []arr= testArr_maopao();
		int size = arr.length;
		int targetSum = 1200;
		for(int i=0; i<size-1; i++) {
			int left=i;
			int right =i;
			int tempSum =arr[left];
			while(tempSum<targetSum) {
				if(right<size-1) {
					tempSum +=arr[right++];
				}
			}
			System.out.println("tempSum=" +tempSum +" right="+right +" left="+left );
			if(tempSum==targetSum) {
				for(int j=left; j<=right; j++) {
					System.out.print (arr[j]+" ");
				}
				return;
			}
		}
	}
	
	//给出一个字符串，打印所有可能的组合
	public void testCharPrint() {
		LOGGER.error(TAG, " testSubSumArr（） start");
		char[] cs = { 'a', 'b', 'c' };
		int length = cs.length;    
		char[] arr =new char[length];
		for(int i=0; i<length; i++) {
			for(int j=0; j<length; j++) {
				arr[i] = cs[j];
				System.out.print (arr[i]+" ");
			}
			System.out.println();
		}
	}
	
}
