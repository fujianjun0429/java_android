package com.zpkj.quick.manage.pojo;

import org.springframework.stereotype.Component;

@Component
public class PhoneSet {
	
	private int id;
	private String name;
	private String phoneNick;
	private int searchType;
	private String phoneTypeStr;
	private String title;//搜索内容类型title
	private String typeStr;
	private int typeNum;
	
	public String getPhoneNick() {
		return phoneNick;
	}
	public void setPhoneNick(String phoneNick) {
		this.phoneNick = phoneNick;
	}
	public int getSearchType() {
		return searchType;
	}
	public void setSearchType(int searchType) {
		this.searchType = searchType;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPhoneTypeStr() {
		return phoneTypeStr;
	}
	public void setPhoneTypeStr(String phoneTypeStr) {
		this.phoneTypeStr = phoneTypeStr;
	}
	public String getTypeStr() {
		return typeStr;
	}
	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}
	public int getTypeNum() {
		return typeNum;
	}
	public void setTypeNum(int typeNum) {
		this.typeNum = typeNum;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "PhoneSet [id=" + id + ", name=" + name + "]";
	}
	
	
	
}
