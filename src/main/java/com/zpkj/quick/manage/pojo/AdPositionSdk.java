package com.zpkj.quick.manage.pojo;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

public class AdPositionSdk {
	
    private String positionName;
    private Timestamp createTime; 
    private String positionTypeId;
    private int enable;
    private String quickInfoKey ;
    private String cpPositionId ;
    private int forceShowAd;
    
    private int id;
    private String thirdAdId;
    private int sdkId;
    private int thisSdkShow;
    
    private String sdkName;
    private String sdkCompany;
    private String downloadUrl;
    private String sdkFileName;
    private String sdkAppId;
//    private String sdkKey;
    
    private String typeName ;
    
	public int getThisSdkShow() {
		return thisSdkShow;
	}
	public void setThisSdkShow(int thisSdkShow) {
		this.thisSdkShow = thisSdkShow;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public int getForceShowAd() {
		return forceShowAd;
	}
	public String getCpPositionId() {
		return cpPositionId;
	}
	public void setCpPositionId(String cpPositionId) {
		this.cpPositionId = cpPositionId;
	}
	public String getThirdAdId() {
		return thirdAdId;
	}
	public void setThirdAdId(String thirdAdId) {
		this.thirdAdId = thirdAdId;
	}
	public void setForceShowAd(int forceShowAd) {
		this.forceShowAd = forceShowAd;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getPositionTypeId() {
		return positionTypeId;
	}
	public void setPositionTypeId(String positionTypeId) {
		this.positionTypeId = positionTypeId;
	}
	public int getEnable() {
		return enable;
	}
	public void setEnable(int enable) {
		this.enable = enable;
	}
	public String getQuickInfoKey() {
		return quickInfoKey;
	}
	public void setQuickInfoKey(String quickInfoKey) {
		this.quickInfoKey = quickInfoKey;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSdkId() {
		return sdkId;
	}
	public void setSdkId(int sdkId) {
		this.sdkId = sdkId;
	}
	public String getSdkName() {
		return sdkName;
	}
	public void setSdkName(String sdkName) {
		this.sdkName = sdkName;
	}
	public String getSdkCompany() {
		return sdkCompany;
	}
	public void setSdkCompany(String sdkCompany) {
		this.sdkCompany = sdkCompany;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public String getSdkFileName() {
		return sdkFileName;
	}
	public void setSdkFileName(String sdkFileName) {
		this.sdkFileName = sdkFileName;
	}
	public String getSdkAppId() {
		return sdkAppId;
	}
	public void setSdkAppId(String sdkAppId) {
		this.sdkAppId = sdkAppId;
	}
	
//	public String getSdkKey() {
//		return sdkKey;
//	}
//	public void setSdkKey(String sdkKey) {
//		this.sdkKey = sdkKey;
//	}
    
    
    

}
