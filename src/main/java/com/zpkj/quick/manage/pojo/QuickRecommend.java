package com.zpkj.quick.manage.pojo;

import java.util.Date;

public class QuickRecommend {
	
	private int id;
	private String name;
	private String type;
	private int status;
	private Date timeStart;
	private Date timeEnd;
	private String strTimeStart;
	private String strTimeEnd;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getTimeStart() {
		return timeStart;
	}
	public void setTimeStart(Date timeStart) {
		this.timeStart = timeStart;
	}
	public Date getTimeEnd() {
		return timeEnd;
	}
	public void setTimeEnd(Date timeEnd) {
		this.timeEnd = timeEnd;
	}
	public String getStrTimeStart() {
		return strTimeStart;
	}
	public void setStrTimeStart(String strTimeStart) {
		this.strTimeStart = strTimeStart;
	}
	public String getStrTimeEnd() {
		return strTimeEnd;
	}
	public void setStrTimeEnd(String strTimeEnd) {
		this.strTimeEnd = strTimeEnd;
	}
	@Override
	public String toString() {
		return "QuickRecommend [id=" + id + ", name=" + name + ", type=" + type + ", status=" + status + ", timeStart="
				+ timeStart + ", timeEnd=" + timeEnd + ", strTimeStart=" + strTimeStart + ", strTimeEnd=" + strTimeEnd
				+ "]";
	}



	
}
