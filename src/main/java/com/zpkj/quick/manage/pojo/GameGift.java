package com.zpkj.quick.manage.pojo;

public class GameGift {
    
//	id	int	11	0	0	0	-1	0									0	0	0	0	-1	0	0
//	price	varchar	10	0	-1	0	0	0							utf8	utf8_general_ci	0	0	0	0	0	0	0
//	szGroup	varchar	255	0	-1	0	0	0							utf8	utf8_general_ci	0	0	0	0	0	0	0
//	nGroupIndex	int	11	0	-1	0	0	0									0	0	0	0	0	0	0
//	packageId	varchar	255	0	-1	0	0	0							utf8	utf8_general_ci	0	0	0	0	0	0	0
//	mailTitle	varchar	255	0	-1	0	0	0							utf8	utf8_general_ci	0	0	0	0	0	0	0
//	mailContent	varchar	255	0	-1	0	0	0							utf8	utf8_general_ci	0	0	0	0	0	0	0
//	nServerId	int	11	0	-1	0	0	0									0	0	0	0	0	0	0

    private int id;
    private String price;
    private String userId;
    private String szGroup;
    private String nGroupIndex;
    private String packageId;
    private String mailTitle;
    private String mailContent;
    private String nServerId;
    
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getSzGroup() {
		return szGroup;
	}
	public void setSzGroup(String szGroup) {
		this.szGroup = szGroup;
	}
	public String getnGroupIndex() {
		return nGroupIndex;
	}
	public void setnGroupIndex(String nGroupIndex) {
		this.nGroupIndex = nGroupIndex;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getMailTitle() {
		return mailTitle;
	}
	public void setMailTitle(String mailTitle) {
		this.mailTitle = mailTitle;
	}
	public String getMailContent() {
		return mailContent;
	}
	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}
	public String getnServerId() {
		return nServerId;
	}
	public void setnServerId(String nServerId) {
		this.nServerId = nServerId;
	}
    
    

}
