package com.zpkj.quick.manage.pojo;

public class Word {
    private String name = "推荐";
    private int bak = 0;

    public String getName() {
        return name;
    }

    public int getBak() {
        return bak;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBak(int bak) {
        this.bak = bak;
    }
}
