package com.zpkj.quick.manage.pojo;

public class AdSdk {
    
    private int id;
    
    private String sdkName;
    private String sdkCompany;
    private String downloadUrl;
    private String sdkFileName;
    private String sdkAppId;
//    private String sdkKey;
    private int sdkSort;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSdkName() {
		return sdkName;
	}
	public void setSdkName(String sdkName) {
		this.sdkName = sdkName;
	}
	public String getSdkCompany() {
		return sdkCompany;
	}
	public void setSdkCompany(String sdkCompany) {
		this.sdkCompany = sdkCompany;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public String getSdkFileName() {
		return sdkFileName;
	}
	public void setSdkFileName(String sdkFileName) {
		this.sdkFileName = sdkFileName;
	}
	public String getSdkAppId() {
		return sdkAppId;
	}
	public void setSdkAppId(String sdkAppId) {
		this.sdkAppId = sdkAppId;
	}
	public int getSdkSort() {
		return sdkSort;
	}
	public void setSdkSort(int sdkSort) {
		this.sdkSort = sdkSort;
	}

}
