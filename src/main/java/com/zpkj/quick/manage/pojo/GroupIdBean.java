package com.zpkj.quick.manage.pojo;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 栏目
 * @author Administrator
 */
public class GroupIdBean {
	
//	groupId	int	11	0	0	0	0	0				栏目id					0	0	0	0	0	0	0
//	title	varchar	255	0	0	0	0	0				标题			utf8	utf8_general_ci	0	0	0	0	0	0	0
//	info	varchar	255	0	-1	0	0	0				详细介绍			utf8	utf8_general_ci	0	0	0	0	0	0	0
//	sort	int	11	0	0	0	-1	0				排序序号					0	0	0	0	0	0	0
//	descGroup	varchar	255	0	-1	0	0	0				说明			utf8	utf8_general_ci	0	0	0	0	0	0	0
//	viewType	varchar	255	0	-1	0	0	0				栏目类型			utf8	utf8_general_ci	0	0	0	0	0	0	0
//	groupAction	varchar	255	0	-1	0	0	0				点击栏目跳转动作			utf8	utf8_general_ci	0	0	0	0	0	0	0
//	productType	varchar	255	0	-1	0	0	0				属于哪个产品(sdk, shop)			utf8	utf8_general_ci	0	0	0	0	0	0	0
//	status	int	255	0	-1	0	0	0									0	0	0	0	0	0	0
//	startTime	datetime	0	0	-1	0	0	0				启用时间					0	0	0	0	0	0	0
//	endTime	datetime	0	0	-1	0	0	0				结束时间					0	0	0	0	0	0	0
//	bak1	varchar	255	0	-1	0	0	0				栏目备用1			utf8	utf8_general_ci	0	0	0	0	0	0	0
	
	private int groupId;
	private String title;
	private String info;
	private int sort;
	private String descGroup;
	private String viewType;
	private String groupAction;
	private String productType;
	private int status;
	private Timestamp startTime;
	private Timestamp endTime;
	private String bak1;
	
	public int getGroupId() {
		return groupId;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public String getDescGroup() {
		return descGroup;
	}
	public void setDescGroup(String descGroup) {
		this.descGroup = descGroup;
	}
	public String getViewType() {
		return viewType;
	}
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}
	public String getGroupAction() {
		return groupAction;
	}
	public void setGroupAction(String groupAction) {
		this.groupAction = groupAction;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getBak1() {
		return bak1;
	}
	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}
	
	@JsonFormat(pattern="MM/dd/yyyy HH:mm:ss")
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	
	@JsonFormat(pattern="MM/dd/yyyy HH:mm:ss")
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	
	

}
