package com.zpkj.quick.manage.pojo;

/**
 * 
 */
public class Rpk {

	private int rpkId;
	private String packageName;
	private String iconUrl;
	private String rpkName;
	private String rpkUrl;
	private String fileMd5;
	private int size;
	private int rpkVersionCode;
	private int openCount;
	private int enable;
	
	public int getEnable() {
		return enable;
	}
	public void setEnable(int enable) {
		this.enable = enable;
	}
	public int getRpkId() {
		return rpkId;
	}
	public void setRpkId(int rpkId) {
		this.rpkId = rpkId;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getRpkName() {
		return rpkName;
	}
	public void setRpkName(String rpkName) {
		this.rpkName = rpkName;
	}
	public String getRpkUrl() {
		return rpkUrl;
	}
	public void setRpkUrl(String rpkUrl) {
		this.rpkUrl = rpkUrl;
	}
	public String getFileMd5() {
		return fileMd5;
	}
	public void setFileMd5(String fileMd5) {
		this.fileMd5 = fileMd5;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getRpkVersionCode() {
		return rpkVersionCode;
	}
	public void setRpkVersionCode(int rpkVersionCode) {
		this.rpkVersionCode = rpkVersionCode;
	}
	public int getOpenCount() {
		return openCount;
	}
	public void setOpenCount(int openCount) {
		this.openCount = openCount;
	}
	
	
	
	
	
}
