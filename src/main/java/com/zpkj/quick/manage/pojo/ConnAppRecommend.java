package com.zpkj.quick.manage.pojo;

import org.springframework.stereotype.Component;

@Component
public class ConnAppRecommend {
	
	private int id;
	private int quickId;
	private int recommendId;
	private String quickName;
	private String recommendName;
	private int quickStatus;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQuickId() {
		return quickId;
	}
	public void setQuickId(int quickId) {
		this.quickId = quickId;
	}
	public int getRecommendId() {
		return recommendId;
	}
	public void setRecommendId(int recommendId) {
		this.recommendId = recommendId;
	}
	public String getQuickName() {
		return quickName;
	}
	public void setQuickName(String quickName) {
		this.quickName = quickName;
	}
	public String getRecommendName() {
		return recommendName;
	}
	public void setRecommendName(String recommendName) {
		this.recommendName = recommendName;
	}
	public int getQuickStatus() {
		return quickStatus;
	}
	public void setQuickStatus(int quickStatus) {
		this.quickStatus = quickStatus;
	}
	@Override
	public String toString() {
		return "ConnAppRecommend [id=" + id + ", quickId=" + quickId + ", recommendId=" + recommendId + ", quickName="
				+ quickName + ", recommendName=" + recommendName + ", quickStatus=" + quickStatus + "]";
	}

	
}
