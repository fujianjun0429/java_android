package com.zpkj.quick.manage.pojo;

public class UpdateInfo {
    private int id;//int(32) unsigned NOT NULL AUTO_INCREMENT,
    private String isUpdate;//varchar(255) NOT NULL,
    private String desc;//varchar(255) DEFAULT NULL,
    private String updateurl;//varchar(255) DEFAULT NULL,
    private int versionCode;//varchar(255) DEFAULT NULL,
    private String versionName;//int(255) DEFAULT NULL,
    private String packageName;//int(255) DEFAULT NULL,
    private boolean bForceUpdate;
    private String openChannel;
    private String imgUrl;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }


//    `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
//    `isUpdate` varchar(8) DEFAULT 'N',
//    `desc` varchar(255) DEFAULT NULL,
//    `updateurl` varchar(255) DEFAULT NULL,
//    `versionCode` int(8) DEFAULT '0',
//    `versionName` varchar(8) DEFAULT NULL,
//    `packageName` varchar(255) NOT NULL,
//    `openChannel` varchar(255) DEFAULT NULL,
//    `bForceUpdate` tinyint(1) DEFAULT '0',
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getIsUpdate() {
        return isUpdate;
    }
    public void setIsUpdate(String isUpdate) {
        this.isUpdate = isUpdate;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getUpdateurl() {
        return updateurl;
    }
    public void setUpdateurl(String updateurl) {
        this.updateurl = updateurl;
    }
    public int getVersionCode() {
        return versionCode;
    }
    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }
    public String getVersionName() {
        return versionName;
    }
    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }
    public String getPackageName() {
        return packageName;
    }
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    public boolean isbForceUpdate() {
        return bForceUpdate;
    }
    public void setbForceUpdate(boolean bForceUpdate) {
        this.bForceUpdate = bForceUpdate;
    }
    public String getOpenChannel() {
        return openChannel;
    }
    public void setOpenChannel(String openChannel) {
        this.openChannel = openChannel;
    }

    
//    id  int 8   0   0   -1  -1  0   0       0                   -1  0
//    isUpdate    varchar 8   0   -1  0   0   0   0   N   0       utf8    utf8_general_ci     0   0
//    desc    varchar 255 0   -1  0   0   0   0       0       utf8    utf8_general_ci     0   0
//    updateurl   varchar 255 0   -1  0   0   0   0       0       utf8    utf8_general_ci     0   0
//    versionCode int 8   0   -1  0   0   0   0   0   0                   0   0
//    versionName varchar 8   0   -1  0   0   0   0       0       utf8    utf8_general_ci     0   0
//    packageName varchar 255 0   0   0   0   0   0       0       utf8    utf8_general_ci     0   0

    
    
}
