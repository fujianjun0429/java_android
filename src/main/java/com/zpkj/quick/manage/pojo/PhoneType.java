package com.zpkj.quick.manage.pojo;

import org.springframework.stereotype.Component;

@Component
public class PhoneType {
	
	private int id;
	private String name;
	private int groupId;
	private String groupName;
	private String phoneNick;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	
	public String getPhoneNick() {
		return phoneNick;
	}
	public void setPhoneNick(String phoneNick) {
		this.phoneNick = phoneNick;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getGroupId() {
		return groupId;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	@Override
	public String toString() {
		return "PhoneType [id=" + id + ", name=" + name + ", groupId=" + groupId + ", groupName=" + groupName + "]";
	}

	
	
	
}
