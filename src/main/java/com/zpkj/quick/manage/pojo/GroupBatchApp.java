package com.zpkj.quick.manage.pojo;

import java.sql.Timestamp;

/**
 * 栏目位置app
 * @author Administrator
 */
public class GroupBatchApp {
	
	private int positionId;
	
	private int positionNo;
	private int status;
	private int appId;
	private String key;
	private String showName;
	
	private int topicId;
	private int hotWordsId;
	private int h5Id;
	
	private String actionType;
	private int searchType;
	private String goUrl;
	private Timestamp startTime;
	private Timestamp endTime;
	private int peoples;
	private String imageUrl;
	
	private String bak1; //机组ID

	public int getPositionId() {
		return positionId;
	}

	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}

	public int getPositionNo() {
		return positionNo;
	}

	public void setPositionNo(int positionNo) {
		this.positionNo = positionNo;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getShowName() {
		return showName;
	}

	public void setShowName(String showName) {
		this.showName = showName;
	}

	public int getTopicId() {
		return topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public int getHotWordsId() {
		return hotWordsId;
	}

	public void setHotWordsId(int hotWordsId) {
		this.hotWordsId = hotWordsId;
	}

	public int getH5Id() {
		return h5Id;
	}

	public void setH5Id(int h5Id) {
		this.h5Id = h5Id;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public int getSearchType() {
		return searchType;
	}

	public void setSearchType(int searchType) {
		this.searchType = searchType;
	}

	public String getGoUrl() {
		return goUrl;
	}

	public void setGoUrl(String goUrl) {
		this.goUrl = goUrl;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public int getPeoples() {
		return peoples;
	}

	public void setPeoples(int peoples) {
		this.peoples = peoples;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getBak1() {
		return bak1;
	}

	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}
	
	
//	<th data-options="field:'positionNo',resizable:true,align:'center',editor:'text'" width="10%">排序</th>
//	<th data-options="field:'showName',resizable:true,align:'center'" width="30%">应用名称</th>
//	<th data-options="field:'key',resizable:true,align:'center'" width="30%">包名</th>
//	<th data-options="field:'appId',resizable:true,align:'center' " width="10%">应用ID</th>
//    <th data-options="field:'status',resizable:true,align:'center'" width="10%">状态</th>
//    <th data-options="field:'bak1',resizable:true,align:'center'" width="10%">机组ID</th>
	
	
}
