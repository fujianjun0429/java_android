package com.zpkj.quick.manage.pojo;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 广告位
 */
public class AdPosition {
	
	private int id;
    private String positionName;
    private Timestamp createTime; 
    private int positionTypeId;
    private String typeName;
    private int enable;
    private String quickInfoKey ;
    private int positionSort;
    private int forceShowAd;
    private String cpPositionId ;
    
	public int getForceShowAd() {
		return forceShowAd;
	}
	public void setForceShowAd(int forceShowAd) {
		this.forceShowAd = forceShowAd;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
	public int getPositionTypeId() {
		return positionTypeId;
	}
	public void setPositionTypeId(int positionTypeId) {
		this.positionTypeId = positionTypeId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public int getEnable() {
		return enable;
	}
	public void setEnable(int enable) {
		this.enable = enable;
	}
	public String getQuickInfoKey() {
		return quickInfoKey;
	}
	public void setQuickInfoKey(String quickInfoKey) {
		this.quickInfoKey = quickInfoKey;
	}
	public int getPositionSort() {
		return positionSort;
	}
	public void setPositionSort(int positionSort) {
		this.positionSort = positionSort;
	}
	public String getCpPositionId() {
		return cpPositionId;
	}
	public void setCpPositionId(String cpPositionId) {
		this.cpPositionId = cpPositionId;
	}
    
    
    
    

}
