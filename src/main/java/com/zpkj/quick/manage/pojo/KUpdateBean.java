package com.zpkj.quick.manage.pojo;

public class KUpdateBean  {
	
	private int gameId;
	private int versionCode;
	private String versionName;
	private String link;
	private String md5;
	private int status;
	private int ssui;
	private String sort;
	private String enginepkgname;
	private String updateDesc;
	
		
	public int getSsui() {
		return ssui;
	}
	public void setSsui(int ssui) {
		this.ssui = ssui;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getUpdateDesc() {
		return updateDesc;
	}
	public void setUpdateDesc(String updateDesc) {
		this.updateDesc = updateDesc;
	}
	public String getEnginepkgname() {
		return enginepkgname;
	}
	public void setEnginepkgname(String enginepkgname) {
		this.enginepkgname = enginepkgname;
	}
	public int getGameId() {
		return gameId;
	}
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
	public int getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getMd5() {
		return md5;
	}
	public void setMd5(String md5) {
		this.md5 = md5;
	}

	

}
