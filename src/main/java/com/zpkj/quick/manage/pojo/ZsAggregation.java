package com.zpkj.quick.manage.pojo;

import java.util.List;

public class ZsAggregation {
	
	private int id;
    private String aggregationTitle;
    private List<ZsApp> apps;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAggregationTitle() {
		return aggregationTitle;
	}

	public void setAggregationTitle(String aggregationTitle) {
		this.aggregationTitle = aggregationTitle;
	}

	public List<ZsApp> getApps() {
		return apps;
	}

	public void setApps(List<ZsApp> apps) {
		this.apps = apps;
	}
    
    
    

}
