package com.zpkj.quick.manage.pojo;

/**
 * 广告位类型
 */
public class AdPositionType {
	
	private int id;
    private String typeName;
    private String typePreSuffix;
    
	public String getTypePreSuffix() {
		return typePreSuffix;
	}
	public void setTypePreSuffix(String typePreSuffix) {
		this.typePreSuffix = typePreSuffix;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
    
    
    
}
