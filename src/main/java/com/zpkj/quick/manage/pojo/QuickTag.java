package com.zpkj.quick.manage.pojo;

import org.springframework.stereotype.Component;

@Component
public class QuickTag {

	private int id;
	private String name;
	private int catalogId;
	private String catalogName;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(int catalogId) {
		this.catalogId = catalogId;
	}
	public String getCatalogName() {
		return catalogName;
	}
	public void setCatalogName(String catalogName) {
		this.catalogName = catalogName;
	}
	@Override
	public String toString() {
		return "QuickTag [id=" + id + ", name=" + name + ", catalogId=" + catalogId + ", catalogName=" + catalogName
				+ "]";
	}

	
}
