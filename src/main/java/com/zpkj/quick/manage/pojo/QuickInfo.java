package com.zpkj.quick.manage.pojo;

import org.springframework.stereotype.Component;

@Component
public class QuickInfo {
	
	private int id;
	private String key;
	private String name;
	private String minVersion;
	private String info;
	private String sketch;
	private String icon;
	private String engineAction;
	private String engineUri;
	private String enginePackage;
	private int statusId;
	private String status;
	private String catalog2Name;
	private int specialId;
	private String type;
	private int viewTypeId;
	private String viewTypeName;
	private String developer;
	
	private int sort;
	
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public String getDeveloper() {
		return developer;
	}
	public void setDeveloper(String developer) {
		this.developer = developer;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMinVersion() {
		return minVersion;
	}
	public void setMinVersion(String minVersion) {
		this.minVersion = minVersion;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getSketch() {
		return sketch;
	}
	public void setSketch(String sketch) {
		this.sketch = sketch;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getEngineAction() {
		return engineAction;
	}
	public void setEngineAction(String engineAction) {
		this.engineAction = engineAction;
	}
	
	
	
	public String getEngineUri() {
		return engineUri;
	}
	public void setEngineUri(String engineUri) {
		this.engineUri = engineUri;
	}
	public String getEnginePackage() {
		return enginePackage;
	}
	public void setEnginePackage(String enginePackage) {
		this.enginePackage = enginePackage;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCatalog2Name() {
		return catalog2Name;
	}
	public void setCatalog2Name(String catalog2Name) {
		this.catalog2Name = catalog2Name;
	}
	public int getSpecialId() {
		return specialId;
	}
	public void setSpecialId(int specialId) {
		this.specialId = specialId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getViewTypeId() {
		return viewTypeId;
	}
	public void setViewTypeId(int viewTypeId) {
		this.viewTypeId = viewTypeId;
	}
	public String getViewTypeName() {
		return viewTypeName;
	}
	public void setViewTypeName(String viewTypeName) {
		this.viewTypeName = viewTypeName;
	}
	
}
