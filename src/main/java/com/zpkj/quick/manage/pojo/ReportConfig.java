package com.zpkj.quick.manage.pojo;

public class ReportConfig {
    private int id;//int(32) unsigned NOT NULL AUTO_INCREMENT,
    private String desc;//varchar(255) DEFAULT NULL,
    private int versionCode;//varchar(255) DEFAULT NULL,
    private String packageName;//int(255) DEFAULT NULL,
    private String caution;//int(255) DEFAULT NULL,
    private boolean bUseLocalData; //是否使用本地sites配置

    public String getDesc() {
        return desc;
    }

    public String getCaution() {
        return caution;
    }

    public void setCaution(String caution) {
        this.caution = caution;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public boolean isbUseLocalData() {
        return bUseLocalData;
    }

    public void setbUseLocalData(boolean bUseLocalData) {
        this.bUseLocalData = bUseLocalData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
