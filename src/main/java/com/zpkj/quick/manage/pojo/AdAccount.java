package com.zpkj.quick.manage.pojo;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

public class AdAccount {
    
    private int id;
    private String quickKey;
    private Timestamp createTime;
    private Timestamp loginTime;
    private String accountName;
//    private String accountPassword;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQuickKey() {
		return quickKey;
	}
	public void setQuickKey(String quickKey) {
		this.quickKey = quickKey;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Timestamp getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Timestamp loginTime) {
		this.loginTime = loginTime;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
    
    
    
    

}
