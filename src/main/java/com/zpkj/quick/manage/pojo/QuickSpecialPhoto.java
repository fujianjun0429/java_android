package com.zpkj.quick.manage.pojo;

public class QuickSpecialPhoto {
	
	private int id;
	private String info;
	private String local;
	private String jumpaddr;
	private int specialId;
	private String specialName;
	private String specialInfo;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public String getJumpaddr() {
		return jumpaddr;
	}
	public void setJumpaddr(String jumpaddr) {
		this.jumpaddr = jumpaddr;
	}
	public int getSpecialId() {
		return specialId;
	}
	public void setSpecialId(int specialId) {
		this.specialId = specialId;
	}
	public String getSpecialName() {
		return specialName;
	}
	public void setSpecialName(String specialName) {
		this.specialName = specialName;
	}
	public String getSpecialInfo() {
		return specialInfo;
	}
	public void setSpecialInfo(String specialInfo) {
		this.specialInfo = specialInfo;
	}
	@Override
	public String toString() {
		return "QuickSpecialPhoto [id=" + id + ", info=" + info + ", local=" + local + ", jumpaddr=" + jumpaddr
				+ ", specialId=" + specialId + ", specialName=" + specialName + ", specialInfo=" + specialInfo + "]";
	}

	
	
}
