package com.zpkj.quick.manage.pojo;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

public class QuickSpecial {
	
	private int id;
	private String name;
	private String info;
	private int isdefault;
	private Timestamp startTime;
	private Timestamp endTime;
	private String specialIcon;
	
	@JsonFormat(pattern="MM/dd/yyyy HH:mm:ss")
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	
	@JsonFormat(pattern="MM/dd/yyyy HH:mm:ss")
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	
	
	public String getSpecialIcon() {
		return specialIcon;
	}
	public void setSpecialIcon(String specialIcon) {
		this.specialIcon = specialIcon;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public int getIsdefault() {
		return isdefault;
	}
	public void setIsdefault(int isdefault) {
		this.isdefault = isdefault;
	}
	@Override
	public String toString() {
		return "QuickSpecial [id=" + id + ", name=" + name + ", info=" + info + ", isdefault=" + isdefault + "]";
	}
	
	
	
}
