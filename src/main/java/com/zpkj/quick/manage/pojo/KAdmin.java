package com.zpkj.quick.manage.pojo;

public class KAdmin {

	private int id;
	private String userName;//==== 该配置 将带有下划线的表字段映射为驼峰格式的实体类属性 ,mybatis.configuration.map-underscore-to-camel-case=true 
	private String userPassword;
	private int permission;
	
//	id	int	11	0	0	0	-1	0									0	0	0	0	-1	-1	0
	//表字段带下划线
//	user_name	varchar	255	0	-1	0	0	0							utf8	utf8_general_ci	0	0	0	0	0	0	0
//	user_password	varchar	255	0	-1	0	0	0							utf8	utf8_general_ci	0	0	0	0	0	0	0
//	permission	int	11	0	-1	0	0	0				权限由低到高（0-10）					0	0	0	0	0	0	0
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getPermission() {
		return permission;
	}
	public void setPermission(int permission) {
		this.permission = permission;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	
	
}
