package com.zpkj.quick.manage.pojo;

import org.springframework.stereotype.Component;

@Component
public class KViewType {

	private int id;
	private String title;
	private String icon;
	private int num;
	private String action;
	private String viewtype;
	private int sort;
	private int status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getViewtype() {
		return viewtype;
	}
	public void setViewtype(String viewtype) {
		this.viewtype = viewtype;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "KViewType [id=" + id + ", title=" + title + ", icon=" + icon + ", num=" + num + ", action=" + action
				+ ", viewtype=" + viewtype + ", sort=" + sort + ", status=" + status + "]";
	}

	
	
}
