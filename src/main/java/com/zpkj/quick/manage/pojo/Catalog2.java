package com.zpkj.quick.manage.pojo;

import org.springframework.stereotype.Component;

@Component
public class Catalog2 {

	private int id;
	private String name;
	private int catalog1;
	private String catalog1Name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCatalog1() {
		return catalog1;
	}
	public void setCatalog1(int catalog1) {
		this.catalog1 = catalog1;
	}
	public String getCatalog1Name() {
		return catalog1Name;
	}
	public void setCatalog1Name(String catalog1Name) {
		this.catalog1Name = catalog1Name;
	}
	@Override
	public String toString() {
		return "Catalog2 [id=" + id + ", name=" + name + ", catalog1=" + catalog1 + ", catalog1Name=" + catalog1Name
				+ "]";
	}


	
	
	
}
