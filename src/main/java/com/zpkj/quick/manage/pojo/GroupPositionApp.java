package com.zpkj.quick.manage.pojo;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 栏目位置app
 * @author Administrator
 */
public class GroupPositionApp {
	
	private int id;
	private int positionId;
	private int appId;
	private int topicId;
	private int hotWordsId;
	private int h5Id;
	private String actionType;
	private int searchType;
	private String goUrl;
	private Timestamp startTime;
	private Timestamp endTime;
	private int peoples;
	private String imageUrl;
	private String bak1;
	
	private String selectType;
	private Integer selectId;
	
	private String showName;
	
	public String getShowName() {
		return showName;
	}
	public void setShowName(String showName) {
		this.showName = showName;
	}
	
	public String getSelectType() {
		return selectType;
	}
	public void setSelectType(String selectType) {
		this.selectType = selectType;
	}
	public Integer getSelectId() {
		return selectId;
	}
	public void setSelectId(Integer selectId) {
		this.selectId = selectId;
	}
	public String getBak1() {
		return bak1;
	}
	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPositionId() {
		return positionId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	public int getTopicId() {
		return topicId;
	}
	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}
	public int getHotWordsId() {
		return hotWordsId;
	}
	public void setHotWordsId(int hotWordsId) {
		this.hotWordsId = hotWordsId;
	}
	public int getH5Id() {
		return h5Id;
	}
	public void setH5Id(int h5Id) {
		this.h5Id = h5Id;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public int getSearchType() {
		return searchType;
	}
	public void setSearchType(int searchType) {
		this.searchType = searchType;
	}
	public String getGoUrl() {
		return goUrl;
	}
	public void setGoUrl(String goUrl) {
		this.goUrl = goUrl;
	}
	@JsonFormat(pattern="MM/dd/yyyy HH:mm:ss")
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	@JsonFormat(pattern="MM/dd/yyyy HH:mm:ss")
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public int getPeoples() {
		return peoples;
	}
	public void setPeoples(int peoples) {
		this.peoples = peoples;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	
	
}
