package com.zpkj.quick.manage.pojo;

import org.springframework.stereotype.Component;

@Component
public class SearchContentType {
	
//	id	int	11	0	0	0	-1	0									0	0	0	0	-1	0	0
//	title	varchar	255	0	-1	0	0	0				类别名称			utf8	utf8_general_ci	0	0	0	0	0	0	0
//	typeStr	varchar	255	0	-1	0	0	0				类别字符串			utf8	utf8_general_ci	0	0	0	0	0	0	0
//	typeNum	int	11	0	-1	0	0	0				类别数字（对应searchType）					0	0	0	0	0	0	0

	
	private int id;
	private String title;
	private String typeStr;
	private String typeNum;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTypeStr() {
		return typeStr;
	}
	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}
	public String getTypeNum() {
		return typeNum;
	}
	public void setTypeNum(String typeNum) {
		this.typeNum = typeNum;
	}

	
	
	
}
