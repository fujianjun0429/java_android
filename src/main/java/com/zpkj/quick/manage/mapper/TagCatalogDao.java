package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.TagCatalog;

@Mapper
public interface TagCatalogDao {
	
	void insertTagCatalog(String name);
	void deleteTagCatalog(int id);
	void updateTagCatalog(@Param("id") int id,@Param("name") String name);
	TagCatalog selectTagCatalogById(int id);
	List<TagCatalog> selectTagCatalogList();
	
	
}
