package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.KViewType;

@Mapper
public interface KViewTypeDao {
	
	void insertKViewType(@Param("title") String title, @Param("icon") String icon, @Param("num") int num,
			@Param("action") String action, @Param("viewtype") String viewtype, @Param("sort") int sort,
			@Param("status") int status);

	void deleteKViewType(int id);

	void updateKViewType(@Param("id") int id, @Param("title") String title, @Param("icon") String icon,
			@Param("num") int num, @Param("action") String action, @Param("viewtype") String viewtype,
			@Param("sort") int sort, @Param("status") int status);

	List<KViewType> selectKViewTypeList();
	
}
