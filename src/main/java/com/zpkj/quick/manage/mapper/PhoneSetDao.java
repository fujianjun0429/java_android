package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.PhoneSet;
import com.zpkj.quick.manage.pojo.SearchContentType;

@Mapper
public interface PhoneSetDao {
	
	Integer insertPhoneSet(
			@Param("name") String name,
			@Param("phoneNick") String phoneNick,
			@Param("searchType") int searchType
			);
	void deletePhoneSet(int id);
	Integer updatePhoneSet(
			@Param("id") int id,
			@Param("name") String name,
			@Param("phoneNick") String phoneNick,
			@Param("searchType") int searchType
			);
	
	PhoneSet selectPhoneSetById(int id);
	//====如果没有@Param("xxx")，会出错  org.mybatis.spring.MyBatisSystemException: nested exception is org.apache.ibatis.reflection.ReflectionException: There is no getter for property named 'phoneNick' in 'class java.lang.String'
	List<PhoneSet> selectPhoneList(@Param("phoneNick") String phoneNick);
	
	List<SearchContentType> selectSearchContentTypeList();

	
}
