package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.AdAccount;
import com.zpkj.quick.manage.pojo.AdPosition;
import com.zpkj.quick.manage.pojo.AdPositionSdk;
import com.zpkj.quick.manage.pojo.AdPositionType;
import com.zpkj.quick.manage.pojo.AdSdk;
import com.zpkj.quick.manage.pojo.QuickInfo;

@Mapper
public interface AdDao {

//	public AdConfig selectAdConfig(@Param("pkgName") String pkgName);
//	
//	public List<AdPosition> selectAdPositions(
//			@Param("positionId") int positionId,
//			@Param("quickInfoKey") String quickInfoKey
//			);
	
	public List<AdPositionSdk> getPositionAd(
			@Param("quickInfoKey") String quickInfoKey,
			@Param("cpPositionId") String cpPositionId
			);
	
	public AdPosition selectPosition(
			@Param("positionTypeId") int positionTypeId, 
			@Param("quickInfoKey") String quickInfoKey);
	
	public AdPosition selectPositionByCppositionId(
			@Param("cpPositionId") String cpPositionId
			);
	
	public int updatePosition( @Param("positionId") int positionId, 
			@Param("positionName") String positionName, 
			@Param("forceShowAd") int forceShowAd);
	public int updatePositionCP( @Param("positionId") int positionId, 
			@Param("cpPositionId") String cpPositionId);
	
	public int insert2Position(AdPosition position);
	
	public int insertOrUpdatemPositionSdk(
			@Param("thirdAdId") String thirdAdId, 
			@Param("positionId") int positionId, 
			@Param("sdkId") int sdkId,
			@Param("thisSdkShow") int thisSdkShow 
			);
	
	public int enablemPositionSdk(
			@Param("thisSdkShow") int thisSdkShow,  
			@Param("positionId") int positionId,  
			@Param("sdkId")int sdkId);
	
	//查询广告类型列表
	public List<AdPositionType> selectAdPositionTypes();
	public AdPositionType selectAdPositionType(@Param("typeId") int typeId);
	//查询sdk列表
	public List<AdSdk> selectSdks();
	
	//账号相关
	public int register(
			@Param("quickKey") String quickKey, 
			@Param("accountName") String accountName, 
			@Param("accountPassword") String accountPassword); //md5值
	public AdAccount allreayRegister(
			@Param("quickKey") String quickKey); //有账号通过这个快应用注册过
	public AdAccount sameAccount(
			@Param("accountName") String accountName
		    ); //账号相同
	
	public AdAccount login(
			@Param("accountName") String accountName, 
			@Param("accountPassword") String accountPassword
			); 
	
	//认领app
	public int claimApp(
			@Param("appId") String appId, 
			@Param("quickKey") String quickKey,
			@Param("accountId") int accountId
			); 
	
	public QuickInfo selectAppByKey(
			@Param("quickKey") String quickKey
			); 
	
}
