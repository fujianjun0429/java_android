package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.Rpk;

@Mapper
public interface RpkDao {
	
	Integer saveRpk( Rpk rpk);
	
	//插入并获取自增长的 ID解决方法是：不加  @Param("groupPositionNew"), mapper.xml文件中，直接用属性引用。如： #{groupId} 而不是 #{groupPositionNew.groupId}
	/*<!-- 有则更新，无则插入 , appId为主键 -->
	<insert id="claimApp"  parameterType="java.util.Map" >
		INSERT INTO ad_claim_app(appId,quickKey,accountId)
		VALUES (#{appId},#{quickKey},#{accountId})
  		ON DUPLICATE KEY UPDATE 
  		quickKey=#{quickKey},
  		accountId=#{accountId}
	</insert>*/

	List<Rpk> selectRpks(
			@Param("rpkName") String rpkName,
			@Param("rpkPkgName") String rpkPkgName
			);
	List<Rpk> selectAllRpks();
	Integer delRpk(@Param("rpkPkgName") String rpkPkgName);
	
	
}
