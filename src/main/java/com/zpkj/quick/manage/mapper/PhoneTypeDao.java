package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.PhoneType;

@Mapper
public interface PhoneTypeDao {
	
	void insertPhoneType(@Param("name") String name,@Param("groupId") int groupId);
	void deletePhoneType(int id);
	void updatePhoneType(@Param("id") int id,@Param("name") String name,@Param("groupId") int groupId);
	PhoneType selectPhoneTypeById(int id);
	List<PhoneType> selectPhoneTypeByGroupId(int groupId);
	
	List<PhoneType> selectPhoneTypeList(@Param("name") String name);
	
}
