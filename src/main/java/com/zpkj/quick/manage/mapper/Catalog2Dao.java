package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.Catalog2;

@Mapper
public interface Catalog2Dao {
	
	List<Catalog2> selectCatalog2List();
	List<Catalog2> selectCatalog2ByCatalog1Id(int catalog1Id);
	void insertCatalog2(@Param("name") String name,@Param("catalog1") int catalog1);
	void deleteCatalog2(int catalog2Id);
	void updateCatalog2(@Param("id") int id,@Param("name") String name,@Param("catalog1") int catalog1);
	Catalog2 selectCatalog2ById(int catalog2Id);
	
}
