package com.zpkj.quick.manage.mapper;

import java.sql.Timestamp;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.QuickSpecial;
import com.zpkj.quick.manage.pojo.SpecialApp;

@Mapper
public interface QuickSpecialDao {

   int insertSpecial(
		   @Param("name") String name, 
		   @Param("info") String info, 
		   @Param("isdefault") int isdefault,
		   @Param("startTime") Timestamp startTime,
		   @Param("endTime") Timestamp endTime,
		   @Param("specialIcon") String specialIcon
		   );

	void deleteSpecial(int id);

	int updateSpecial(@Param("id") int id, 
				@Param("name") String name,
				@Param("info") String info,
				@Param("isdefault") int isdefault,
				@Param("startTime") Timestamp startTime,
				@Param("endTime") Timestamp endTime,
				@Param("specialIcon") String specialIcon
			);

	QuickSpecial selectSpecialById(int id);

	List<QuickSpecial> selectSpecialList();
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	List<SpecialApp> selectSpecialApps(@Param("specialId") int specialId);
	
	SpecialApp selectSpecialApp(
			@Param("specialId") Integer specialId,
			@Param("appId") Integer appId
			);
	
	int addAppToSpecial(
			@Param("specialId") Integer specialId,
			@Param("appId") Integer appId
			);
	int deleteAppToSpecial(
			@Param("specialId") Integer specialId,
			@Param("appId") Integer appId
			);
	

}
