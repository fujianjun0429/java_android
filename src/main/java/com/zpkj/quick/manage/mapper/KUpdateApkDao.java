package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.CheckUpdateBean;
import com.zpkj.quick.manage.pojo.KUpdateBean;

@Mapper
public interface KUpdateApkDao {
	
	Integer saveKupdate(@Param("updateBean") KUpdateBean updateBean);

	List<KUpdateBean> selectUpdateApks();
	
	KUpdateBean selectOneUpdateApk(@Param("app") CheckUpdateBean app);
	
}
