package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.Catalog1;

@Mapper
public interface Catalog1Dao {
	
	List<Catalog1> selectCatalog1List();
	void insertCatalog1(String catalog1Name);
	void deleteCatalog1(int catalog1Id);
	void updateCatalog1(@Param("id") int id,@Param("name") String name);
	Catalog1 selectCatalog1ById(int catalog1Id);
	
}
