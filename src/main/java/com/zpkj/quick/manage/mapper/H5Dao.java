package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.GameGift;
import com.zpkj.quick.manage.pojo.H5Bean;

@Mapper
public interface H5Dao {

   int insertH5(
		   @Param("name") String name, 
		   @Param("url") String url, 
		   @Param("imageUrl") String imageUrl,
		   @Param("status") Integer status
		   );
   int deleteH5(
		   @Param("id") int id
		   );


	List<H5Bean> selectH5s();

	public Integer saveOrder(
			@Param("userId")String userId,
			@Param("merchantNo") String merchantNo,
			@Param("orderAmount")String orderAmount,
			@Param("orderNo")	String orderNo  ,
			@Param("wtfOrderNo") String wtfOrderNo,
			@Param("orderStatus") int orderStatus,
			@Param("sign") String sign,
			@Param("payTime") String  payTime,
			@Param("productName") String productName,
			@Param("productDesc") String productDesc,
			@Param("remark") String remark);
	
	public Integer seleceOrder(@Param("orderNo") String orderNo);
	public Integer selectShouChong(@Param("userId") String userId);
	
	public GameGift selectGameGift	(
			@Param("szGroup") String szGroup,
			@Param("nGroupIndex") String nGroupIndex
			);
	
}
