package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.ConnAppRecommend;

@Mapper
public interface ConnAppRecommendDao {

	void insertConnAppRecommend(@Param("quickId")int quickId,@Param("recommendId")int recommendId);
	
	Integer  selectConnAppRecommend(@Param("quickId")int quickId,@Param("recommendId")int recommendId);
	
	List<ConnAppRecommend> selectQuickByRecommend(int recommendId);
	void deleteConnAppRecommend(@Param("quickId")int quickId,@Param("recommendId")int recommendId);
	
}
