package com.zpkj.quick.manage.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.KAdmin;

@Mapper
public interface KAdminDao {
	
	KAdmin selectUser(@Param("userName") String userName);
	
}
