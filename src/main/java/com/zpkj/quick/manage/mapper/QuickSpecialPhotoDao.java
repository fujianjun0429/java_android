package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.QuickSpecialPhoto;

@Mapper
public interface QuickSpecialPhotoDao {

	void insertSpecialPhoto(@Param("info") String info,@Param("local") String local,@Param("jumpaddr") String jumpaddr,@Param("specialId") int specialId);
	void deleteSpecialPhoto(int id);
	void updateSpecialPhoto(@Param("id") int id,@Param("info") String info,@Param("local") String local,@Param("jumpaddr") String jumpaddr,@Param("specialId") int specialId);
	QuickSpecialPhoto selectSpecialPhotoById(int id);
	List<QuickSpecialPhoto> selectPhotoBySpecialId(int specialId);
	List<QuickSpecialPhoto> selectPhotoList(@Param("specialId") int specialId);
	
}
