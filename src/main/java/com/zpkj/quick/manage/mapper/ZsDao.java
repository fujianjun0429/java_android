package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.ZsAggregation;
import com.zpkj.quick.manage.pojo.ZsApp;

@Mapper
public interface ZsDao {

	
	public List<String> getQuestions(
			@Param("appId") int appId
			);
	
	public List<ZsApp> getApps(
			@Param("aggregationId") int aggregationId
			);
	
	public List<ZsAggregation> getAggregations(
			);
	
	
}
