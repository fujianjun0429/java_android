package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.QuickWords;

@Mapper
public interface WordsDao {

	void insertWords(@Param("name") String name, @Param("status") int status);

	void deleteWords(int id);

	void updateWords(@Param("id") int id, @Param("name") String name, @Param("status") int status);

	QuickWords selectWordsById(int id);

	List<QuickWords> selectWordsList();
	
}
