package com.zpkj.quick.manage.mapper;

import java.sql.Timestamp;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.GroupBatchApp;
import com.zpkj.quick.manage.pojo.GroupIdBean;
import com.zpkj.quick.manage.pojo.GroupPositionApp;
import com.zpkj.quick.manage.pojo.GroupPositionBean;

@Mapper
public interface GroupDao {

   int insertGroup(
		   @Param("title") String title, 
		   @Param("info") String info, 
		   @Param("sort") int sort, 
		   @Param("descGroup") String descGroup,
		   @Param("viewType") String viewType,
		   @Param("groupAction") String groupAction,
		   @Param("productType") String productType,
		   @Param("status") int status,
		   @Param("startTime") Timestamp startTime,
		   @Param("endTime") Timestamp endTime
		   );

	int updateGroup(
			   @Param("groupId") int groupId, 
			   @Param("title") String title, 
			   @Param("info") String info, 
			   @Param("sort") int sort, 
			   @Param("descGroup") String descGroup,
			   @Param("viewType") String viewType,
			   @Param("groupAction") String groupAction,
			   @Param("productType") String productType,
			   @Param("status") int status,
			   @Param("startTime") Timestamp startTime,
			   @Param("endTime") Timestamp endTime
			);

	List<GroupIdBean> selectGroups(
			@Param("productType") String productType,
			@Param("groupId") int groupId,
			@Param("onlyVorH") int onlyVorH
			);

	GroupIdBean selectGroup(@Param("viewType") String viewType);
	
	Integer selectMaxGroupId();
	
	int deleteGroup(
			@Param("groupId") Integer groupId
			);
	
	
	//////////////////////////////////////////////
	int insertGroupPosition(
			   @Param("groupId") int groupId, 
			   @Param("positionNo") int positionNo, 
			   @Param("positionDesc") String positionDesc, 
			   @Param("status") int status
			   );

		int updateGroupPosition(
				   @Param("id") int id, 
				   @Param("groupId") int groupId, 
				   @Param("positionNo") int positionNo, 
				   @Param("positionDesc") String positionDesc, 
				   @Param("status") int status
				);

		List<GroupPositionBean> selectGroupPositions(@Param("groupId") int groupId);
		
		int deleteGroupPosition(
				@Param("id") Integer id
				);
	////////////////////////////////////////////////////
		int insertApp2Position(
				   @Param("positionId") int positionId, 
				   @Param("appId") int appId, 
				   @Param("topicId") int topicId, 
				   @Param("hotWordsId") int hotWordsId, 
				   @Param("h5Id") int h5Id, 
				   @Param("actionType") String actionType, 
				   @Param("searchType") int searchType, 
				   @Param("goUrl") String goUrl, 
				   @Param("startTime") Timestamp startTime, 
				   @Param("endTime") Timestamp endTime, 
				   @Param("peoples") int peoples,
				   @Param("bak1") String bak1,//bak1用于存放机组ID
				   @Param("imageUrl") String imageUrl 
				   );
		int updateApp2Position(
				@Param("id") int id, 
				@Param("positionId") int positionId, 
				@Param("appId") int appId, 
				@Param("topicId") int topicId, 
				@Param("hotWordsId") int hotWordsId, 
				@Param("h5Id") int h5Id, 
				@Param("actionType") String actionType, 
				@Param("searchType") int searchType, 
				@Param("goUrl") String goUrl, 
				@Param("startTime") Timestamp startTime, 
				@Param("endTime") Timestamp endTime, 
				@Param("peoples") int peoples,
				 @Param("bak1") String bak1,//bak1用于存放机组ID
				@Param("imageUrl") String imageUrl 
				);
		int updateAllBak1(
				@Param("bak1") String bak1//bak1用于存放机组ID
				);
		
		List<GroupPositionApp> selectAppOfPositions(@Param("positionId") int positionId);
		List<GroupBatchApp> selectAppOfGroup(
				@Param("viewType") String viewType, 
				@Param("groupId") int groupId);
		
			
			int deleteAppOfPosition(
					@Param("id") Integer id
					);
 //////////////////////////////////////改版//////////////////////////////////////////////////////////////////
			GroupPositionBean selectGroupPosition(@Param("groupId") int groupId,
					@Param("positionNo") int positionNo);

			int deletePositionApps(@Param("positionId") int positionId);
			//org.apache.ibatis.reflection.ReflectionException: There is no getter for property named groupPositionNew 
			//不加  @Param("groupPositionNew")会有上面错误。但加了后，自动获取增长的 ID 就无效。 
			//====(sql)插入并获取自增长的 ID解决方法是：不加  @Param("groupPositionNew"), mapper.xml文件中，直接用属性引用。如： #{groupId} 而不是 #{groupPositionNew.groupId}
			int insertGroupPositionBean(GroupPositionBean groupPositionNew);
			
			int batchInsertApp(@Param("groupBatchApps") List<GroupBatchApp> groupBatchApps );
			
}
