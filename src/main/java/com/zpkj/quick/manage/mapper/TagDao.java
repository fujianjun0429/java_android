package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.QuickTag;

@Mapper
public interface TagDao {
	
	void insertQuickTag(@Param("name") String name, @Param("catalogId") int catalogId);

	void deleteQuickTag(int id);

	void updateQuickTag(@Param("id") int id, @Param("name") String name, @Param("catalogId") int catalogId);

	QuickTag selectTagById(int id);

	List<QuickTag> selectTagByCatalogId(int catalogId);

	List<QuickTag> selectTagList();
	
	
}
