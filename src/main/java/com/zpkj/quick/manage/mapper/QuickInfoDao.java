package com.zpkj.quick.manage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.zpkj.quick.manage.pojo.QuickInfo;


@Mapper
public interface QuickInfoDao {
	
	Integer allOnOfOff(
			@Param("status") int status);
	
	List<QuickInfo> selectQuickInfoList(@Param("appName") String appName, 
			@Param("offset") int offset);
	List<QuickInfo> selectQuickInfoListOnlyOnline(@Param("appName") String appName
			 );
	
	List<QuickInfo> onlineQuickInfoList(@Param("appName") String appName, 
			@Param("offset") int offset);
	
	Integer updateQuickInfoImage(@Param("appId") int appId,@Param("picUrl") String picUrl);
	
	QuickInfo selectQuickInfoById(int id);
	List<QuickInfo> selectQuickInfoListByWord(@Param("name") String name);
	List<QuickInfo> selectQuickInfoListByKey(String key);
	void updateQuickStatusOnline(int id);
	void updateQuickStatusOffline(int id);
	
	Integer updateQuickEdit(
			@Param("id") int id,
			@Param("name")    String name,
			@Param("key")    String key,
			@Param("info")    String info,
			@Param("engineUri")    String engineUri,
			@Param("enginePackage")    String enginePackage,
			@Param("engineAction")    String engineAction,
			@Param("type")    String type,
			@Param("icon")    String icon,
			@Param("minVersion")    String minVersion,
			@Param("statusId")    int statusId
			);
	Integer addQuickEdit(
			@Param("name")    String name,
			@Param("pinYin")    String pinYin,
			@Param("pinYinBrief")    String pinYinBrief,
			@Param("key")    String key,
			@Param("info")    String info,
			@Param("engineUri")    String engineUri,
			@Param("enginePackage")    String enginePackage,
			@Param("engineAction")    String engineAction,
			@Param("type")    String type,
			@Param("icon")    String icon,
			@Param("minVersion")    String minVersion,
			@Param("statusId")    int statusId
			);
	
	
	void insertQuick(@Param("key")String key,@Param("name")String name,@Param("info")String info);
	
}
