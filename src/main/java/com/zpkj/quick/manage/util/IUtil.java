package com.zpkj.quick.manage.util;

public final class IUtil {

	private IUtil() {
	};
	
    public static Integer check(Integer i){
    	i = (null == i)?0:i;
    	return i;
    }
    
    public static Long check(Long i){
    	i = (null == i)?0:i;
    	return i;
    }
    
    public static int getPercent(Integer i, Integer last){
    	int perent= 0;
    	if(null != last && last >0){
    		perent=	(i - last) * 100 / last;
    	}else{
			if (null != i && i > 0) {
    			perent = 100;
    		}
    	}
    			
    	return perent;
    }

    public static Float getPercentf(Float i, Float last){
    	Float perent= 100f;
		if(null != last && last >0){
    		perent=	(i - last) * 100 / last;
    	}
    			
    	return perent;
    }
}
