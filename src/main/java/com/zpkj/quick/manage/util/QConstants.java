package com.zpkj.quick.manage.util;

public interface QConstants {
	public final static String SUCCESS = "success";
	public final static int SUCCESS_CODE =0;
	public final static int SUCCESS_CODE_200 =200;
	public final static String ERROR = "error";
	public final static int ERROR_CODE = -1;

}
