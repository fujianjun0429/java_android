package com.zpkj.quick.manage.util.http;

import java.security.KeyStore;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class HttpClientManager {
    private static final String TAG = HttpClientManager.class.getSimpleName();
    private HttpParams mHttpParams;
    ClientConnectionManager ccm;
    private final static int MAX_TOTAL_CONNECTIONS = 10; // 最大连接数
    private final static int WAIT_TIMEOUT = 15000; //获取连接的最大等待时间
    private final static int MAX_ROUTE_CONNECTIONS = 30; //每个路由最大连接数
    private final static int CONNECT_TIMEOUT = 15000; // 连接超时时间
    private final static int READ_TIMEOUT = 15000; //读取超时时间
    private static HttpClientManager instance = null;

    public HttpClientManager( ) {
    }

    public static HttpClientManager getInstance( ) {
        if (null == instance) {
            instance = new HttpClientManager();
        }
        return instance;
    }
//    public AndroidHttpClient getAndroidHttpClient() {
//        AndroidHttpClient httpClient = AndroidHttpClient.newInstance(null, m);
//        return httpClient;
//    }
    
    public DefaultHttpClient getSimpleHttpClient() {
        HttpParams params = new BasicHttpParams();
        // 设置网络超时参数
        HttpConnectionParams.setConnectionTimeout(params, 15000);
        HttpConnectionParams.setSoTimeout(params, 15000);
        DefaultHttpClient mHttpClient = new DefaultHttpClient(params);
        mHttpClient.setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(3, true));

        return mHttpClient;
    }

    public HttpClient getHttpClient() {
        setParams();
        DefaultHttpClient httpclient = new DefaultHttpClient(ccm,mHttpParams);
        // 连接中断，重试三次
        httpclient.setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(3, true));
//        httpclient.setKeepAliveStrategy(new ConnectionKeepAliveStrategy() {
//            @Override
//            public long getKeepAliveDuration(HttpResponse response, Http ) {
//                // 实现'keep-alive'头部信息
//                HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
//                while (it.hasNext()) {
//                    HeaderElement he = it.nextElement();
//                    String param = he.getName();
//                    String value = he.getValue();
//                    if (value != null && param.equalsIgnoreCase("timeout")) {
//                        try {
//                            return Long.parseLong(value) * 1000;
//                        }
//                        catch (NumberFormatException ignore) {
//                        }
//                    }
//                }
//
//                HttpHost target = (HttpHost) .getAttribute(Execution.HTTP_TARGET_HOST);
//                if ("hispaceclt.hicloud.com".equalsIgnoreCase(target.getHostName())) {
//                    // 只保持活动60秒
//                    return 60 * 1000;
//                }
//                else {
//                    // 否则保持活动30秒
//                    return 30 * 1000;
//                }
//            }
//        });
        
        return httpclient;
    }
    
    private void setParams() {
        mHttpParams = new BasicHttpParams();
        // 设置最大连接数
        ConnManagerParams.setMaxTotalConnections(mHttpParams, MAX_TOTAL_CONNECTIONS);
        // 设置获取连接的最大等待时间
        ConnManagerParams.setTimeout(mHttpParams, WAIT_TIMEOUT);

        // 设置每个路由最大连接数
        ConnPerRouteBean connPerRoute = new ConnPerRouteBean(MAX_ROUTE_CONNECTIONS);
        ConnManagerParams.setMaxConnectionsPerRoute(mHttpParams, connPerRoute);
        // 对localhost:80增加最大连接到50
        HttpHost httpHost = new HttpHost("localhost", 80);
        connPerRoute.setMaxForRoute(new HttpRoute(httpHost), 50);

        // 设置连接超时时间 和读取超时时间
        HttpConnectionParams.setConnectionTimeout(mHttpParams, CONNECT_TIMEOUT);
        HttpConnectionParams.setSoTimeout(mHttpParams, READ_TIMEOUT);

       // 设置http https支持
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));
            ccm= new ThreadSafeClientConnManager(mHttpParams, registry);
            //HttpClient client = new DefaultHttpClient(ccm, mHttpParams);
        }
        catch (Exception e) {
        }
        
    }

    /**
     * 设置代理服务器
     */
//    public  void setWapDefaultProxy( ) {
//        String proxyHost = Proxy.getHost();
//        int proxyPort = Proxy.getPort();
//        HttpHost proxy = null;
//        if (proxyHost != null && proxyHost.length() > 0 && proxyPort != -1) {
//            proxy = new HttpHost(proxyHost, proxyPort);
//        }
//        mDefaulthttpHostProxy = proxy;
//    }
    
    /**
     * 设置代理服务器
     */
//    public  void setWapDefaultProxy() {
//        HttpHost proxy = null;
//        proxy = new HttpHost("XX.XX.XX.XX", 80);
//        mDefaulthttpHostProxy = proxy;
//    }
//
//    public  HttpHost getWapDefaultProxy() {
//        return mDefaulthttpHostProxy;
//    }
//
//    /**
//     * 获取PS域类型
//     * @param netInfo
//     * @return int
//     */
//    private static int getPsType(NetworkInfo netInfo) {
//        int psType = NetType.TYPE_UNKNOWN;
//        if (netInfo != null && netInfo.isConnected()) {
//            int netType = netInfo.getType();
//            if (ConnectivityManager.TYPE_WIFI == netType) {
//                psType = NetType.TYPE_WIFI;
//            }
//            else if (ConnectivityManager.TYPE_MOBILE == netType) {
//               // 移动和联通的2G为GPRS或EDGE，电信的2G为CDMA，电信的3G为EVDO,联通的3G为UMTS或HSDPA
//                switch (netInfo.getSubtype()) {
//                    case TelephonyManager.NETWORK_TYPE_GPRS:
//                    case TelephonyManager.NETWORK_TYPE_EDGE:
//                    case TelephonyManager.NETWORK_TYPE_CDMA:
//                    case TelephonyManager.NETWORK_TYPE_1xRTT:
//                    case TelephonyManager.NETWORK_TYPE_IDEN: {
//                        psType = NetType.TYPE_2G;
//                        break;
//                    }
//                    case TelephonyManager.NETWORK_TYPE_UMTS:
//                    case TelephonyManager.NETWORK_TYPE_HSDPA:
//                    case TelephonyManager.NETWORK_TYPE_EVDO_0:
//                    case TelephonyManager.NETWORK_TYPE_EVDO_A:
//                    case TelephonyManager.NETWORK_TYPE_HSUPA:
//                    case TelephonyManager.NETWORK_TYPE_HSPA: {
//                        psType = NetType.TYPE_3G;
//                        break;
//                    }
//                    default: {
//                        psType = NetType.TYPE_2G;// 默认为2G，确保应用至少能够被下载
//                    }
//                }
//            }
//            else if (ConnectivityManager.TYPE_WIMAX == netType) {
//                psType = NetType.TYPE_4G;
//            }
//        }
//        return psType;
//    }
//
//    /**
//     * 获取当前数据连接的网络类型，例如2G，3G，WIFI等
//     */
//    public static int getPsType( ) {
//        NetworkInfo networkInfo = getActiveNetworkInfo();
//        if (null == networkInfo) {
//            return NetType.TYPE_UNKNOWN;
//        }
//        psType = getPsType(networkInfo);
//
//        if (NetType.TYPE_WIFI != psType) {
//            // 2G 3G 4G 都有可能是wap/net网络
//            if (isWap(networkInfo, )) {
//                return NetType.WAP;
//            }
//            else {
//                return NetType.NET;
//            }
//        }
//        return psType;
//    }
//
//    /**
//     * 获取活动网络连接信息
//     */
//    private static NetworkInfo getActiveNetworkInfo( ) {
//        ConnectivityManager mConnMgr = (ConnectivityManager) .getSystemService(.CONNECTIVITY_SERVICE);
//        NetworkInfo aActiveInfo = mConnMgr.getActiveNetworkInfo();
//        return aActiveInfo;
//    }
//
//    /**
//     * 判断是否WAP连接
//     */
//    private static boolean isWap(NetworkInfo netInfo,  ) {
//        if(netInfo == null) return false;
//        // 如果是使用的运营商网络, 且是wap
//        if (netInfo.getType() == ConnectivityManager.TYPE_MOBILE && ApnUtil.getInstance().isWap()) {
//            return true;
//        }
//        return false;
//    }

    /**
     * 数据网络类型
     */
    public interface NetType {
        public static final int WAP = -3;
        public static final int NET = -2;
        public static final int TYPE_NEED_INIT = -1;
        public static final int TYPE_UNKNOWN = 0;
        public static final int TYPE_WIFI = 1;
        public static final int TYPE_2G = 2;
        public static final int TYPE_3G = 3;
        public static final int TYPE_4G = 4;
    }

}