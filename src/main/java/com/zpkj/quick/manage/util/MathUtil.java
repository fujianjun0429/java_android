package com.zpkj.quick.manage.util;

import java.util.Random;

public class MathUtil {
    
    /**
     * 获取随机数 
     * 带种子： 下次程序运行，返回结果都是一样的
     * @param seed 
     * @param range
     */
    public static int getRandomWithSeed(int seed, int range) {
        int nRes = 0;
        Random random = new Random(seed);
        nRes = random.nextInt(range);
        return nRes;
    }
    
    /**
     * 获取随机数
     * @param range
     */
    public static int getRandomInt(int range) {
        int nRes = 0;
        if (range > 0) {
            Random random = new Random();
            nRes = random.nextInt(range);
        }
        return nRes;
    }
    
    public static int getRandom(int start, int end){
    	Random random = new Random();
    	int ret= random.nextInt(end)+start;
    	return ret;
    }
}
