package com.zpkj.quick.manage.util;

import java.security.MessageDigest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AndroidMD5 {
	private static final Logger LOGGER = LoggerFactory.getLogger(AndroidMD5.class);
    private static final String TAG = "MD5";

    private static final char HEX_DIGITS[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
            'E', 'F' };

    public static String getMd5Str(String s) {
        String res = "";
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();
            res = toHexString(messageDigest);
        }
        catch (Exception e) {
            LOGGER.error(TAG, "MD5.toMd5 Exception", e);
        }
        return res;
    }
    
    public static String getMd5Str(byte [] byteData) {
        String res = "";
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(byteData);
            byte messageDigest[] = digest.digest();
            res = toHexString(messageDigest);
            LOGGER.error(TAG, "MD5:convet_str = " + res);
        }
        catch (Exception e) {
            LOGGER.error(TAG, "MD5.toMd5 Exception", e);
        }
        return res;
    }
    
//    /**
//     * MD5加密
//     */
//    public static byte[] encryptMD5(byte[] data) throws Exception {
//        MessageDigest md5 = MessageDigest.getInstance(KEY_MD5);
//        md5.update(data);
//        return md5.digest();
//    }


    private static String toHexString(byte[] b) {
        // String to byte
        StringBuilder sb = new StringBuilder(b.length * 2);
        for (int i = 0; i < b.length; i++) {
            sb.append(HEX_DIGITS[(b[i] & 0xf0) >>> 4]);
            sb.append(HEX_DIGITS[b[i] & 0x0f]);
        }
        return sb.toString();
    }
}