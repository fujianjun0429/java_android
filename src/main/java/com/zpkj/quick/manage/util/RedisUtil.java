package com.zpkj.quick.manage.util;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisUtil {
	// ====redis用法： 
	/*
	 * @Autowired
	   RedisUtil mRedisUtil;
	 * 
	 * mRedisUtil.setStrWithCacheTime(url, res,CACHE_TIME_MINITUE);
	 * mRedisUtil.getStr(url);
	 */
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	
	public void setStr(String key, String value){
		stringRedisTemplate.opsForValue().set(key, value);
	}
	
	public void setStrWithCacheTime(String key, String value, int minitue){
		stringRedisTemplate.opsForValue().set(key, value,minitue,TimeUnit.MINUTES);
	}
	
	public String getStr(String key ){
		return stringRedisTemplate.opsForValue().get(key);
	}
	
	public void setObject(String key, Object value){
		String valueStr = JSONUtils.toJson(value);
		stringRedisTemplate.opsForValue().set(key, valueStr);
	}
	public void setObject(String key, Object value, int minitue){
		String valueStr = JSONUtils.toJson(value);
		stringRedisTemplate.opsForValue().set(key, valueStr, minitue,TimeUnit.MINUTES);
	}
	
    //如果不存在则插入，返回true为插入成功,false失败
    public boolean setIfAbsent(String key, String value) {
        return stringRedisTemplate.opsForValue().setIfAbsent(key, value);
    }
	
	//AdPositionType a = mRedisUtil.getObject(key, AdPositionType.class);
	public <T> T getObject(String key, Class<T> clazz) {
		String jsonStr = stringRedisTemplate.opsForValue().get(key);
		return JSONUtils.fromJson(jsonStr, clazz);
	}
	
	/////////////////////////list操作///////////////////////////////////////////////
	public List<String> findList(String key, int start, int end) {
		return stringRedisTemplate.opsForList().range(key, start, end);
	}

	/**
	 * @Description 插入多条数据
	 */
	public long setList(String key, List<String> value) {
		return stringRedisTemplate.opsForList().rightPushAll(key, value);
	}
	
	//////////////////////////////Hash操作////////////////////////////////////////////
    public Map<Object, Object> findHash(String key) {
        return stringRedisTemplate.opsForHash().entries(key);
    }

    /**
     * @Desscription 插入hash数据
     */
    public long insertHash(String key, Map<String, Object> map) {
    	stringRedisTemplate.opsForHash().putAll(key,map);
        return map.size();
    }
    
// stringRedisTemplate.opsForValue();　　//操作字符串
// stringRedisTemplate.opsForHash();　　 //操作hash
// stringRedisTemplate.opsForList();　　 //操作list
// stringRedisTemplate.opsForSet();　　   //操作set
// stringRedisTemplate.opsForZSet();　 　//操作有序set

	

}
