package com.zpkj.quick.manage.util.http;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.zpkj.quick.manage.util.NullUtil;
import com.zpkj.quick.manage.util.RedisUtil;


/**
 * ==== @Component是一个通用的Spring容器管理的单例bean组件。而@Repository, @Service, @Controller就是针对不同的使用场景所采取的特定功能化的注解组件。
 * 当你的一个类被@Component所注解，那么就意味着同样可以用@Repository, @Service, @Controller来替代它
 * @Service, @Controller , @Repository = {@Component + 一些特定的功能}
 */

@Service
public class HttpDoRequest {
	private static final Logger LOGGER = LoggerFactory.getLogger(HttpDoRequest.class);
    private final static String TAG = HttpDoRequest.class.getSimpleName()+"{} ";
    
    private final static int BODY_TYPE_JSON = 0;
    private final static int BODY_TYPE_SOAP = 1;
    private final static int BODY_TYPE_FORM = 2;
    private final static int BODY_TYPE_DEFAULT= 3;
    private final static int BODY_TYPE_UPLOAD_FILE= 4;
    private final static int BODY_TYPE_XML= 5;
    
    private final static int CACHE_TIME_MINITUE = 5;
    
//   ==== @Autowired 注入 ，对象已经实例化，是在这个接口注解的时候实例化的； 整个生命周期都交由IOC容器管控；
//   而new是实例化一个对象，new的对象不能调用注入其他类，因为new出来的对象生命周期不受IOC容器管控，自然无法完成属性的注入 ， 如下面:mRedisUtil里面new的对象stringRedisTemplate不能调用，会为null
	@Autowired
	RedisUtil mRedisUtil;
    
    public String doGetRequestInCache(String urlHost, HashMap<String, String> paramsMap, int minitue) {
         String url = "";
         if (null == paramsMap|| paramsMap.isEmpty()) {
             url = urlHost;
         }
         else {
             url = urlHost + "?" + getUrlParams(paramsMap).toString();
         }
         
		String res = mRedisUtil.getStr(url);
		if (NullUtil.isNull(res)) {
			res = doGetRequest_old(urlHost, paramsMap);
			// 保存
			mRedisUtil.setStrWithCacheTime(url, res, CACHE_TIME_MINITUE);
		} else {
			LOGGER.error(TAG, " doGetRequest  url = " + url + "\t in redis cache " + res);
		}
		
		return res;
    }
    
    /**get方式请求服务器
     * @param urlHost
     * @param paramsMap
     */
    public String doGetRequest_old(String urlHost, HashMap<String, String> paramsMap) {
        String result = "";
        String url = "";
        if (null == paramsMap|| paramsMap.isEmpty()) {
            url = urlHost;
        }
        else {
            url = urlHost + "?" + getUrlParams(paramsMap).toString();
        }
        LOGGER.error(TAG, "doGetRequest_old url = " + url);
        if (NullUtil.isNull(urlHost)) {
            return result;
        }
        HttpClient httpClient = HttpClientManager.getInstance().getSimpleHttpClient();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse httpResponse;
        try {
            addGetHeaders(httpGet);
            httpResponse = httpClient.execute(httpGet);
            result = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
        }
        catch (Exception e) {
            LOGGER.error(TAG,  e.getMessage());
        }
        LOGGER.error(TAG, " doGetRequest_old url=" + url + "\t result=" + result);
        return result;
    }

    /**get方式请求服务器
     * @param urlHost
     * @param paramsMap
     */
    public String doGetRequest(String urlHost, HashMap<String, String> paramsMap) {
    	return sendGet(urlHost, paramsMap);
    }
    
	private String sendGet(String urlHost, HashMap<String, String> paramsMap) {
    	 String result = "";
         BufferedReader in = null;
    	 String url = "";
         if (null == paramsMap|| paramsMap.isEmpty()) {
             url = urlHost;
         }
         else {
             url = urlHost + "?" + getUrlParams(paramsMap).toString();
         }
         LOGGER.error(TAG, "sendGet url = " + url);
         if (NullUtil.isNull(urlHost)) {
             return result;
         }
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
//            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
//            for (String key : map.keySet()) {
//                System.out.println(key + "--->" + map.get(key));
//            }
            // 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			LOGGER.error(TAG, "发送GET请求出现异常！" + e.getMessage());
		}
        // 使用finally块来关闭输入流
        finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
        LOGGER.debug(TAG, "sendGet url = " + url+"\n result="+result);
        return result;
    }
	
	
	public HttpEntity doGetRequestBackEntry(String urlHost, HashMap<String, String> paramsMap) {
    	HttpEntity result =null;
        String url = "";
        if (null == paramsMap|| paramsMap.isEmpty()) {
            url = urlHost;
        }
        else {
            url = urlHost + "?" + getUrlParams(paramsMap).toString();
        }
        LOGGER.error(TAG, "doGetRequest url = " + url);
        if (NullUtil.isNull(urlHost)) {
            return null;
        }
        HttpClient httpClient = HttpClientManager.getInstance().getSimpleHttpClient();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse httpResponse;
        try {
            addGetHeaders(httpGet);
            httpResponse = httpClient.execute(httpGet);
            result = httpResponse.getEntity() ;
        }
        catch (Exception e) {
            LOGGER.error(TAG,  e.getMessage());
        }
        
//        try {
//			LOGGER.error(TAG, " doGetRequest url=" + url + "\t result=" + EntityUtils.toString( result, "utf-8"));
//		} catch (ParseException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
        
        return result;
    }
	
    
    public boolean doDeleteRequest(String urlHost, HashMap<String, String> paramsMap) {
        boolean result = false;
        String url = "";
        if (null == paramsMap|| paramsMap.isEmpty()) {
            url = urlHost;
        }
        else {
            url = urlHost + "?" + getUrlParams(paramsMap).toString();
        }
        LOGGER.error(TAG, "httpDelete url = " + url);
        if (NullUtil.isNull(urlHost)) {
            return false;
        }
        HttpClient httpClient = HttpClientManager.getInstance().getHttpClient();
        HttpDelete httpDelete = new HttpDelete(url);
        HttpResponse httpResponse;
        try {
            // addNewHeaders(httpGet);
            httpResponse = httpClient.execute(httpDelete);
            int responseCode = httpResponse.getStatusLine().getStatusCode();
            if (HttpStatus.SC_OK == responseCode) {
                result = true;
            }
            else {
                result = false;
            }
        }
        catch (Exception e) {
            LOGGER.error(TAG, "", e);
        }
        LOGGER.error(TAG, "doDeleteRequest url="+url+"\t result=" + result);
        return result;
    }
    
    /**
     * 根据url获取json文本 机制useCache为true使用缓存 根据url判断缓存文件是否存在以及是否在有效期内
     * 缓存时间传入
     * useCache为false则重新请求接口，失败则使用本地缓存
     */
//    public String doGetRequest( String urlHost,HashMap<String, String> paramsMap, boolean useCache,long validTime ) {
//    	String response = null;
//        File cacheFile = null;
//    	String url = "";
//        if (null == paramsMap|| paramsMap.isEmpty()) {
//            url = urlHost;
//        }
//        else {
//            url = urlHost + "?" + getUrlParams(paramsMap).toString();
//        }
//        LOGGER.error(TAG, "doGetRequest url = " + url);
//        if (NullUtil.isNull(url))    return "";
//        try {
//            cacheFile = AndroidCacheUtils.getCacheFile(, url);
//            if (useCache && AndroidCacheUtils.isCacheValid(cacheFile, validTime)) {
//                response = AndroidCacheUtils.getCacheContent(, cacheFile);
//                LOGGER.error(TAG, "doGetRequest useCache = " + useCache);
//                return response;
//            }
//        }
//        catch (Exception e) {
//        	LOGGER.error("","",e);
//        }
//
//        try {
//            response = AndroidHttpUtils.getJsonByGet(, url, cacheFile);
//            LOGGER.error(TAG, "doGetRequest getJsonByGet () success ");
//        }
//        catch (AndroidHttpException e) {
//            if (cacheFile != null && TextUtils.isEmpty(response)) {
//                try {
//                    response = AndroidCacheUtils.getCacheContent(, cacheFile);
//                }
//                catch (AndroidCacheException ace) {
//                    LOGGER.error("", "", ace);
//                }
//            }
//            LOGGER.error(TAG, "doGetRequest getJsonByGet () AndroidHttpException e ", e);
//        }
//        catch (AndroidCacheException e) {
//        	LOGGER.error("","",e);
//        }
//        LOGGER.error(TAG, "doGetRequest url="+url+"\t result=" + response);
//        return response;
//    }
    
    /**put方式请求服务器
     */
    public  boolean doPutRequest(String urlHost, HashMap<String, String> bodyMap) {
        boolean bRes = false;
        if (NullUtil.isNull(urlHost)) {
            return bRes;
        }
        if (NullUtil.isNull(bodyMap)) {
            LOGGER.error(TAG, "doPutMap   bodyMap== null ");
            return bRes;
        }
        
        JSONObject jsonObj = new JSONObject(bodyMap);
        LOGGER.error(TAG, "doPutRequest() url = "+urlHost + " bodyMap="+jsonObj.toString() );
        bRes = putExecute(urlHost,  jsonObj.toString(), BODY_TYPE_JSON);
        
        LOGGER.error(TAG, "doPutRequest() result=" + bRes);
        return bRes;
    }
    
    /**put方式请求服务器
     */
    public  boolean doPutRequest(String urlHost, String putBodyStr) {
        boolean bRes = false;
        if (NullUtil.isNull(urlHost)) {
            return bRes;
        }
        if (NullUtil.isNull(putBodyStr)) {
            LOGGER.error(TAG, "doPutRequest   putBodyStr== null ");
            return bRes;
        }
        LOGGER.error(TAG, "doPutRequest() url = "+urlHost + " putBodyStr="+putBodyStr );
        bRes = putExecute(urlHost, putBodyStr, BODY_TYPE_JSON);
        
        LOGGER.error(TAG, "doPutRequest() url="+urlHost+"\t bRes=" + bRes);
        return bRes;
    }
    
    /**post方式请求服务器
     */
    public String doPostSoap(String urlHost, String postBody) {
        String result = "";
        LOGGER.error(TAG, "doPostRequest url = " + urlHost + " postBody=" + postBody);
        if (NullUtil.isNull(urlHost)) {
            return result;
        }
        if ("null".equals(postBody) || NullUtil.isNull(postBody)) {
            return result;
        }
        result = postExcecute(urlHost, postBody, BODY_TYPE_SOAP);
        LOGGER.error(TAG, "doPostRequest url="+urlHost+"\t result=" + result);
        return result;
    }
    /**post方式请求服务器
     */
    public HttpEntity  doPostXML(String urlHost, String postBody) {
        HttpEntity entity = null;
        LOGGER.error(TAG, "doPostRequest url = " + urlHost + " postBody=" + postBody);
        if (NullUtil.isNull(urlHost)) {
            return entity;
        }
        if ("null".equals(postBody) || NullUtil.isNull(postBody)) {
            return entity;
        }
        entity = postExcecuteBackEntity(urlHost, postBody, BODY_TYPE_XML);
        LOGGER.error(TAG, "doPostRequest url="+urlHost+"\t result=" + entity);
        return entity;
    }

    public String doPostForm(String urlHost, HashMap<String, String> paramsMap) {
        LOGGER.error(TAG, "doPostForm() urlHost=" + urlHost);
        String result = "";
        if (NullUtil.isNull(urlHost)) {
            return result;
        }
        if (NullUtil.isNull(paramsMap)) {
            return result;
        }
        String urlBodyStr =getUrlParams(paramsMap).toString();
        LOGGER.error(TAG, "doPostForm() body=" + urlBodyStr);
        result = postExcecute(urlHost, urlBodyStr, BODY_TYPE_FORM);
        LOGGER.error(TAG, "doPostForm() url="+urlHost+"\t result=" + result);
        return result;
    }
    
    
    public String doPostRequest(String urlHost, HashMap<String, String> paramsMap) {
        String result = "";
        String urlBodyStr =getUrlParams(paramsMap).toString();
        LOGGER.error(TAG, "doPostRequest url = "+urlHost + " urlBodyStr="+urlBodyStr );
        if (NullUtil.isNull(urlHost)) {
            return result;
        }
        if ("null".equals(urlBodyStr) || NullUtil.isNull(urlBodyStr)) {
            return result;
        }
        result = postExcecute(urlHost,  urlBodyStr, BODY_TYPE_FORM);
        LOGGER.error(TAG, "doPostRequest url="+urlHost+"\t result=" + result);
        return result;
    }
    
    public String doPostRequest(String urlHost, HashMap<String, String> getParamsMap,
            HashMap<String, String> postParamsMap) {
        String result = "";
        urlHost += "?" + getUrlParams(getParamsMap).toString();
        String urlBodyStr = getUrlParams(postParamsMap).toString();
        LOGGER.error(TAG, "doPostRequest url = " + urlHost + " urlBodyStr=" + urlBodyStr);
        if (NullUtil.isNull(urlHost)) {
            return result;
        }
        if ("null".equals(urlBodyStr) || NullUtil.isNull(urlBodyStr)) {
            return result;
        }
        result = postExcecute(urlHost,  urlBodyStr, BODY_TYPE_FORM);
        LOGGER.error(TAG, "doPostRequest url="+urlHost+"\t result=" + result);
        return result;
    }

    public String doPostRequest(String urlHost, HashMap<String, String> getParamsMap, String postBody) {
        String result = "";
        urlHost += "?" + getUrlParams(getParamsMap).toString();
        LOGGER.error(TAG, "doPostRequest url = " + urlHost + " postBody=" + postBody);
        
        if (NullUtil.isNull(urlHost)) {
            return result;
        }
        if ("null".equals(postBody) || NullUtil.isNull(postBody)) {
            return result;
        }
        try {
            postBody = URLEncoder.encode(postBody, "UTF-8");
        }
        catch (UnsupportedEncodingException e1) {
        }
        result = postExcecute(urlHost, postBody, BODY_TYPE_FORM);
        LOGGER.error(TAG, "doPostRequest url="+urlHost+"\t result=" + result);
        return result;
    }
    
    public String doPostRequest(String urlHost, String postBody) {
        String result = "";
        LOGGER.error(TAG, "doPostRequest url = " + urlHost + " postBody=" + postBody);
        if (NullUtil.isNull(urlHost)) {
            return result;
        }
        if ("null".equals(postBody) || NullUtil.isNull(postBody)) {
            return result;
        }
        result = postExcecute(urlHost, postBody, BODY_TYPE_FORM);
        LOGGER.error(TAG, "doPostRequest url="+urlHost+"\t result=" + result);
        return result;
    }
    
    /**
     * postBody是json字符串
     * @param urlHost
     */
    public String doPostJsonRequest(String urlHost, String postBody) {
        String result = "";
        LOGGER.error(TAG, "doPostRequest url = " + urlHost + " postBody=" + postBody);
        if (NullUtil.isNull(urlHost)) {
            return result;
        }
        if ("null".equals(postBody) || NullUtil.isNull(postBody)) {
            return result;
        }
        result = postExcecute(urlHost, postBody, BODY_TYPE_JSON);
        LOGGER.error(TAG, "doPostRequest url="+urlHost+"\t result=" + result);
        return result;
    }
    
    /**
     * paramsMap转化成json字符串再发送出去
     * @param urlHost
     */
    public String doPostMapInJson(String urlHost, HashMap<String, String> paramsMap) {
        LOGGER.error(TAG, "doPostMap() urlHost=" + urlHost);
        String result = "";
        if (NullUtil.isNull(urlHost)) {
            return result;
        }
        if (NullUtil.isNull(paramsMap)) {
            return result;
        }
        JSONObject jsonObj = new JSONObject(paramsMap);
        String body = jsonObj.toString();
        LOGGER.error(TAG, "doPostMapInJson() body=" + body);
        result = postExcecute(urlHost, body, BODY_TYPE_JSON);
        LOGGER.error(TAG, "doPostMapInJson() url="+urlHost+"\t result=" + result);
        return result;
    }
    
    /**
     *发送 json字符串出去
     */
    public String doPostJson(String urlHost,  JSONObject jsonObj) {
        LOGGER.error(TAG, "doPostJson() urlHost=" + urlHost);
        String result = "";
        if (NullUtil.isNull(urlHost)) {
            return result;
        }
        Gson gson = new Gson();
        String body = gson.toJson(jsonObj);
        LOGGER.error(TAG, "doPostJson() body=" + body);
        result = postExcecute(urlHost, body, BODY_TYPE_JSON);
        LOGGER.error(TAG, "doPostJson() url="+urlHost+"\t result=" + result);
        return result;
    }
    
    /**
     * List<NameValuePair> pairList = new ArrayList<NameValuePair>();
     * NameValuePair pair1 = new BasicNameValuePair("username", name);
     * NameValuePair pair2 = new BasicNameValuePair("age", age);
     * pairList.add(pair1); 
     * pairList.add(pair2);
     * @param urlHost
     * @param nameValues
     * @return
     */
    public String doPostNameValuePair(String urlHost,  List<NameValuePair> nameValues) {
        LOGGER.error(TAG, "doPostNameValuePair() urlHost=" + urlHost);
        String result = "";
        if (NullUtil.isNull(urlHost)) {
            return result;
        }
        for (NameValuePair item : nameValues) {
            LOGGER.error(TAG, item.getName() + " \t " + item.getValue());
        }
        result = postExcuteNameValuePair(urlHost, nameValues, BODY_TYPE_FORM);
        LOGGER.error(TAG, "doPostNameValuePair() url="+urlHost+"\t result=" + result);
        return result;
    }
    
    private String postExcecute(String urlHost, String urlBodyStr, int postBodyType) {
        String result = null;
        HttpClient httpClient = HttpClientManager.getInstance().getHttpClient();
        HttpPost httpPost = new HttpPost(urlHost);
        HttpResponse httpResponse;
        StringEntity stringEntity;
        try {
            addHeaders(httpPost, postBodyType);
            stringEntity = new StringEntity(urlBodyStr, "UTF-8");
            stringEntity.setContentEncoding("UTF-8");
            httpPost.setEntity(stringEntity);
            
            httpResponse = httpClient.execute(httpPost);
            result = EntityUtils.toString(httpResponse.getEntity());
        }
        catch (Exception e) {
            LOGGER.error(TAG, "", e);
        }
        return result;
    }
    private HttpEntity postExcecuteBackEntity(String urlHost, String urlBodyStr, int postBodyType) {
        HttpClient httpClient = HttpClientManager.getInstance().getHttpClient();
        HttpPost httpPost = new HttpPost(urlHost);
        HttpResponse httpResponse = null;
        StringEntity stringEntity;
        try {
            addHeaders(httpPost, postBodyType);
            stringEntity = new StringEntity(urlBodyStr, "UTF-8");
            stringEntity.setContentEncoding("UTF-8");
            httpPost.setEntity(stringEntity);

            httpResponse = httpClient.execute(httpPost);
            return httpResponse.getEntity();
        }
        catch (Exception e) {
            LOGGER.error(TAG, "", e);
        }
        return  null;
    }

    /**
     * @param url
     * @param pairList
     * @param postBodyType
     */
    public String postExcuteNameValuePair(String url, List<NameValuePair> pairList,int postBodyType){
      String result = null;
      HttpClient httpClient = HttpClientManager.getInstance().getHttpClient();
      HttpPost httpPost = new HttpPost(url);
      HttpResponse response;
      try {
          addHeaders(httpPost, postBodyType);
          HttpEntity requestHttpEntity = new UrlEncodedFormEntity(pairList);
          httpPost.setEntity(requestHttpEntity);
          // 显示响应
          response = httpClient.execute(httpPost);
          result = EntityUtils.toString(response.getEntity());
      }
      catch (Exception e) {
          LOGGER.error(TAG, "", e);
      }
      return result;
  }
   
    private boolean putExecute(String urlHost, String putBodyStr, int bodyType) {
        boolean bRes= false;
        String result = null;
        HttpClient httpClient = HttpClientManager.getInstance().getHttpClient();
        HttpPut httpPut = new HttpPut(urlHost);
        HttpResponse httpResponse;
        StringEntity stringEntity;
        try {
            addHeaders(httpPut, bodyType);
            stringEntity = new StringEntity(putBodyStr, "UTF-8");
            stringEntity.setContentEncoding("UTF-8");
            httpPut.setEntity(stringEntity);
            
            httpResponse = httpClient.execute(httpPut);
            result = EntityUtils.toString(httpResponse.getEntity());
            
            int responseCode = httpResponse.getStatusLine().getStatusCode();
            if (HttpStatus.SC_OK == responseCode) {
                bRes = true;
            }
            else {
                bRes = false;
            }
            LOGGER.error(TAG, "putMapExcute() responseCode=" + responseCode+" result="+result);
        }
        catch (Exception e) {
            LOGGER.error(TAG, "", e);
            bRes  = false;
        }
        return bRes;
    }
    
    /***
     * 将参数map转化成字符串参数,用于url的body
     */
    public StringBuffer getUrlParams(HashMap<String, String> paramsMap) {
        StringBuffer body = new StringBuffer();
        Set<String> keyset = paramsMap.keySet();
        ArrayList<String> keyList = new ArrayList<String>(keyset);

        // 对key键值按字典升序排序
        Collections.sort(keyList);
        String value = null;
        try {
            for (int i = 0, size = keyList.size(); i < size; i++) {
                value = paramsMap.get(keyList.get(i));
                if (value != null) {
                    value = URLEncoder.encode(value, HTTP.UTF_8)
                            .replace("+", "%20")
                            .replace("*", "%2A")
                            .replace("%7E", "~");
                }
                body.append(keyList.get(i) + "=" + value);
                if (i != (keyList.size()-1) ) {
                    body.append("&");
                }
            }
        }
        catch (UnsupportedEncodingException e) {
           LOGGER.error(TAG, "getUrlBody()", e);
        }
        return body;
    }
    
    /**
     * MultipartEntity entity = new MultipartEntity();
       entity.addPart("file", new FileBody(uploadFile));
       entity.addPart("phoneNumber", new StringBody(phoneNumber)); //字符串参数，可添加多个
       entity.addPart("aa", new StringBody(aa)); //字符串参数，可添加多个
       
       HttpDoRequest.getInstance(mActivity).upLoadFileByHttpClient4(actionUrl, entity);
     */
//    public String upLoadFileByHttpClient4(String url, MultipartEntity entity) throws ClientProtocolException,
//            IOException {
//        LOGGER.error(TAG, "upLoadFileByHttpClient4() url= "+url);
//        HttpClient httpclient = new DefaultHttpClient();
//        httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
//        HttpPost httppost = new HttpPost(url);
//        httppost.setEntity(entity);
//        HttpResponse response = httpclient.execute(httppost);
//        HttpEntity resEntity = response.getEntity();
//        String result = EntityUtils.toString(resEntity);
//        LOGGER.error(TAG, "upLoadFileByHttpClient4() url="+url+"\t result=" + response);
//        if (resEntity != null) {
//            resEntity.consumeContent();
//        }
//        httpclient.getConnectionManager().shutdown();
//      
//        return  result;
//    }
    
//    public String upLoadFileByHttpClient4(String url, File uploadFile, String phoneNumber)
//            throws ClientProtocolException, IOException {
////        LOGGER.error(TAG, "upLoadFileByHttpClient4() url= "+url);
////        HttpClient httpclient = new DefaultHttpClient();
////        httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
////        HttpPost httppost = new HttpPost(url);
////        MultipartEntity entity = new MultipartEntity();
////        entity.addPart("file", new FileBody(uploadFile));
////        entity.addPart("phoneNumber", new StringBody(phoneNumber)); //字符串 参数，可添加多个
////        httppost.setEntity(entity);
////        HttpResponse response = httpclient.execute(httppost);
////        HttpEntity resEntity = response.getEntity();
////        String result = EntityUtils.toString(resEntity);
////        LOGGER.error(TAG, "upLoadFileByHttpClient4() result= "+result);
////        if (resEntity != null) {
////            resEntity.consumeContent();
////        }
////        httpclient.getConnectionManager().shutdown();
////        
////        return  result;
//        return  "";
//    }
    
    
    /**
     * 请求的头部信息
     */
    private void addHeaders(HttpRequest request, int postBodyType) {
        if (postBodyType == BODY_TYPE_FORM) {
            request.addHeader("content-type", "application/x-www-form-urlencoded");
        }
        else if (postBodyType == BODY_TYPE_JSON) {
            request.addHeader("content-type", "application/json"); 
        }
        else if (postBodyType == BODY_TYPE_SOAP) {
            request.addHeader("content-type", "application/soap+xml");
        }
        else if (postBodyType == BODY_TYPE_DEFAULT) {
            request.addHeader("content-type", "text/plain");  //默认
        }
        else if (postBodyType == BODY_TYPE_UPLOAD_FILE) {
            request.addHeader("content-type", "multipart/form-data"); 
        }
        else if (postBodyType == BODY_TYPE_XML) {
            request.setHeader("Content-Type", "text/xml;charset=ISO-8859-1");
        }
        
        request.addHeader("charset", "utf-8");
        request.addHeader("User-Agent", "Android/1.0");
    }
    /**
     * 请求的头部信息
     */
    public static void addGetHeaders(HttpRequest request) {
//        String secretKey = AndroidMD5.getMd5Str("abc");
//        request.addHeader("secretKey", secretKey);
    }
    
}
    
