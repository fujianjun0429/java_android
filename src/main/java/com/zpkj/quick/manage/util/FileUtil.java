package com.zpkj.quick.manage.util;

import com.zpkj.quick.manage.util.http.HttpDoRequest;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

public class FileUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(HttpDoRequest.class);

	public static String readFile(String filePath) {
		String content = "";
		try {
			//读取文件OK
			InputStream inputStream = FileUtil.class.getClassLoader().getResourceAsStream(filePath);//解析及播放最好的
			if (null != inputStream) {
				content = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
			} else {
				LOGGER.info("read " + filePath + " inputStream == null");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}

	/**
	 * 下载图片
	 * 
	 * @param urlString 图片url
	 * @param filename 本地文件全限定名
	 * @throws Exception
	 */
	public static boolean download(String urlString, String filename) {
		boolean success = false;
		try {
			// 构造URL
			URL url;
			url = new URL(urlString);
			// 打开连接
			URLConnection con = url.openConnection();
			// 输入流
			InputStream is = con.getInputStream();
			// 1K的数据缓冲
			byte[] bs = new byte[1024];
			// 读取到的数据长度
			int len;
			// 输出的文件流
			OutputStream os = new FileOutputStream(filename);
			// 开始读取
			while ((len = is.read(bs)) != -1) {
				os.write(bs, 0, len);
			}
			// 完毕，关闭所有链接
			os.close();
			is.close();
			success = true;
			return success;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}
}
