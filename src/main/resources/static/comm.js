/**
 * Created by 13 on 2017/2/22.
 */

	var appName = '';
	function doSearchOnline(value){
		appName = value;
		/*$('#setRecommend_table').datagrid({
			url:'onlineQuickInfoList?appName='+value
		})	*/
		var url = 'onlineQuickInfoList?appName='+value;
		$('#setRecommend_table').datagrid('reload',url);
		$('#onlineApp_table').datagrid('reload',url);
	}
	
	function buttonAdd(value,row,index){
		return '<button onclick="doAdd()">添加</button>';
	}

	 var dataGridP ='';
	 function doShowAddOrEditPositionDlg(tableControlID) {
		   var row = tableControlID.datagrid('getSelected');
		   dataGridP = tableControlID;
			$('#addOrEditPosition_dlg').dialog('open').dialog('setTitle', '编辑信息');
			$('#addOrEditPosition_fm').form('clear')
			$('#addOrEditPosition_fm').form('load', row);
	};
	
	$('#addOrEditPosition_fm').form({
		success:function(data){
			var data = eval('(' + data + ')');
			if (data.resultStatus){
				$.messager.alert('成功',data.errorMessage,'info');
				dataGridP.datagrid('reload');	 
				$('#addOrEditPosition_dlg').dialog('close');
			}else{
				$.messager.alert('失败',data.errorMessage,'info');
			}
		}
	})
	
	function  doDelPosition( tableControlID) {
		var row = tableControlID.datagrid('getSelected');
		if (row){
			$.messager.confirm('提示','确定删除?',function(r){
				if (r){
					$.post('deleteGroupPosition',{id:row.id},function(result){
						if (result.resultStatus){
							$.messager.alert('成功',result.errorMessage,'info');
							tableControlID.datagrid('reload');	 
							
						} else {
							$.messager.alert('失败',result.errorMessage,'info');
						}
					},'json');
				}
			});
		}
	};
	
	
	 var dataGridApp ='';
	 function doShowAddOrEditPositionAppDlg(tableAppId) {
		 dataGridApp = tableAppId;
		var row = tableAppId.datagrid('getSelected');
		$('#addOrEditPositionAPP_dlg').dialog('open').dialog('setTitle', '配置位置上的内容');
		$('#addOrEditPositionAPP_fm').form('clear');
		$('#addOrEditPositionAPP_fm').form('load', row);
	};
	
	
	$('#addOrEditPositionAPP_fm').form({
		success:function(data){
			var data = eval('(' + data + ')');
			if (data.resultStatus){
				$.messager.alert('成功',data.errorMessage,'info');
				dataGridApp.datagrid('reload');	 
				$('#addOrEditPositionAPP_dlg').dialog('close');
			}else{
				$.messager.alert('失败',data.errorMessage,'info');
			}
		}
	})
	
	function  doDelPositionApp( tableAppId) {
		var row = tableAppId.datagrid('getSelected');
		if (row){
			$.messager.confirm('提示','确定删除?',function(r){
				if (r){
					$.post('deleteAppOfPosition',{id:row.id},function(result){
						if (result.resultStatus){
							$.messager.alert('成功',result.errorMessage,'info');
							tableAppId.datagrid('reload');	 
							
						} else {
							$.messager.alert('失败',result.errorMessage,'info');
						}
					},'json');
				}
			});
		}
	};
	
	function  doSearch( value) {
		$('#appId').combobox('reload','onlineQuickInfoList?appName='+value);
		$('#topicId').combobox('reload','selectSpecialList?key='+value);
		$('#h5Id').combobox('reload','selectWordsList?key='+value);
		$('#hotWordsId').combobox('reload','selectWordsList?key='+value);
	};
	
	$('#selectType').combobox({
		onSelect : function(rec) {
			// ====js打印到控制台
			//console.log("Body tag is %o", rec);
			if (rec.value == "hotWords") {
				console.log("Body tag value hotWords");
				//$('#imageUrl').textbox({ disabled: true }); // 禁用
				//$('#imageFile').filebox({ disabled: true }); // 禁用
				$('#goUrl').textbox({ disabled: true }); // 禁用
				
			} else if (rec.value == "app") {
				$('#selectId').combobox("reload", "onlineQuickInfoList");
				//$('#imageUrl').textbox({ disabled: true }); // 禁用
				//$('#imageFile').filebox({ disabled: true }); // 禁用
				$('#goUrl').textbox({ disabled: true }); // 禁用
				
			} else if (rec.value == "special") {
				$('#selectId').combobox("reload", "selectSpecialList");
				$('#imageUrl').textbox({ disabled: false }); // 启用
				$('#imageFile').filebox({ disabled: false }); // 启用
				$('#goUrl').textbox({ disabled: false }); // 启用
				
			} 
			else if (rec.value == "h5") {
				$('#selectId').combobox("reload", "selectH5s");
				$('#imageUrl').textbox({ disabled: false }); // 启用
				$('#imageFile').filebox({ disabled: false }); // 启用
				$('#goUrl').textbox({ disabled: false }); // 启用
				
			}
			else if (rec.value == "hotWords2") {
				$('#selectId').combobox("reload", "selectWordsList");
				//$('#imageUrl').textbox({ disabled: true }); // 启用
				//$('#imageFile').filebox({ disabled: true }); // 启用
				$('#goUrl').textbox({ disabled: true }); // 启用
			}
		}
	});
	
/**
 * 保存文章
 * @param status
 */
function subArticle(status) {
    var title = $('#articleForm input[name=title]').val();
    var content = $('#text').val();
    if (title == '') {
        tale.alertWarn('请输入文章标题');
        return;
    }
    if (content == '') {
        tale.alertWarn('请输入文章内容');
        return;
    }
    $('#content-editor').val(content);
    $("#articleForm #status").val(status);
    $("#articleForm #categories").val($('#multiple-sel').val());
    var params = $("#articleForm").serialize();
    var url = $('#articleForm #cid').val() != '' ? '/admin/article/modify' : '/admin/article/publish';
    tale.post({
        url:url,
        data:params,
        success: function (result) {
            if (result && result.success) {
                tale.alertOk({
                    text:'文章保存成功',
                    then: function () {
                        setTimeout(function () {
                            window.location.href = '/admin/article';
                        }, 500);
                    }
                });
            } else {
                tale.alertError(result.msg || '保存文章失败');
            }
        }
    });
}
